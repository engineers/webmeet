using System.Data.Entity.Migrations;
using WebMeet.Migrations.SeedData;
using EntityFramework.DynamicFilters;

namespace WebMeet.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<WebMeet.EntityFramework.WebMeetDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "WebMeet";
        }

        protected override void Seed(WebMeet.EntityFramework.WebMeetDbContext context)
        {
            context.DisableAllFilters();
            new InitialDataBuilder(context).Build();
        }
    }
}
