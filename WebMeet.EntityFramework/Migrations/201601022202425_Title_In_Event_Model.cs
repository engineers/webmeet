namespace WebMeet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Title_In_Event_Model : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Conversations", "PersonBase_Id", "dbo.PersonBases");
            DropForeignKey("dbo.Events", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Files", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Messages", "ReceiverId", "dbo.PersonBases");
            DropForeignKey("dbo.Conversations", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Groups", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Guests", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Persons", "Id", "dbo.PersonBases");
            DropPrimaryKey("dbo.PersonBases");
            AddColumn("dbo.Events", "Title", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.PersonBases", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.PersonBases", "Id");
            AddForeignKey("dbo.Conversations", "PersonBase_Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Events", "PersonBaseId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Files", "PersonBaseId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Messages", "ReceiverId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Conversations", "PersonBaseId", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Groups", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Guests", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Persons", "Id", "dbo.PersonBases", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Persons", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Guests", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Groups", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Conversations", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Messages", "ReceiverId", "dbo.PersonBases");
            DropForeignKey("dbo.Files", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Events", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Conversations", "PersonBase_Id", "dbo.PersonBases");
            DropPrimaryKey("dbo.PersonBases");
            AlterColumn("dbo.PersonBases", "Id", c => c.Long(nullable: false));
            DropColumn("dbo.Events", "Title");
            AddPrimaryKey("dbo.PersonBases", "Id");
            AddForeignKey("dbo.Persons", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Guests", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Groups", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Conversations", "PersonBaseId", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Messages", "ReceiverId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Files", "PersonBaseId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Events", "PersonBaseId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Conversations", "PersonBase_Id", "dbo.PersonBases", "Id");
        }
    }
}
