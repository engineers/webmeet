namespace WebMeet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Conversation_CallerId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Conversations", "CallerId", c => c.Long(nullable: false));
            CreateIndex("dbo.Conversations", "CallerId");
            AddForeignKey("dbo.Conversations", "CallerId", "dbo.Persons", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Conversations", "CallerId", "dbo.Persons");
            DropIndex("dbo.Conversations", new[] { "CallerId" });
            DropColumn("dbo.Conversations", "CallerId");
        }
    }
}
