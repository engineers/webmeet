﻿using WebMeet.EntityFramework;

namespace WebMeet.Migrations.SeedData
{
    public class InitialDataBuilder
    {
        private readonly WebMeetDbContext _context;

        public InitialDataBuilder(WebMeetDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            new DefaultTenantRoleAndUserBuilder(_context).Build();
        }
    }
}
