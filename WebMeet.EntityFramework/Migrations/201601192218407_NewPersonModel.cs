namespace WebMeet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewPersonModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Persons", "Id", "dbo.User");
            DropForeignKey("dbo.Conversations", "PersonBase_Id", "dbo.PersonBases");
            DropForeignKey("dbo.Events", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Files", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Messages", "ReceiverId", "dbo.PersonBases");
            DropForeignKey("dbo.Conversations", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Groups", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Guests", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Persons", "Id", "dbo.PersonBases");
            DropPrimaryKey("dbo.PersonBases");
            AddColumn("dbo.Persons", "UserId", c => c.Long(nullable: false));
            AlterColumn("dbo.PersonBases", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.PersonBases", "Id");
            CreateIndex("dbo.Persons", "UserId");
            AddForeignKey("dbo.Persons", "UserId", "dbo.User", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Conversations", "PersonBase_Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Events", "PersonBaseId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Files", "PersonBaseId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Messages", "ReceiverId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Conversations", "PersonBaseId", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Groups", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Guests", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Persons", "Id", "dbo.PersonBases", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Persons", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Guests", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Groups", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Conversations", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Messages", "ReceiverId", "dbo.PersonBases");
            DropForeignKey("dbo.Files", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Events", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Conversations", "PersonBase_Id", "dbo.PersonBases");
            DropForeignKey("dbo.Persons", "UserId", "dbo.User");
            DropIndex("dbo.Persons", new[] { "UserId" });
            DropPrimaryKey("dbo.PersonBases");
            AlterColumn("dbo.PersonBases", "Id", c => c.Long(nullable: false));
            DropColumn("dbo.Persons", "UserId");
            AddPrimaryKey("dbo.PersonBases", "Id");
            AddForeignKey("dbo.Persons", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Guests", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Groups", "Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Conversations", "PersonBaseId", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Messages", "ReceiverId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Files", "PersonBaseId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Events", "PersonBaseId", "dbo.PersonBases", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Conversations", "PersonBase_Id", "dbo.PersonBases", "Id");
            AddForeignKey("dbo.Persons", "Id", "dbo.User", "Id");
        }
    }
}
