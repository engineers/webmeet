namespace WebMeet.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Init_Models : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AbpRoles", newName: "Roles");
            RenameTable(name: "dbo.AbpUsers", newName: "User");
            RenameTable(name: "dbo.AbpTenants", newName: "Tenants");
            CreateTable(
                "dbo.Acquaintances",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstPersonId = c.Long(nullable: false),
                        SecondPersonId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        Person_Id = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Acquaintance_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.Person_Id)
                .ForeignKey("dbo.Persons", t => t.FirstPersonId)
                .ForeignKey("dbo.Persons", t => t.SecondPersonId)
                .Index(t => t.FirstPersonId)
                .Index(t => t.SecondPersonId)
                .Index(t => t.Person_Id);
            
            CreateTable(
                "dbo.PersonBases",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Avatar = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PersonBase_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BlacklistItems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        OwnerId = c.Long(nullable: false),
                        PersonId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        Person_Id = c.Long(),
                        Person_Id1 = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BlacklistItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.OwnerId)
                .ForeignKey("dbo.Persons", t => t.PersonId)
                .ForeignKey("dbo.Persons", t => t.Person_Id)
                .ForeignKey("dbo.Persons", t => t.Person_Id1)
                .Index(t => t.OwnerId)
                .Index(t => t.PersonId)
                .Index(t => t.Person_Id)
                .Index(t => t.Person_Id1);
            
            CreateTable(
                "dbo.Conversations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PersonBaseId = c.Long(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        PersonBase_Id = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Conversation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonBases", t => t.PersonBase_Id)
                .ForeignKey("dbo.PersonBases", t => t.PersonBaseId)
                .Index(t => t.PersonBaseId)
                .Index(t => t.PersonBase_Id);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PersonBaseId = c.Long(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(nullable: false, maxLength: 500),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Event_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonBases", t => t.PersonBaseId, cascadeDelete: true)
                .Index(t => t.PersonBaseId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PersonBaseId = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_File_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonBases", t => t.PersonBaseId, cascadeDelete: true)
                .Index(t => t.PersonBaseId);
            
            CreateTable(
                "dbo.SharedFiles",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FileId = c.Long(nullable: false),
                        PersonId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SharedFile_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.FileId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.PersonId)
                .Index(t => t.FileId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SenderId = c.Long(nullable: false),
                        ReceiverId = c.Long(nullable: false),
                        Content = c.String(nullable: false, maxLength: 300),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Message_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonBases", t => t.ReceiverId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.SenderId)
                .Index(t => t.SenderId)
                .Index(t => t.ReceiverId);
            
            CreateTable(
                "dbo.GroupUsers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        GroupId = c.Long(nullable: false),
                        PersonId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GroupUser_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.GroupId)
                .ForeignKey("dbo.Persons", t => t.PersonId)
                .Index(t => t.GroupId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Invitations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SenderId = c.Long(nullable: false),
                        ReceiverId = c.Long(nullable: false),
                        State = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        Person_Id = c.Long(),
                        Person_Id1 = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Invitation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.ReceiverId)
                .ForeignKey("dbo.Persons", t => t.SenderId)
                .ForeignKey("dbo.Persons", t => t.Person_Id)
                .ForeignKey("dbo.Persons", t => t.Person_Id1)
                .Index(t => t.SenderId)
                .Index(t => t.ReceiverId)
                .Index(t => t.Person_Id)
                .Index(t => t.Person_Id1);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Group_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonBases", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Guests",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Token = c.Guid(nullable: false),
                        ConnectedDate = c.DateTime(),
                        ConversationId = c.Long(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Guest_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonBases", t => t.Id)
                .ForeignKey("dbo.Conversations", t => t.ConversationId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.ConversationId);
            
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Address = c.String(maxLength: 150),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Person_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonBases", t => t.Id)
                .ForeignKey("dbo.User", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Persons", "Id", "dbo.User");
            DropForeignKey("dbo.Persons", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Guests", "ConversationId", "dbo.Conversations");
            DropForeignKey("dbo.Guests", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Groups", "Id", "dbo.PersonBases");
            DropForeignKey("dbo.Acquaintances", "SecondPersonId", "dbo.Persons");
            DropForeignKey("dbo.Acquaintances", "FirstPersonId", "dbo.Persons");
            DropForeignKey("dbo.Invitations", "Person_Id1", "dbo.Persons");
            DropForeignKey("dbo.Invitations", "Person_Id", "dbo.Persons");
            DropForeignKey("dbo.Invitations", "SenderId", "dbo.Persons");
            DropForeignKey("dbo.Invitations", "ReceiverId", "dbo.Persons");
            DropForeignKey("dbo.Conversations", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.GroupUsers", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.GroupUsers", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.Messages", "SenderId", "dbo.Persons");
            DropForeignKey("dbo.Messages", "ReceiverId", "dbo.PersonBases");
            DropForeignKey("dbo.SharedFiles", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.SharedFiles", "FileId", "dbo.Files");
            DropForeignKey("dbo.Files", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Events", "PersonBaseId", "dbo.PersonBases");
            DropForeignKey("dbo.Conversations", "PersonBase_Id", "dbo.PersonBases");
            DropForeignKey("dbo.BlacklistItems", "Person_Id1", "dbo.Persons");
            DropForeignKey("dbo.BlacklistItems", "Person_Id", "dbo.Persons");
            DropForeignKey("dbo.BlacklistItems", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.BlacklistItems", "OwnerId", "dbo.Persons");
            DropForeignKey("dbo.Acquaintances", "Person_Id", "dbo.Persons");
            DropIndex("dbo.Persons", new[] { "Id" });
            DropIndex("dbo.Guests", new[] { "ConversationId" });
            DropIndex("dbo.Guests", new[] { "Id" });
            DropIndex("dbo.Groups", new[] { "Id" });
            DropIndex("dbo.Invitations", new[] { "Person_Id1" });
            DropIndex("dbo.Invitations", new[] { "Person_Id" });
            DropIndex("dbo.Invitations", new[] { "ReceiverId" });
            DropIndex("dbo.Invitations", new[] { "SenderId" });
            DropIndex("dbo.GroupUsers", new[] { "PersonId" });
            DropIndex("dbo.GroupUsers", new[] { "GroupId" });
            DropIndex("dbo.Messages", new[] { "ReceiverId" });
            DropIndex("dbo.Messages", new[] { "SenderId" });
            DropIndex("dbo.SharedFiles", new[] { "PersonId" });
            DropIndex("dbo.SharedFiles", new[] { "FileId" });
            DropIndex("dbo.Files", new[] { "PersonBaseId" });
            DropIndex("dbo.Events", new[] { "PersonBaseId" });
            DropIndex("dbo.Conversations", new[] { "PersonBase_Id" });
            DropIndex("dbo.Conversations", new[] { "PersonBaseId" });
            DropIndex("dbo.BlacklistItems", new[] { "Person_Id1" });
            DropIndex("dbo.BlacklistItems", new[] { "Person_Id" });
            DropIndex("dbo.BlacklistItems", new[] { "PersonId" });
            DropIndex("dbo.BlacklistItems", new[] { "OwnerId" });
            DropIndex("dbo.Acquaintances", new[] { "Person_Id" });
            DropIndex("dbo.Acquaintances", new[] { "SecondPersonId" });
            DropIndex("dbo.Acquaintances", new[] { "FirstPersonId" });
            DropTable("dbo.Persons",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Person_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Guests",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Guest_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Groups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Group_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Invitations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Invitation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.GroupUsers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GroupUser_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Messages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Message_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SharedFiles",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SharedFile_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Files",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_File_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Events",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Event_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Conversations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Conversation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.BlacklistItems",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BlacklistItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PersonBases",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PersonBase_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Acquaintances",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Acquaintance_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            RenameTable(name: "dbo.Tenants", newName: "AbpTenants");
            RenameTable(name: "dbo.User", newName: "AbpUsers");
            RenameTable(name: "dbo.Roles", newName: "AbpRoles");
        }
    }
}
