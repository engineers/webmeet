﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using WebMeet.Authorization.Roles;
using WebMeet.Calendar;
using WebMeet.Conversations;
using WebMeet.Files;
using WebMeet.Files.Shared;
using WebMeet.Friends.Acquaintances;
using WebMeet.Friends.Blacklists;
using WebMeet.Friends.Invitations;
using WebMeet.Groups;
using WebMeet.Messages;
using WebMeet.MultiTenancy;
using WebMeet.Persons;
using WebMeet.Persons.Addresses;
using WebMeet.Persons.Guests;
using WebMeet.Users;

namespace WebMeet.EntityFramework
{
    /// <summary>
    /// Klasa reprezentująca obiekty z bazy danych
    /// </summary>
    public class WebMeetDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        //TODO: Define an IDbSet for your Entities...

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public WebMeetDbContext()
            : base("WebMeet")
        {
        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in WebMeetDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of WebMeetDbContext since ABP automatically handles it.
         */
        public WebMeetDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        //This constructor is used in tests
        public WebMeetDbContext(DbConnection connection)
            : base(connection, true)
        {
        }

        /// <summary>
        /// Znajomośći
        /// </summary>
        public IDbSet<Acquaintance> Acquaintances { get; set; }
        /// <summary>
        /// Czarne listy
        /// </summary>
        public IDbSet<BlacklistItem> BlacklistItems { get; set; }
        /// <summary>
        /// Rozmowy
        /// </summary>
        public IDbSet<Conversation> Conversations { get; set; }
        /// <summary>
        /// Wydarzenia
        /// </summary>
        public IDbSet<Event> Event { get; set; }
        /// <summary>
        /// Pliki
        /// </summary>
        public IDbSet<File> File { get; set; }
        /// <summary>
        /// Grupy
        /// </summary>
        public IDbSet<Group> Groups { get; set; }
        /// <summary>
        /// Użytkownicy w grupach
        /// </summary>
        public IDbSet<GroupUser> GroupUsers { get; set; }
        /// <summary>
        /// Goście
        /// </summary>
        public IDbSet<Guest> Guests { get; set; } 
        /// <summary>
        /// Zaproszenia
        /// </summary>
        public IDbSet<Invitation> Invitations { get; set; }
        /// <summary>
        /// Wiadomości
        /// </summary>
        public IDbSet<Message> Messages { get; set; }
        /// <summary>
        /// Osoby
        /// </summary>
        public IDbSet<Person> Persons { get; set; }    
        /// <summary>
        /// Udostępnione pliki
        /// </summary>
        public IDbSet<SharedFile> SharedFiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Acquaintance>()
                .HasRequired(c => c.FirstPerson)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Acquaintance>()
                .HasRequired(s => s.SecondPerson)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BlacklistItem>()
                .HasRequired(c => c.Owner)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BlacklistItem>()
                .HasRequired(s => s.Person)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Conversation>()
                .HasRequired(s => s.PersonBase)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invitation>()
                .HasRequired(s => s.Sender)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invitation>()
                .HasRequired(s => s.Receiver)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonBase>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
                
            base.OnModelCreating(modelBuilder);
        }
    }
}
