﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace WebMeet.EntityFramework.Repositories
{
    /// <summary>
    /// Klasa bazowa implementująca wzorzec repozytorium
    /// </summary>
    /// <typeparam name="TEntity">Encja</typeparam>
    /// <typeparam name="TPrimaryKey">Typ klucza głównego encji</typeparam>
    public abstract class WebMeetRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<WebMeetDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected WebMeetRepositoryBase(IDbContextProvider<WebMeetDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }
    }

    /// <summary>
    /// Klasa bazowa implementująca wzorzec repozytorium dla encji z kluczem głównym typu long
    /// </summary>
    /// <typeparam name="TEntity">Encja</typeparam>
    public abstract class WebMeetRepositoryBase<TEntity> : WebMeetRepositoryBase<TEntity, long>
        where TEntity : class, IEntity<long>
    {
        protected WebMeetRepositoryBase(IDbContextProvider<WebMeetDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }
    }
}
