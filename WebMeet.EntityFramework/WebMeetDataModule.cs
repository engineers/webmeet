﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using WebMeet.EntityFramework;

namespace WebMeet
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(WebMeetCoreEntitiesModule))]
    public class WebMeetDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
