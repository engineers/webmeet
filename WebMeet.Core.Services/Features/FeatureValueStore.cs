using Abp.Application.Features;
using WebMeet.Authorization.Roles;
using WebMeet.Core.Services.MultiTenancy;
using WebMeet.MultiTenancy;
using WebMeet.Users;

namespace WebMeet.Core.Services.Features
{
    /// <summary>
    /// Implementacja interfejsu <see cref="IFeatureValueStore"/>
    /// </summary>
    public class FeatureValueStore : AbpFeatureValueStore<Tenant, Role, User>
    {
        public FeatureValueStore(TenantManager tenantManager)
            : base(tenantManager)
        {
        }
    }
}