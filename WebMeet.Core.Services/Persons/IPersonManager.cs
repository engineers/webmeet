﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMeet.Core.Services.Shared;
using WebMeet.Persons;

namespace WebMeet.Core.Services.Persons
{
    /// <summary>
    /// Interfejs dla serwisu odpowiedzilnego za operacje na encji typu <see cref="Person"/>
    /// </summary>
    public interface IPersonManager : IIdentifiableDomainManagerBase<Person>
    {
        /// <summary>
        /// Aktualizuje osobę w obiekcie <paramref name="person"/>
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="person">Osoba z nowymi danymi</param>
        /// <returns>Zaktualizowana osoba</returns>
        Task<Person> Update(long personId, Person person);
    }
}
