﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using CuttingEdge.Conditions;
using WebMeet.Core.Services.Shared;
using WebMeet.Persons;

namespace WebMeet.Core.Services.Persons.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na encji typu <see cref="Person"/>
    /// </summary>
    public class PersonManager : IdentifiableDomainManagerBase<Person>, IPersonManager
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="repository">Repozytorium typu <see cref="Person"/></param>
        public PersonManager(IRepository<Person, long> repository) : base(repository)
        {
        }

        /// <summary>
        /// Aktualizuje osobę w obiekcie <paramref name="person"/>
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="person">Osoba z nowymi danymi</param>
        /// <returns>Zaktualizowana osoba</returns>
        public async Task<Person> Update(long personId, Person person)
        {
            Condition.Requires(person).IsNotNull();
            if(personId != person.Id)
                throw new UserFriendlyException(L("YouAreNotResourceOwner"));
            return await Repository.UpdateAsync(person);
        }
    }
}
