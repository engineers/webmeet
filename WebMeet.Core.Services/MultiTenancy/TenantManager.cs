﻿using Abp.Domain.Repositories;
using Abp.MultiTenancy;
using WebMeet.Authorization.Roles;
using WebMeet.Core.Services.Editions;
using WebMeet.MultiTenancy;
using WebMeet.Users;

namespace WebMeet.Core.Services.MultiTenancy
{
    /// <summary>
    /// Serwis odpowiedzilany zao peracje na encji <see cref="Tenant"/>
    /// </summary>
    public class TenantManager : AbpTenantManager<Tenant, Role, User>
    {
        public TenantManager(EditionManager editionManager)
            : base(editionManager)
        {
        }
    }
}