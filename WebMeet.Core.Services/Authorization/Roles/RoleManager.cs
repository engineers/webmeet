﻿using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Runtime.Caching;
using Abp.Zero.Configuration;
using WebMeet.Authorization.Roles;
using WebMeet.MultiTenancy;
using WebMeet.Users;

namespace WebMeet.Core.Services.Authorization.Roles
{
    /// <summary>
    /// Klasa  implementująca 'Role Manager' z ASP.NET Identity Framework
    /// </summary>
    public class RoleManager : AbpRoleManager<Tenant, Role, User>
    {
        public RoleManager(
            RoleStore store,
            IPermissionManager permissionManager,
            IRoleManagementConfig roleManagementConfig,
            ICacheManager cacheManager)
            : base(
                store,
                permissionManager,
                roleManagementConfig,
                cacheManager)
        {
        }
    }
}