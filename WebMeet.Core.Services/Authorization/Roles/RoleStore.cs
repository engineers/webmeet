﻿using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Runtime.Caching;
using WebMeet.Authorization.Roles;
using WebMeet.MultiTenancy;
using WebMeet.Users;

namespace WebMeet.Core.Services.Authorization.Roles
{
    /// <summary>
    /// Klasa  implementująca 'Role Store' z ASP.NET Identity Framework
    /// </summary>
    public class RoleStore : AbpRoleStore<Tenant, Role, User>
    {
        public RoleStore(
            IRepository<Role> roleRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<RolePermissionSetting, long> rolePermissionSettingRepository,
            ICacheManager cacheManager)
            : base(
                roleRepository,
                userRoleRepository,
                rolePermissionSettingRepository,
                cacheManager)
        {
        }
    }
}