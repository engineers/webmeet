﻿using Abp.Authorization;
using WebMeet.Authorization.Roles;
using WebMeet.Core.Services.Users;
using WebMeet.Core.Services.Users.Abp;
using WebMeet.MultiTenancy;
using WebMeet.Users;

namespace WebMeet.Core.Services.Authorization.Permissions
{
    /// <summary>
    /// Klasa odpowiedzialna za sprawdzenie uprawnień użytkownika
    /// </summary>
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(AbpUserManager userManager)
            : base(userManager)
        {

        }
    }
}
