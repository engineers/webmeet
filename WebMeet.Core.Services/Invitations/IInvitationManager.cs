﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using WebMeet.Core.Services.Shared;
using WebMeet.Friends.Blacklists;
using WebMeet.Friends.Invitations;
using WebMeet.Persons;

namespace WebMeet.Core.Services.Invitations
{
    /// <summary>
    /// Interfejs dla serwisu odpowiedzialnego za operacje na encji typu <see cref="Invitation"/>
    /// </summary>
    public interface IInvitationManager : IIdentifiableDomainManagerBase<Invitation>
    {
        /// <summary>
        /// Wysłanie zaproszenia do znajomości
        /// </summary>
        /// <param name="sender">Nadawca zaproszenia</param>
        /// <param name="receiver">Odbiorca zaproszenia</param>
        /// <returns>Utworzone zaproszenie</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy jedna ze stron znajduje się na czarnej liście u drugiej osoby</exception>
        /// <exception cref="UserFriendlyException">Rzucany fdy osoby są już znajomymi</exception>
        Task<Invitation> InvitePerson(Person sender, Person receiver);
        /// <summary>
        /// Akceptacja zaproszenia
        /// </summary>
        /// <param name="acceptingPersonId">Id osoby akceptującej</param>
        /// <param name="invitation">Obiekt reprezentujący zaproszenie</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy zaproszenie nie jest adresowany do osoby akceptującej</exception>
        void AcceptInvitation(long acceptingPersonId, Invitation invitation);
        /// <summary>
        /// Odrzucenie zaproszenia
        /// </summary>
        /// <param name="rejectingPersonId">Id osoby odrzucającej</param>
        /// <param name="invitation">Obiekt reprezentujący zaproszenie</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy zaproszenie nie jest adresowany do osoby akceptującej</exception>
        void RejectInvitation(long rejectingPersonId, Invitation invitation);
        /// <summary>
        /// Anulowanie zaproszenia
        /// </summary>
        /// <param name="cancelingPersonId">Id osoby anulującej</param>
        /// <param name="invitation">Obiekt reprezentujący zaproszenie</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy zaproszenie nie jest adresowany do osoby akceptującej</exception>
        void CancelInvitation(long cancelingPersonId, Invitation invitation);
    }
}
