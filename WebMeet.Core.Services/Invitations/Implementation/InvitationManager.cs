﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using CuttingEdge.Conditions;
using WebMeet.Core.Services.Shared;
using WebMeet.Friends.Acquaintances;
using WebMeet.Friends.Blacklists;
using WebMeet.Friends.Invitations;

namespace WebMeet.Core.Services.Invitations.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na encji typu <see cref="Invitation"/>
    /// </summary>
    public class InvitationManager : IdentifiableDomainManagerBase<Invitation>, IInvitationManager
    {
        protected readonly IRepository<BlacklistItem, long> BlacklistRepository;
        protected readonly IRepository<Acquaintance, long> AcquaintanceRepository; 

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="blacklistRepository">Repozytorium typu <see cref="BlacklistItem"/></param>
        /// <param name="acquaintanceRepository">Repozytorium typu <see cref="Acquaintance"/></param>
        /// <param name="invitationRepository">Repozytorium typu <see cref="Invitation"/></param>
        public InvitationManager(IRepository<BlacklistItem, long> blacklistRepository,
            IRepository<Acquaintance, long> acquaintanceRepository,
            IRepository<Invitation, long> invitationRepository) : base(invitationRepository)
        {
            BlacklistRepository = blacklistRepository;
            AcquaintanceRepository = acquaintanceRepository;
        }

        /// <summary>
        /// Wysłanie zaproszenia do znajomości
        /// </summary>
        /// <param name="sender">Nadawca zaproszenia</param>
        /// <param name="receiver">Odbiorca zaproszenia</param>
        /// <returns>Utworzone zaproszenie</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy jedna ze stron znajduje się na czarnej liście u drugiej osoby</exception>
        /// <exception cref="UserFriendlyException">Rzucany fdy osoby są już znajomymi</exception>
        public async Task<Invitation> InvitePerson(WebMeet.Persons.Person sender, WebMeet.Persons.Person receiver)
        {
            Condition.Requires(sender).IsNotNull();
            Condition.Requires(receiver).IsNotNull();
            await ThrowExceptionIfBlackListContainsUsers(sender, receiver);
            await ThrowExceptionIfBlackListContainsUsers(receiver, sender);
            await ThrowExceptionIfPersonsAreAcquaintances(sender.Id, receiver.Id);
            ThrowExceptionIfThereIsActiveInvitationsBetweenUsers(sender.Id, receiver.Id);

            var invitation = await GetInvitationBetweenUsers(sender.Id, receiver.Id);
            invitation.State = InvitationState.Sent;
            return await Repository.InsertOrUpdateAsync(invitation);
        }

        /// <summary>
        /// Akceptacja zaproszenia
        /// </summary>
        /// <param name="acceptingPersonId">Id osoby akceptującej</param>
        /// <param name="invitation">Obiekt reprezentujący zaproszenie</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy zaproszenie nie jest adresowany do osoby akceptującej</exception>
        public void AcceptInvitation(long acceptingPersonId, Invitation invitation)
        {
            Condition.Requires(invitation).IsNotNull();
            ThrowExceptionIfPersonIsNotReceiverOfInvitation(acceptingPersonId, invitation);
            invitation.State = InvitationState.Accepted;
            CreateAcquaintance(acceptingPersonId, invitation.SenderId);
        }

        /// <summary>
        /// Odrzucenie zaproszenia
        /// </summary>
        /// <param name="rejectingPersonId">Id osoby odrzucającej</param>
        /// <param name="invitation">Obiekt reprezentujący zaproszenie</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy zaproszenie nie jest adresowany do osoby akceptującej</exception>
        public void RejectInvitation(long rejectingPersonId, Invitation invitation)
        {
            Condition.Requires(invitation).IsNotNull();
            ThrowExceptionIfInvitationIsAccepted(invitation);
            ThrowExceptionIfPersonIsNotReceiverOfInvitation(rejectingPersonId, invitation);
            invitation.State = InvitationState.Rejected;
        }

        /// <summary>
        /// Anulowanie zaproszenia
        /// </summary>
        /// <param name="cancelingPersonId">Id osoby anulującej</param>
        /// <param name="invitation">Obiekt reprezentujący zaproszenie</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy zaproszenie nie jest adresowany do osoby akceptującej</exception>
        public void CancelInvitation(long cancelingPersonId, Invitation invitation)
        {
            Condition.Requires(invitation).IsNotNull();
            ThrowExceptionIfInvitationIsAccepted(invitation);
            ThrowExceptionIfPersonIsNotSenderOfInvitation(cancelingPersonId, invitation);
            invitation.State = InvitationState.Canceled;
        }

        private async Task ThrowExceptionIfBlackListContainsUsers(WebMeet.Persons.Person owner, WebMeet.Persons.Person person)
        {
            if (await BlacklistRepository.FirstOrDefaultAsync(
                blacklistitem => blacklistitem.OwnerId == owner.Id && blacklistitem.PersonId == person.Id) != null)
            {
                throw new UserFriendlyException(L("BlackList"), L("PersonIsOnBlacklist", string.Format("{0} {1}", person.FirstName, person.LastName), string.Format("{0} {1}", owner.FirstName, owner.LastName)));
            }
        }

        private void ThrowExceptionIfPersonIsNotReceiverOfInvitation(long receiverId, Invitation invitation)
        {
            if (invitation.ReceiverId != receiverId)
                throw new UserFriendlyException(L("ErrorDetected"), L("YouAreNotReceiverOfInvitation"));
        }

        private void ThrowExceptionIfPersonIsNotSenderOfInvitation(long senderId, Invitation invitation)
        {
            if (invitation.SenderId != senderId)
                throw new UserFriendlyException(L("ErrorDetected"), L("YouAreNotSenderOfInvitation"));
        }

        private async Task ThrowExceptionIfPersonsAreAcquaintances(long senderId, long receiverId)
        {
            var acquaintance =await AcquaintanceRepository
                .FirstOrDefaultAsync(acq =>
                    (acq.FirstPersonId == senderId && acq.SecondPersonId == receiverId) ||
                    (acq.FirstPersonId == receiverId && acq.SecondPersonId == senderId));

            if(acquaintance != null)
                throw new UserFriendlyException(L("IllegalOperation"), L("UsersAreFriends"));
        }

        private void CreateAcquaintance(long firstPersonId, long secondPersonId)
        {
            var acquaintance = new Acquaintance
            {
                FirstPersonId = firstPersonId,
                SecondPersonId = secondPersonId,
            };
            AcquaintanceRepository.Insert(acquaintance);
        }

        private void ThrowExceptionIfInvitationIsAccepted(Invitation invitation)
        {
            if(invitation.State == InvitationState.Accepted)
                throw new UserFriendlyException(L("IllegalOperation"), L("InvitationAcceptedYet"));
        }

        private void ThrowExceptionIfThereIsActiveInvitationsBetweenUsers(long firstId, long secondId)
        {
            if(Repository.GetAll().Any(invitation => (( invitation.SenderId == firstId && invitation.ReceiverId == secondId) || ( invitation.SenderId == secondId && invitation.ReceiverId == firstId)) && invitation.State == InvitationState.Sent))
                throw new UserFriendlyException(L("IllegalOperation"), L("InvitationBetweenUsersExist"));
        }

        private async Task<Invitation> GetInvitationBetweenUsers(long senderId, long receiverId)
        {
            var invitation =
                await
                    Repository.FirstOrDefaultAsync(
                        i => i.SenderId == senderId && i.ReceiverId == receiverId);

            return invitation ?? Invitation.CreateSentInvitation(senderId, receiverId);
        }
    }
}
