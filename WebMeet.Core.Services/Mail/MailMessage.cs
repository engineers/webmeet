﻿using System;
using System.Collections.Generic;

namespace WebMeet.Core.Services.Mail
{
    /// <summary>
    /// Klasa reprezentująca wiadomość
    /// </summary>
    public class MailMessage
    {
        private List<string> _recipients = new List<string>();
        private string _subject;

        /// <summary>
        ///     Tworzy pustą instancję klasy<see cref="MailMessage" />.
        /// </summary>
        public MailMessage()
        {
        }

        /// <summary>
        ///     Tworzy nową instancję klasy <see cref="MailMessage" /> z parametrami.
        /// </summary>
        /// <param name="recipient">Mail</param>
        /// <param name="subject">Tytuł wiadomości</param>
        /// <param name="body">Zawartość wiadomości</param>
        public MailMessage(string recipient, string subject, string body)
        {
            Recipients.Add(recipient);
            Body = body;
            Subject = subject;
        }

        /// <summary>
        ///     Pobiera/ustawia adresy odbiorców
        /// </summary>
        /// <exception cref="ArgumentNullException">Rzucany gdy ustawimy NULL</exception>
        public List<string> Recipients
        {
            get { return _recipients; }
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                if (_recipients.Equals(value))
                {
                    return;
                }
                _recipients = value;
            }
        }

        /// <summary>
        ///     Zawartość wiadomości
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        ///     Tytuł wiadomości
        /// </summary>
        public string Subject
        {
            get { return string.IsNullOrEmpty(_subject) ? string.Empty : _subject; }
            set { _subject = value; }
        }

        /// <summary>
        /// Zawartość wiadomości jako HTML
        /// </summary>
        public string HtmlBody { get; set; }
    }
}
