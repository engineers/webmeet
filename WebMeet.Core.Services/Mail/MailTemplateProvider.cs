﻿using System;
using Abp.Domain.Services;

namespace WebMeet.Core.Services.Mail
{
    /// <summary>
    /// Klasa zawierająca szablony wiadomości wysyłanych w aplikacji
    /// </summary>
    public class MailTemplateProvider : DomainService, IMailTemplateProvider
    {
        public MailTemplateProvider()
        {
            LocalizationSourceName = WebMeetConsts.LocalizationSourceName;
        }

        /// <summary>
        ///     Tworzy szablon dla wiadomości potwierdzającej email
        /// </summary>
        /// <param name="userName">Login użytkownika</param>
        /// <param name="email">Email</param>
        /// <param name="linkUrl">Link potwierdzający</param>
        public Func<MailMessage> EmailConfirmationTemplate(string userName, string email, string linkUrl)
        {
            if (string.IsNullOrWhiteSpace(userName)) throw new ArgumentException("userName");
            if (string.IsNullOrWhiteSpace(email)) throw new ArgumentException("email");
            if (string.IsNullOrWhiteSpace(linkUrl)) throw new ArgumentException("linkUrl");
            return () =>
            {
                var message = new MailMessage(
                    email,
                    L("ConfirmMail"),
                    string.Empty)
                {
                    HtmlBody = L("ConfirmMailRedirection", linkUrl)
                };
                return message;
            };
        }

        /// <summary>
        ///     Tworzy szablon dla wiadomości resetującej hasło
        /// </summary>
        /// <param name="userName">Login użytkownika</param>
        /// <param name="email">Email</param>
        /// <param name="linkUrl">Link do resetu hasła</param>
        public Func<MailMessage> PasswordResetTemplate(string userName, string email, string linkUrl)
        {
            if (string.IsNullOrWhiteSpace(userName)) throw new ArgumentException("userName");
            if (string.IsNullOrWhiteSpace(email)) throw new ArgumentException("email");
            if (string.IsNullOrWhiteSpace(linkUrl)) throw new ArgumentException("linkUrl");
            return () =>
            {
                var message = new MailMessage(
                    email,
                    L("ResetPassword"),
                    string.Empty)
                {
                    HtmlBody = L("ResetPasswordRedirection", linkUrl)
                };
                return message;
            };
        }

        /// <summary>
        ///     Tworzy szablon wiadomości z zaproszeniem do systemu
        /// </summary>
        /// <param name="email">Email</param>
        /// <param name="senderName">Nazwa użytkownika zapraszającego</param>
        /// <param name="senderLogin">Login użytkownika zapraszjącego</param>
        /// <param name="linkUrl">Link do systemu</param>
        /// <returns></returns>
        public Func<MailMessage> FriendInvitationToSystemTemplate(string email, string senderName, string senderLogin, string linkUrl)
        {
            if (string.IsNullOrWhiteSpace(email)) throw new ArgumentException("email");
            if (string.IsNullOrWhiteSpace(senderName)) throw new ArgumentException("senderName");
            if (string.IsNullOrWhiteSpace(senderLogin)) throw new ArgumentException("senderLogin");
            if (string.IsNullOrWhiteSpace(linkUrl)) throw new ArgumentException("linkUrl");
            return () =>
            {
                var message = new MailMessage(
                    email,
                    L("FriendInvitationToSystemTitle"),
                    string.Empty)
                {
                    HtmlBody = L("FriendInvitationToSystemContent", senderName, senderLogin, linkUrl)
                };
                return message;
            };
        }
    }
}
