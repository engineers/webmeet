﻿using System;
using Abp.Domain.Services;

namespace WebMeet.Core.Services.Mail
{
    /// <summary>
    /// Interfejs reprezentujący szablony wiadomości wysyłanych w aplikacji
    /// </summary>
    public interface IMailTemplateProvider : IDomainService
    {
        /// <summary>
        ///     Tworzy szablon dla wiadomości potwierdzającej email
        /// </summary>
        /// <param name="userName">Login użytkownika</param>
        /// <param name="email">Email</param>
        /// <param name="linkUrl">Link potwierdzający</param>
        Func<MailMessage> EmailConfirmationTemplate(string userName, string email, string linkUrl);

        /// <summary>
        ///     Tworzy szablon dla wiadomości resetującej hasło
        /// </summary>
        /// <param name="userName">Login użytkownika</param>
        /// <param name="email">Email</param>
        /// <param name="linkUrl">Link do resetu hasła</param>
        Func<MailMessage> PasswordResetTemplate(string userName, string email, string linkUrl);

        /// <summary>
        ///     Tworzy szablon wiadomości z zaproszeniem do systemu
        /// </summary>
        /// <param name="email">Email</param>
        /// <param name="senderName">Nazwa użytkownika zapraszającego</param>
        /// <param name="senderLogin">Login użytkownika zapraszjącego</param>
        /// <param name="linkUrl">Link do systemu</param>
        /// <returns></returns>
        Func<MailMessage> FriendInvitationToSystemTemplate(string email, string senderName, string senderLogin, string linkUrl);
    }
}
