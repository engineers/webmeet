﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.UI;
using WebMeet.Core.Services.Shared;
using WebMeet.Groups;

namespace WebMeet.Core.Services.Groups
{
    /// <summary>
    /// Interfejs dla serwisu odpowiedzialnego za operacje na encji typu <see cref="Group"/>
    /// </summary>
    public interface IGroupManager : IIdentifiableDomainManagerBase<Group>
    {
        /// <summary>
        /// Tworzy nową grupę
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="group">Obiekt grupy</param>
        /// <returns>Utworzoną grupę</returns>
        Task<Group> Create(long personId, Group group);
        /// <summary>
        /// Aktualizuje dane grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej</param>
        /// <param name="group">Nowy obiekt grupy</param>
        /// <returns>Zaktualizowaną grupę</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy osoba nie należy do grupy</exception>
        Task<Group> Update(long personId, Group group);
        /// <summary>
        /// Dodaje osobę do grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="personIdToAdd">Id osoby do dodania</param>
        /// <param name="groupId">Id grupy</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik wykonujący operację nie należy do grupy</exception>
        Task AddPerson(long personId, long personIdToAdd, long groupId);
        /// <summary>
        /// Usuwa osobę z grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="personIdToDelete">Id osoby do usunięcia</param>
        /// <param name="groupId">Id grupy</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik wykonujący operację nie należy do grupy</exception>
        Task DeletePerson(long personId, long personIdToDelete, long groupId);
        /// <summary>
        /// Pobiera grupę
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="groupId">Id grupy do pobrania</param>
        /// <returns>Grupę</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik wykonujący operację nie należy do grupy</exception>
        Task<Group> Get(long personId, long groupId);
        /// <summary>
        /// Usunięcie grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="group">Id grupy do pobrania</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik wykonujący operację nie należy do grupy</exception>
        Task Delete(long personId, Group group);
    }
}
