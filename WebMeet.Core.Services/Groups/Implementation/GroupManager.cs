﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using CuttingEdge.Conditions;
using WebMeet.Core.Services.Shared;
using WebMeet.Groups;
using WebMeet.Persons;

namespace WebMeet.Core.Services.Groups.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na encji typu <see cref="Group"/>
    /// </summary>
    public class GroupManager : IdentifiableDomainManagerBase<Group>, IGroupManager
    {
        protected readonly IRepository<GroupUser, long> GroupUserRepository;
        protected readonly IRepository<Person, long> PersonRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="repository">Repozytorium typu <see cref="Group"/></param>
        /// <param name="groupUserRepository">Repozytorium typu <see cref="GroupUser"/></param>
        /// <param name="personRepository">Repozytorium typu <see cref="Person"/></param>
        public GroupManager(IRepository<Group, long> repository, 
            IRepository<GroupUser, long> groupUserRepository, 
            IRepository<Person, long> personRepository) : base(repository)
        {
            GroupUserRepository = groupUserRepository;
            PersonRepository = personRepository;
        }

        /// <summary>
        /// Tworzy nową grupę
        /// </summary>
        /// <param name="persondId">Id osoby wykonującej operację</param>
        /// <param name="group">Obiekt grupy</param>
        /// <returns>Utworzoną grupę</returns>
        public async Task<Group> Create(long persondId, Group group)
        {
            Condition.Requires(group).IsNotNull();

            group.Id = GetIdForInsert();
            await Repository.InsertAsync(group);
            
            await GroupUserRepository.InsertAsync(new GroupUser
            {
                GroupId = group.Id,
                PersonId = persondId
            });
            return group;
        }

        /// <summary>
        /// Aktualizuje dane grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej</param>
        /// <param name="group">Nowy obiekt grupy</param>
        /// <returns>Zaktualizowaną grupę</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy osoba nie należy do grupy</exception>
        public async Task<Group> Update(long personId, Group group)
        {
            await CheckIfPersonBelongToGroup(personId, group.Id);
            return await Repository.UpdateAsync(group);
        }

        /// <summary>
        /// Dodaje osobę do grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="personIdToAdd">Id osoby do dodania</param>
        /// <param name="groupId">Id grupy</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik wykonujący operację nie należy do grupy</exception>
        public async Task AddPerson(long personId, long personIdToAdd, long groupId)
        {
            await CheckIfPersonBelongToGroup(personId, groupId);
            await CheckIfPersonNotBelongToGroup(personIdToAdd, groupId);
            await GroupUserRepository.InsertAsync(new GroupUser
            {
                GroupId = groupId,
                PersonId = personIdToAdd
            });
        }

        /// <summary>
        /// Usuwa osobę z grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="personIdToDelete">Id osoby do usunięcia</param>
        /// <param name="groupId">Id grupy</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik wykonujący operację nie należy do grupy</exception>
        public async Task DeletePerson(long personId, long personIdToDelete, long groupId)
        {
            await CheckIfPersonBelongToGroup(personId, groupId);
            await CheckIfPersonBelongToGroup(personIdToDelete, groupId);
            await GroupUserRepository.DeleteAsync(gu => gu.GroupId == groupId && gu.PersonId == personIdToDelete);
        }

        /// <summary>
        /// Pobiera grupę
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="groupId">Id grupy do pobrania</param>
        /// <returns>Grupę</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik wykonujący operację nie należy do grupy</exception>
        public async Task<Group> Get(long personId, long groupId)
        {
            await CheckIfPersonBelongToGroup(personId, groupId);
            return await GetAsync(groupId);
        }

        /// <summary>
        /// Usunięcie grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="group">Id grupy do pobrania</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik wykonujący operację nie należy do grupy</exception>
        public async Task Delete(long personId, Group group)
        {
            await CheckIfPersonBelongToGroup(personId, group.Id);
            await Repository.DeleteAsync(group);
        }

        private async Task CheckIfPersonBelongToGroup(long personId, long groupId)
        {
            if( (await GroupUserRepository.FirstOrDefaultAsync(gu => gu.GroupId == groupId && gu.PersonId == personId)) == null)
                throw new UserFriendlyException(L("YouAreNotBelongToGroup"));
        }

        private async Task CheckIfPersonNotBelongToGroup(long personId, long groupId)
        {
            if ((await GroupUserRepository.FirstOrDefaultAsync(gu => gu.GroupId == groupId && gu.PersonId == personId)) != null)
                throw new UserFriendlyException(L("PersonBelongToGroup"));
        }

        private long GetIdForInsert()
        {
            long personMax = 1;
            long groupMax = 1;
            if (PersonRepository.GetAll().Any())
                personMax = PersonRepository.GetAll().Max(p => p.Id) + 1;
            if (Repository.GetAll().Any())
                groupMax = Repository.GetAll().Max(p => p.Id) + 1;
            return personMax > groupMax ? personMax : groupMax;
        }
    }
}
