﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using WebMeet.Conversations;
using WebMeet.Core.Services.Shared;
using WebMeet.Friends.Acquaintances;
using WebMeet.Groups;
using WebMeet.Persons;

namespace WebMeet.Core.Services.Conversations.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na encji typu <see cref="Conversation"/>
    /// </summary>
    public class ConversationManager : IdentifiableDomainManagerBase<Conversation>, IConversationManager
    {
        protected readonly IRepository<Group, long> GroupRepository;
        protected readonly IRepository<Person, long> PersonRepository;
        protected readonly IRepository<Acquaintance, long> AcquaintanceRepository; 

        /// <summary>
        /// Konstrukotr
        /// </summary>
        /// <param name="repository">Repozytorium typu <see cref="Conversation"/></param>
        /// <param name="groupRepository">Repozytorium typu <see cref="Group"/></param>
        /// <param name="personRepository">Repozytorium typu <see cref="Person"/></param>
        /// <param name="acquaintanceRepository">Repozytorium typu <see cref="Acquaintance"/></param>
        public ConversationManager(IRepository<Conversation, long> repository, 
            IRepository<Group, long> groupRepository, 
            IRepository<Person, long> personRepository, 
            IRepository<Acquaintance, long> acquaintanceRepository) : base(repository)
        {
            GroupRepository = groupRepository;
            PersonRepository = personRepository;
            AcquaintanceRepository = acquaintanceRepository;
        }

        /// <summary>
        /// Metoda tworząca nową rozmowę
        /// </summary>
        /// <param name="callerId">Id osoby dzwoniącej</param>
        /// <param name="personBaseId">Id osoby/grupy do której dzwonimy</param>
        /// <returns>Utowrzoną rozmowę</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy nie należymy do grupy lub nie jesteśmy znajomymi</exception>
        public async Task<Conversation> Call(long callerId, long personBaseId)
        {
            await CheckPersonOrGroup(callerId, personBaseId);
            var conversation = new Conversation
            {
                CallerId = callerId,
                PersonBaseId = personBaseId,
                StartDate = DateTime.Now
            };
            conversation.Id = await Repository.InsertAndGetIdAsync(conversation);
            return conversation;
        }

        /// <summary>
        /// Metoda do zakończenia rozmowy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="conversation">Obiekt rozmowy do zakończenia</param>
        /// <returns>Zaktualizowaną rozmowę</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy osoba nie należała do rozmowy</exception>
        public async Task<Conversation> EndConversation(long personId, Conversation conversation)
        {
            await CheckPersonOrGroup(personId, conversation.PersonBaseId);
            if(await CheckPerson(personId, conversation.PersonBaseId))
                if(conversation.CallerId != personId && conversation.PersonBaseId != personId)
                    throw new UserFriendlyException(L("YouAreNotIncludedInCall"));

            if(conversation.StartDate > DateTime.Now)
                throw new UserFriendlyException(L("EndDateLowerThanStartDate"));

            conversation.EndDate = DateTime.Now;
            await Repository.UpdateAsync(conversation);
            return conversation;
        }

        public void EndConversation(long conversationId)
        {
            Conversation c = Repository.Get(conversationId);
            c.EndDate = DateTime.Now;

            Repository.Update(c);
        }

        private async Task CheckPersonOrGroup(long personId, long personBaseId)
        {
            if(!(await CheckPerson(personId, personBaseId)))
                if(!(await CheckGroup(personId, personBaseId)))
                    throw new UserFriendlyException(L("NotFound", personBaseId.ToString(CultureInfo.InvariantCulture)));                                  
        }

        private async Task<bool> CheckPerson(long personId, long personBaseId)
        {
            var person = await TryGetPerson(personBaseId);
            if (person == null) return false;
            if (AcquaintanceRepository.GetAll().All(a => a.FirstPersonId != personId && a.SecondPersonId != personId))
                throw new UserFriendlyException(L("YouAreNotAcquaintances"));
            return true;
        }

        private async Task<bool> CheckGroup(long personId, long personBaseId)
        {
            var group = await TryGetGroup(personBaseId);
            if (group == null) return false;             
            if (group.GroupUsers.All(gu => gu.PersonId != personId))
                throw new UserFriendlyException(L("YouDontBelongToGroup"));
            return true;
        }

        private async Task<Person> TryGetPerson(long personId)
        {
            return await PersonRepository.FirstOrDefaultAsync(p => p.Id == personId);
        }

        private async Task<Group> TryGetGroup(long groupId)
        {
            return await GroupRepository.FirstOrDefaultAsync(g => g.Id == groupId);
        }
    }
}
