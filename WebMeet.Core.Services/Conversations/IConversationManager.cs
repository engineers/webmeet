﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.UI;
using WebMeet.Conversations;
using WebMeet.Core.Services.Shared;

namespace WebMeet.Core.Services.Conversations
{
    /// <summary>
    /// Interfejs serwisu odpowiedzialnego za operacje na encji typu <see cref="Conversation"/>
    /// </summary>
    public interface IConversationManager : IIdentifiableDomainManagerBase<Conversation>
    {
        /// <summary>
        /// Metoda tworząca nową rozmowę
        /// </summary>
        /// <param name="callerId">Id osoby dzwoniącej</param>
        /// <param name="personBaseId">Id osoby/grupy do której dzwonimy</param>
        /// <returns>Utowrzoną rozmowę</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy nie należymy do grupy lub nie jesteśmy znajomymi</exception>
        Task<Conversation> Call(long callerId, long personBaseId);
        /// <summary>
        /// Metoda do zakończenia rozmowy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="conversation">Obiekt rozmowy do zakończenia</param>
        /// <returns>Zaktualizowaną rozmowę</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy osoba nie należała do rozmowy</exception>
        Task<Conversation> EndConversation(long personId, Conversation conversation);

        void EndConversation(long conversationId);
    }
}
