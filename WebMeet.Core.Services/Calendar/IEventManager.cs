﻿using System.Threading.Tasks;
using WebMeet.Calendar;
using WebMeet.Core.Services.Shared;

namespace WebMeet.Core.Services.Calendar
{
    /// <summary>
    /// Interfejs serwisu odpowiedzialnego za operacje na encji typu <see cref="Event"/>
    /// </summary>
    public interface IEventManager : IIdentifiableDomainManagerBase<Event>
    {
        /// <summary>
        /// Tworzy wydarzenie dla osoby
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="event">Wydarzenie do utworzenia</param>
        /// <returns>Utworzone wydarzenie</returns>
        Task<Event> CreateForPerson(long personId, Event @event);
        /// <summary>
        /// Tworzy wydarzenie dla grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="event">Wydarzenie do utworzenia</param>
        /// <returns>Utworzone wydarzenie</returns>
        Task<Event> CreateForGroup(long personId, Event @event);
        /// <summary>
        /// Aktualizuje dane wydarzenie osoby
        /// </summary>
        /// <param name="personId">Id osoby wykounjącej operację</param>
        /// <param name="event">Wydarzenie z nowymi danymi</param>
        /// <returns>Zaktualizowane wydarzenie</returns>
        Task<Event> UpdateForPerson(long personId, Event @event);
        /// <summary>
        /// Aktualizuje dane wydarzenie grupy
        /// </summary>
        /// <param name="personId">Id osoby wykounjącej operację</param>
        /// <param name="event">Wydarzenie z nowymi danymi</param>
        /// <returns>Zaktualizowane wydarzenie</returns>
        Task<Event> UpdateForGroup(long personId, Event @event);
        /// <summary>
        /// Usuwa wydarzenie osoby
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="event">Wydarzenie do usunięcia</param>
        Task DeleteForPerson(long personId, Event @event);
        /// <summary>
        /// Usuwa wydarzenie grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="event">Wydarzenie do usunięcia</param>
        Task DeleteForGroup(long personId, Event @event);
    }
}
