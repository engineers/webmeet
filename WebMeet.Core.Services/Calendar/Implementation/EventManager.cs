﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using WebMeet.Calendar;
using WebMeet.Core.Services.Shared;
using WebMeet.Friends.Acquaintances;
using WebMeet.Groups;

namespace WebMeet.Core.Services.Calendar.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na encji typu <see cref="Event"/>
    /// </summary>
    public class EventManager : IdentifiableDomainManagerBase<Event>, IEventManager
    {
        protected readonly IRepository<GroupUser, long> GroupUserRepository;
        protected readonly IRepository<Group, long> GroupRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="repository">Repozytorium typu <see cref="Event"/></param>
        /// <param name="groupUserRepository">Repozytorium typu <see cref="GroupUser"/></param>
        /// <param name="groupRepository">Repozytorium typu <see cref="Group"/></param>
        public EventManager(IRepository<Event, long> repository, 
            IRepository<GroupUser, long> groupUserRepository, 
            IRepository<Group, long> groupRepository) : base(repository)
        {
            GroupUserRepository = groupUserRepository;
            GroupRepository = groupRepository;
        }

        /// <summary>
        /// Tworzy wydarzenie dla osoby
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="event">Wydarzenie do utworzenia</param>
        /// <returns>Utworzone wydarzenie</returns>
        public async Task<Event> CreateForPerson(long personId, Event @event)
        {
            CheckEquality(personId, @event.PersonBaseId);
            return await Repository.InsertAsync(@event);
        }

        /// <summary>
        /// Tworzy wydarzenie dla grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="event">Wydarzenie do utworzenia</param>
        /// <returns>Utworzone wydarzenie</returns>
        public async Task<Event> CreateForGroup(long personId, Event @event)
        {
            await CheckIfPersonBelongToGroup(personId, @event.PersonBaseId);
            return await Repository.InsertAsync(@event);
        }

        /// <summary>
        /// Aktualizuje dane wydarzenie grupy
        /// </summary>
        /// <param name="personId">Id osoby wykounjącej operację</param>
        /// <param name="event">Wydarzenie z nowymi danymi</param>
        /// <returns>Zaktualizowane wydarzenie</returns>
        public async Task<Event> UpdateForGroup(long personId, Event @event)
        {
            await CheckIfPersonBelongToGroup(personId, @event.PersonBaseId);
            return await Repository.UpdateAsync(@event);
        }

        /// <summary>
        /// Aktualizuje dane wydarzenie osoby
        /// </summary>
        /// <param name="personId">Id osoby wykounjącej operację</param>
        /// <param name="event">Wydarzenie z nowymi danymi</param>
        /// <returns>Zaktualizowane wydarzenie</returns>
        public async Task<Event> UpdateForPerson(long personId, Event @event)
        {
            CheckEquality(personId, @event.PersonBaseId);
            return await Repository.UpdateAsync(@event);
        }

        /// <summary>
        /// Usuwa wydarzenie osoby
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="event">Wydarzenie do usunięcia</param>
        public async Task DeleteForPerson(long personId, Event @event)
        {
            CheckEquality(personId, @event.PersonBaseId);
            await Repository.DeleteAsync(@event);
        }

        /// <summary>
        /// Usuwa wydarzenie grupy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="event">Wydarzenie do usunięcia</param>
        public async Task DeleteForGroup(long personId, Event @event)
        {
            await CheckIfPersonBelongToGroup(personId, @event.PersonBaseId);
            await Repository.DeleteAsync(@event);
        }

        private async Task CheckIfPersonBelongToGroup(long personId, long groupId)
        {
            if ((await GroupUserRepository.FirstOrDefaultAsync(gu => gu.PersonId == personId && gu.GroupId == groupId)) == null)
                throw new UserFriendlyException(L("YouDontBelongToGroup"));
        }

        private void CheckEquality(long id1, long id2)
        {
            if (id1 != id2)
                throw new UserFriendlyException(L("InvalidModel"));
        } 
    }
}
