﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.UI;
using WebMeet.Core.Services.Shared;
using WebMeet.Friends.Blacklists;
using WebMeet.Persons;

namespace WebMeet.Core.Services.Blacklist
{
    /// <summary>
    /// Interfejs serwisu odpowiedzialnego za operacje na encji typu <see cref="BlacklistItem"/>
    /// </summary>
    public interface IBlacklistItemManager : IIdentifiableDomainManagerBase<BlacklistItem>
    {
        /// <summary>
        /// Usuwa osobę z czarnej listy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="item">Obiekt z czarnej listy do usunięcia</param>
        /// <exception cref="UserFriendlyException">Rzucan gdy nie jesteśmy właścicielem czarnej listy</exception>
        void Delete(long personId, BlacklistItem item);
        /// <summary>
        /// Dodaje osobę do czarnej listy
        /// </summary>
        /// <param name="ownerId">Id osoby wykonującej operację</param>
        /// <param name="person">Osoba do czarnej listy</param>
        /// <returns>Utworzony obiekt na czarnej liście</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik znajduje się już na czarnej liście</exception>
        Task<BlacklistItem> AddPersonToBlacklist(long ownerId, Person person);
    }
}
