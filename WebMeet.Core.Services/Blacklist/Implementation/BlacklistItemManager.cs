﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using CuttingEdge.Conditions;
using WebMeet.Core.Services.Shared;
using WebMeet.Friends.Acquaintances;
using WebMeet.Friends.Blacklists;
using WebMeet.Friends.Invitations;
using WebMeet.Persons;

namespace WebMeet.Core.Services.Blacklist.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na encji typu <see cref="BlacklistItem"/>
    /// </summary>
    public class BlacklistItemManager : IdentifiableDomainManagerBase<BlacklistItem>, IBlacklistItemManager
    {
        protected readonly IRepository<Acquaintance, long> AcquaintanceRepository;
        protected readonly IRepository<Invitation, long> InvitationRepository; 

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="repository">Repozytorium typu <see cref="BlacklistItem"/></param>
        /// <param name="acquaintanceRepository">Repozytorium typu <see cref="Acquaintance"/></param>
        /// <param name="invitationRepository">Repozytorium typu <see cref="Invitation"/></param>
        public BlacklistItemManager(IRepository<BlacklistItem, long> repository, 
            IRepository<Acquaintance, long> acquaintanceRepository, 
            IRepository<Invitation, long> invitationRepository) : base(repository)
        {
            AcquaintanceRepository = acquaintanceRepository;
            InvitationRepository = invitationRepository;
        }

        /// <summary>
        /// Usuwa osobę z czarnej listy
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="item">Obiekt z czarnej listy do usunięcia</param>
        /// <exception cref="UserFriendlyException">Rzucan gdy nie jesteśmy właścicielem czarnej listy</exception>
        public void Delete(long personId, BlacklistItem item)
        {
            Condition.Requires(item).IsNotNull();

            if(item.OwnerId != personId)
                throw new UserFriendlyException(L("YouAreNotOwnerOfBlacklist"));
            Repository.Delete(item);
        }

        /// <summary>
        /// Dodaje osobę do czarnej listy
        /// </summary>
        /// <param name="ownerId">Id osoby wykonującej operację</param>
        /// <param name="person">Osoba do czarnej listy</param>
        /// <returns>Utworzony obiekt na czarnej liście</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik znajduje się już na czarnej liście</exception>
        public async Task<BlacklistItem> AddPersonToBlacklist(long ownerId, Person person)
        {
            Condition.Requires(person).IsNotNull();

            if(ownerId == person.Id)
                throw new UserFriendlyException(L("CannotAddSelfToBlacklist"));
            if(Repository.FirstOrDefault(item => item.OwnerId == ownerId && item.PersonId == person.Id ) != null)
                throw new UserFriendlyException(L("Duplicate"));

            await DeleteAcquaintanceBetweenUsers(ownerId, person.Id);
            await DeleteAllInvitationsBetweenUsers(ownerId, person.Id);
            var blacklistItem = new BlacklistItem
            {
                PersonId = person.Id,
                OwnerId = ownerId
            };
            return Repository.Insert(blacklistItem);
        }

        protected async Task DeleteAcquaintanceBetweenUsers(long firstPersonId, long secondPersonId)
        {
            await AcquaintanceRepository.DeleteAsync(
                acq => (acq.FirstPersonId == firstPersonId && acq.SecondPersonId == secondPersonId) ||
                       (acq.FirstPersonId == secondPersonId && acq.SecondPersonId == firstPersonId));
        }

        protected async Task DeleteAllInvitationsBetweenUsers(long firstPersonId, long secondPersonId)
        {
            await
                InvitationRepository.DeleteAsync(
                    invitation => (invitation.SenderId == firstPersonId && invitation.ReceiverId == secondPersonId) ||
                                  (invitation.SenderId == secondPersonId && invitation.ReceiverId == firstPersonId));
        }
    }
}
