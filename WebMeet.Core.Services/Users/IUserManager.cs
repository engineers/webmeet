﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMeet.Core.Services.Shared;
using WebMeet.Users;

namespace WebMeet.Core.Services.Users
{
    /// <summary>
    /// Interfejs dla serwisu odpowiedzialnego za operacje na encji <see cref="User"/>
    /// </summary>
    public interface IUserManager : IIdentifiableDomainManagerBase<User>
    {
        /// <summary>
        /// Aktualizuje użytkownika w obiekcie <paramref name="user"/>
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="user">Użytkownik z nowymi danymi</param>
        /// <returns>Zaktualizowany użytkownik</returns>
        Task<User> Update(long personId, User user);

        User Get(long personId);
    }
}
