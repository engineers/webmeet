using System.Threading.Tasks;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Caching;
using Microsoft.AspNet.Identity;
using WebMeet.Authorization.Roles;
using WebMeet.MultiTenancy;
using WebMeet.Users;

namespace WebMeet.Core.Services.Users.Abp
{
    /// <summary>
    /// Implementuje 'User Store' z ASP.NET Identity Framework
    /// </summary>
    public class UserStore : AbpUserStore<Tenant, Role, User>, IUserSecurityStampStore<User,long>
    {
        public UserStore(
            IRepository<User, long> userRepository,
            IRepository<UserLogin, long> userLoginRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<Role> roleRepository,
            IRepository<UserPermissionSetting, long> userPermissionSettingRepository,
            IUnitOfWorkManager unitOfWorkManager,
            ICacheManager cacheManager)
            : base(
              userRepository,
              userLoginRepository,
              userRoleRepository,
              roleRepository,
              userPermissionSettingRepository,
              unitOfWorkManager,
              cacheManager)
        {
        }

        public System.Threading.Tasks.Task<string> GetSecurityStampAsync(User user)
        {
            return Task.FromResult(string.Empty);
        }

        public System.Threading.Tasks.Task SetSecurityStampAsync(User user, string stamp)
        {
            return Task.FromResult(0);
        }
    }
}