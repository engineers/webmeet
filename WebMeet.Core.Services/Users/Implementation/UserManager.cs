﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using CuttingEdge.Conditions;
using WebMeet.Core.Services.Shared;
using WebMeet.Users;

namespace WebMeet.Core.Services.Users.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na encji <see cref="User"/>
    /// </summary>
    public class UserManager : IdentifiableDomainManagerBase<User>, IUserManager
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="repository">Repozytorium typu <see cref="User"/></param>
        public UserManager(IRepository<User, long> repository) : base(repository)
        {
        }

        /// <summary>
        /// Aktualizuje użytkownika w obiekcie <paramref name="user"/>
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="user">Użytkownik z nowymi danymi</param>
        /// <returns>Zaktualizowany użytkownik</returns>
        public async Task<User> Update(long personId, User user)
        {
            Condition.Requires(user).IsNotNull();
            if(personId != user.Id)
                throw new UserFriendlyException(L("YouAreNotResourceOwner"));
            await CheckEmailUniqueness(user.EmailAddress, user.Id);
            await CheckUserNameUniqueness(user.UserName, user.Id);
            return await Repository.UpdateAsync(user); 
        }

        public User Get(long personId)
        {
            return Repository.Get(personId);
        }

        private async Task CheckEmailUniqueness(string email, long exceptUserId)
        {
            var userWithDuplicateEmail = await Repository.FirstOrDefaultAsync(user => user.EmailAddress == email && user.Id != exceptUserId);
            if(userWithDuplicateEmail != null)
                throw new UserFriendlyException(L("EmailExistYet"));
        }

        private async Task CheckUserNameUniqueness(string login, long exceptUserId)
        {
            var userWithDuplicateEmail = await Repository.FirstOrDefaultAsync(user => user.UserName == login && user.Id != exceptUserId);
            if (userWithDuplicateEmail != null)
                throw new UserFriendlyException(L("UsernameExistYet"));
        }
    }
}
