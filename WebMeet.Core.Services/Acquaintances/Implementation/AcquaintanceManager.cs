﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using CuttingEdge.Conditions;
using WebMeet.Core.Services.Shared;
using WebMeet.Friends.Acquaintances;

namespace WebMeet.Core.Services.Acquaintances.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na encji typu <see cref="Acquaintance"/>
    /// </summary>
    public class AcquaintanceManager :IdentifiableDomainManagerBase<Acquaintance>, IAcquaintanceManager
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="repository">Repozytorium typu <see cref="Acquaintance"/></param>
        public AcquaintanceManager(IRepository<Acquaintance, long> repository) : base(repository)
        {
        }

        /// <summary>
        /// Usuwa znajomość
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="acquaintance">Znajomość do usunięcia</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy znajomość nie istnieje</exception>
        public void Delete(long personId, Friends.Acquaintances.Acquaintance acquaintance)
        {
            Condition.Requires(acquaintance).IsNotNull();

            if(acquaintance.FirstPersonId != personId && acquaintance.SecondPersonId != personId)
                throw new UserFriendlyException(L("DeleteNotSelfAcquaintance"));
            Repository.Delete(acquaintance);
        }
    }
}
