﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.UI;
using WebMeet.Core.Services.Shared;
using WebMeet.Friends.Acquaintances;

namespace WebMeet.Core.Services.Acquaintances
{
    /// <summary>
    /// Interfejs serwisu odpowiedzialnego za operacje na encji typu <see cref="Acquaintance"/>
    /// </summary>
    public interface IAcquaintanceManager : IIdentifiableDomainManagerBase<Acquaintance>
    {
        /// <summary>
        /// Usuwa znajomość
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="acquaintance">Znajomość do usunięcia</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy znajomość nie istnieje</exception>
        void Delete(long personId, Acquaintance acquaintance);
    }
}
