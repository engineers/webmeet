﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Services;

namespace WebMeet.Core.Services.Shared
{
    /// <summary>
    /// Klasa bazowa dla serwisów domenowych
    /// </summary>
    public class DomainManagerBase : DomainService
    {
        public DomainManagerBase()
        {
            LocalizationSourceName = WebMeetConsts.LocalizationSourceName;
        }
    }
}
