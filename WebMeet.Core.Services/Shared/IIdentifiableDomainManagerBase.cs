﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Services;
using Abp.UI;

namespace WebMeet.Core.Services.Shared
{
    /// <summary>
    /// Klasa bazowa dla serwisów domenowych reprezentujących operacje na encji z identyfikatorem
    /// </summary>
    /// <typeparam name="T">Typ encji</typeparam>
    public interface IIdentifiableDomainManagerBase<T> : IDomainService
        where T : class, IEntity<long>
    {
        /// <summary>
        /// Pobiera obiekt o podanym identyfikatorze, jśli nie istnieje rzuca wyjątek typu {UserFriendlyException}
        /// </summary>
        /// <exception cref="UserFriendlyException">Rzucany gdy obiekt nie istnieje</exception>
        Task<T> GetAsync(long id);
    }
}
