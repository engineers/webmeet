﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.UI;

namespace WebMeet.Core.Services.Shared
{
    /// <summary>
    /// Klasa bazowa dla serwisów domenowych reprezentujących operacje na encji z identyfikatorem
    /// </summary>
    /// <typeparam name="T">Typ encji</typeparam>
    public class IdentifiableDomainManagerBase<T> : DomainManagerBase, IIdentifiableDomainManagerBase<T>
        where T : class, IEntity<long>
    {
        protected readonly IRepository<T, long> Repository; 

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="repository">Repozytorium typu <typeparamref name="T"/></param>
        public IdentifiableDomainManagerBase(IRepository<T, long> repository)
        {
            Repository = repository;
        }

        /// <summary>
        /// Pobiera obiekt o podanym identyfikatorze
        /// </summary>
        /// <exception cref="UserFriendlyException">Rzucany gdy obiekt nie istnieje</exception>
        public async Task<T> GetAsync(long id)
        {
            var entity = await Repository.FirstOrDefaultAsync(id);
            if(entity == null)
                throw new UserFriendlyException(L("ErrorDetected"), L("NotFound", id));
            return entity;
        }
    }
}
