﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Modules;

namespace WebMeet.Core.Services
{
    /// <summary>
    /// Klasa reprezentująca moduł serwisów domenowych
    /// </summary>
    [DependsOn(typeof(WebMeetCoreEntitiesModule))]
    public class WebMeetCoreServicesModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
