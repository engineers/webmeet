﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.UI;
using WebMeet.Core.Services.Shared;
using WebMeet.Messages;

namespace WebMeet.Core.Services.Messages
{
    /// <summary>
    /// Interfejs dla serwisu odpowiedzialnego za operacje na encji typu <see cref="Message"/>
    /// </summary>
    public interface IMessageManager : IIdentifiableDomainManagerBase<Message>
    {
        /// <summary>
        /// Tworzy wiadomość
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="message">Wiadomość do utworzenia</param>
        /// <returns>Utworzoną wiadomość</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy id osoby wykonującej operację jest rózne od nadawcy wiadomości</exception>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie należy do grupy(odbiorca wiadomości)</exception>
        /// <exception cref="UserFriendlyException">Rzucany gdy nadwaca i odbiorca nie są znajomymi</exception>
        Task<Message> Create(long personId, Message message);
    }
}
