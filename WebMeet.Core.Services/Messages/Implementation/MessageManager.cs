﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using WebMeet.Core.Services.Shared;
using WebMeet.Friends.Acquaintances;
using WebMeet.Groups;
using WebMeet.Messages;

namespace WebMeet.Core.Services.Messages.Implementation
{
    /// <summary>
    /// Serwis odpowiedzilany za operacje na encji typu <see cref="Message"/>
    /// </summary>
    public class MessageManager : IdentifiableDomainManagerBase<Message>, IMessageManager
    {
        protected readonly IRepository<Group, long> GroupRepository;
        protected readonly IRepository<Acquaintance, long> AcquaintanceRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="repository">Repozytorium typu <see cref="Message"/></param>
        /// <param name="groupRepository">Repozytorium typu <see cref="Group"/></param>
        /// <param name="acquaintanceRepository">Repozytorium typu <see cref="Acquaintance"/></param>
        public MessageManager(IRepository<Message, long> repository, 
            IRepository<Group, long> groupRepository, 
            IRepository<Acquaintance, long> acquaintanceRepository) : base(repository)
        {
            GroupRepository = groupRepository;
            AcquaintanceRepository = acquaintanceRepository;
        }

        /// <summary>
        /// Tworzy wiadomość
        /// </summary>
        /// <param name="personId">Id osoby wykonującej operację</param>
        /// <param name="message">Wiadomość do utworzenia</param>
        /// <returns>Utworzoną wiadomość</returns>
        /// <exception cref="UserFriendlyException">Rzucany gdy id osoby wykonującej operację jest rózne od nadawcy wiadomości</exception>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie należy do grupy(odbiorca wiadomości)</exception>
        /// <exception cref="UserFriendlyException">Rzucany gdy nadwaca i odbiorca nie są znajomymi</exception>
        public async Task<Message> Create(long personId, Message message)
        {
            if(personId != message.SenderId)
                throw new ApplicationException("personId != message.SenderId");

            await CheckIfPersonBelongToGroup(personId, message.ReceiverId);
            await CheckIfPersonsAreAcquaintances(personId, message.ReceiverId);

            message.Id =await Repository.InsertAndGetIdAsync(message);
            return message;
        }

        private async Task CheckIfPersonBelongToGroup(long personId, long groupId)
        {
            var group = await GroupRepository.FirstOrDefaultAsync(g => g.Id == groupId);
            if(group != null && group.GroupUsers.All(gu=> gu.PersonId != personId))
                throw new UserFriendlyException(L("YouAreNotBelongToGroup"));
        }

        private async Task CheckIfPersonsAreAcquaintances(long firstPersonId, long secondPersonId)
        {
            var group = await GroupRepository.FirstOrDefaultAsync(g => g.Id == firstPersonId || g.Id == secondPersonId);

            if (group == null)
            {
                var acquaintance =
                await AcquaintanceRepository.FirstOrDefaultAsync(
                    a => (a.FirstPersonId == firstPersonId && a.SecondPersonId == secondPersonId) ||
                         (a.FirstPersonId == secondPersonId && a.SecondPersonId == firstPersonId));
                if(acquaintance==null)
                    throw new UserFriendlyException(L("YouAreNotAcquaintances"));
            }
        }

    }
}
