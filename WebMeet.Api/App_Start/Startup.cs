﻿using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using WebMeet.Api;
using WebMeet.Api.Controllers;

[assembly: OwinStartup(typeof(Startup))]
namespace WebMeet.Api
{
    public class Startup
    {
        /// <summary>
        /// Metoda konfigurująca aplikację
        /// </summary>
        /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            //app.UseOAuthBearerAuthentication(AccountController.OAuthBearerOptions);

            app.ConfigureOAuth();

            config.Register();

            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(config);
        }
    }
}