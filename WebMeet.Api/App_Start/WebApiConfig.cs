﻿using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.Web.Http.Cors;

namespace WebMeet.Api
{
    /// <summary>
    /// Klasa zawierająca metody konfigurujące WebApi
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Metoda konfigurująca WebApi, np. routing, cors
        /// </summary>
        /// <param name="config"></param>
        public static void Register(this HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            var cors = new EnableCorsAttribute("*", "*", "*");

            config.EnableCors(cors);
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new {id = RouteParameter.Optional}
                );

            var camelCaseContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = camelCaseContractResolver;
            config.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
        }
    }
}