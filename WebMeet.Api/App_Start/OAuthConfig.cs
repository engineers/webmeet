﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using WebMeet.Api.Providers;

namespace WebMeet.Api
{
    /// <summary>
    /// Klasa zawierająca metody rozszerzające dla OAuth
    /// </summary>
    public static class OAuthConfig
    {
        /// <summary>
        /// Metoda konfigurująca OAuth
        /// </summary>
        /// <param name="app"></param>
        public static void ConfigureOAuth(this IAppBuilder app)
        {
            var options = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth2/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleOAuthAuthorizationServerProvider()
            };
            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}