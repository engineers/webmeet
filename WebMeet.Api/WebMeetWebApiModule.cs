﻿using System.Reflection;
using System.Web.Http;
using Abp.Application.Services;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.Modules;
using Abp.Web;
using Abp.WebApi;
using Abp.WebApi.Controllers.Dynamic.Builders;
using Abp.WebApi.Controllers.Filters;
using WebMeet.Calendar;
using WebMeet.Conversations;
using WebMeet.Friends.Acquaintance;
using WebMeet.Friends.Blacklist;
using WebMeet.Friends.Invitation;
using WebMeet.Groups;
using WebMeet.Messages;
using WebMeet.Persons;

namespace WebMeet.Api
{
    /// <summary>
    /// Klasa reprezentująca moduł WebApi
    /// </summary>
    [DependsOn(typeof(AbpWebApiModule), typeof(WebMeetDataModule), typeof(WebMeetApplicationModule))]
    public class WebMeetWebApiModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.Languages.Add(new LanguageInfo("pl", "Polski", "famfamfam-flag-england", true));
            Configuration.Localization.Languages.Add(new LanguageInfo("en", "English", "famfamfam-flag-pl"));
            Configuration.Localization.Languages.Add(new LanguageInfo("tr", "Türkçe", "famfamfam-flag-tr"));
            Configuration.Localization.Languages.Add(new LanguageInfo("zh-CN", "简体中文", "famfamfam-flag-cn"));
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(WebMeetApplicationModule).Assembly, "app")
                .Build();

            DynamicApiControllerBuilder
                .ForAll<IInvitationAppService>(typeof(WebMeetApplicationModule).Assembly, "app")
                .Build();

            DynamicApiControllerBuilder
                .ForAll<IPersonAppService>(typeof(WebMeetApplicationModule).Assembly, "app")
                .Build();

            DynamicApiControllerBuilder
                .ForAll<IAcquaintanceAppService>(typeof(WebMeetApplicationModule).Assembly, "app")
                .Build();

            DynamicApiControllerBuilder
                .ForAll<IBlacklistAppService>(typeof(WebMeetApplicationModule).Assembly, "app")
                .Build();

            DynamicApiControllerBuilder
                .ForAll<IEventAppService>(typeof(WebMeetApplicationModule).Assembly, "app")
                .Build();

            DynamicApiControllerBuilder
                .ForAll<IGroupAppService>(typeof(WebMeetApplicationModule).Assembly, "app")
                .Build();

            DynamicApiControllerBuilder
                .ForAll<IMessageAppService>(typeof(WebMeetApplicationModule).Assembly, "app")
                .Build();

            DynamicApiControllerBuilder
                .ForAll<IConversationAppService>(typeof(WebMeetApplicationModule).Assembly, "app")
                .Build();

            Configuration.Modules.AbpWebApi().HttpConfiguration.Filters.Add(new HostAuthenticationFilter("Bearer"));
        }

        public override void PostInitialize()
        {
            Configuration.Modules.AbpWebApi().HttpConfiguration.EnsureInitialized();
        }
    }
}
