﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Abp.Authorization.Users;
using Abp.Dependency;
using Abp.Localization;
using Abp.Localization.Sources;
using Abp.UI;
using Castle.Core.Logging;
using Microsoft.Owin.Security.OAuth;
using WebMeet.Account;
using WebMeet.Account.Dto;
using WebMeet.Authorization.Roles;
using WebMeet.Core.Services;
using WebMeet.MultiTenancy;
using WebMeet.Users;

namespace WebMeet.Api.Providers
{
    /// <summary>
    /// Klasa reprezentująca logowanie za pomocą OAuth
    /// </summary>
    public class SimpleOAuthAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly ILogger _logger;
        private readonly ILocalizationManager _localizationManager;
        private readonly IAccountAppService _accountAppService;

        public SimpleOAuthAuthorizationServerProvider()
        {
            _logger = IocManager.Instance.Resolve<ILogger>();
            _localizationManager = IocManager.Instance.Resolve<ILocalizationManager>();
            _accountAppService = IocManager.Instance.Resolve<IAccountAppService>();
        }

        /// <summary>
        /// Metoda sprawdzająca poprawnośc danych podczas logowania
        /// </summary>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            if (context.UserName == null || context.Password == null)
            {
               throw new UserFriendlyException(L("LoginFailed"), L("InvalidUserNameOrPassword"));
            }

            var identity = await
                _accountAppService.Login(new LoginInput
                {
                    UsernameOrEmailAddress = context.UserName,
                    Password = context.Password,
                    AuthenticationType = context.Options.AuthenticationType
                });

            context.Validated(identity);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // we have only one client - ourselves
            context.Validated();
            return Task.FromResult(0);
        }

        public string L(string code)
        {
            return _localizationManager.GetString(WebMeetConsts.LocalizationSourceName, code);
        }
    }
}