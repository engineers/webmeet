﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Abp.Dependency;
using Abp.Web;
using Castle.Facilities.Logging;

namespace WebMeet.Api
{
    /// <summary>
    /// Klasa reprezentująca aplikację
    /// </summary>
    public class WebApiApplication : AbpWebApplication
    {
        protected void Application_Start()
        {
            //GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected override void Application_Start(object sender, EventArgs e)
        {
            IocManager.Instance.IocContainer.AddFacility<LoggingFacility>(
                f => f.UseLog4Net().WithConfig("log4net.config"));
            base.Application_Start(sender, e);
        }
    }
}
