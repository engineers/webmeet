﻿using System.Collections.Generic;
using System.Linq;
using Abp.Dependency;
using Castle.Core.Internal;
using Newtonsoft.Json;
using WebMeet.Core.Services.Conversations;
using WebMeet.Core.Services.Users;
using WebMeet.Core.Services.Users.Abp;
using WebMeet.Core.Services.Users.Implementation;
using WebMeet.Helpers.Chat;
using WebMeet.Helpers.Chat.Message;
using WebMeet.Users;

namespace WebMeet.Api.Chat
{
    class ChatSingleton
    {
        private static ChatSingleton _instance;

        private readonly Dictionary<long, UserWebSocketHandler> _connections;
        private readonly Dictionary<long, Conversation> _conversations; 
        private readonly IUserManager _userManager; 
        private readonly IConversationManager _conversationManager; 

        private ChatSingleton()
        {
            _connections = new Dictionary<long, UserWebSocketHandler>();
            _conversations = new Dictionary<long, Conversation>();

            _userManager = IocManager.Instance.Resolve<IUserManager>();
            _conversationManager = IocManager.Instance.Resolve<IConversationManager>();
        }

        public static ChatSingleton Instance
        {
            get { return _instance ?? (_instance = new ChatSingleton()); }
        }

        public void ConnectUser(UserWebSocketHandler user)
        {
            _connections.Add(user.User.Id, user);

            if (user.User.Id > 0)
            {
                User u = _userManager.Get(user.User.Id);
                user.User.Name = u.Name + " " + u.Surname;
            }
        }

        public void DisconnectUser(long identity)
        {
            _connections.Remove(identity);

            foreach (var c in _conversations.Where(c => !c.Value.ended).Select(c => c.Value))
            {
                Interlocutor interlocutor = c.Interlocutors.Find(i => i.Id == identity && i.Status != InterlocutorStatus.Rejected && i.Status != InterlocutorStatus.Stopped);
                c.Interlocutors.Find(i => i.Id == identity).Status = InterlocutorStatus.Rejected;
                if (interlocutor != null)
                {
                    interlocutor.Status = InterlocutorStatus.Stopped;

                    if (
                        c.Interlocutors.Where(
                            i => i.Status != InterlocutorStatus.Rejected && i.Status != InterlocutorStatus.Stopped)
                            .ToList()
                            .Count < 2)
                    {
                        _conversationManager.EndConversation(c.Id);
                        c.ended = true;
                        break;
                    }
                }
            }
        }

        public void CreateConversation(Message message)
        {
            Conversation conversation = new Conversation()
            {
                Caller = message.SenderId,
                Destination = message.Talk.ReceiverId,
                Id = message.Talk.Id,
                Interlocutors = new List<Interlocutor>()
            };

            Interlocutor caller = new Interlocutor(message.SenderId);
            caller.Status = InterlocutorStatus.Accepted;

            conversation.Interlocutors.Add(caller);
            conversation.Interlocutors.AddRange(message.ReceiversIdentities.Select(i => new Interlocutor(i)));

            _conversations.Add(conversation.Id, conversation);

            foreach (var i in conversation.Interlocutors)
            {
                i.Name = _connections[i.Id].User.Name;
            }

            ForwardMessage(message);
        }

        public void JoinGuestToConversation(Message message)
        {

            if (_conversations.ContainsKey(message.Talk.Id) && !_conversations[message.Talk.Id].ended)
            {
                Message m = new Message();
                Conversation c = _conversations[message.Talk.Id];

                Interlocutor guest = new Interlocutor(message.SenderId);
                guest.Status = InterlocutorStatus.Accepted;
                guest.Name = message.Talk.Name;
                c.Interlocutors.Add(guest);

                m.MessageType = MessageType.Talk;
                m.Talk = new Talk()
                {
                    Id = message.Talk.Id,
                    Type = TType.TalkInfoInterlocutors,
                    TalkInfoInterlocutors = c.Interlocutors
                };

                SendMessage(message.SenderId, m);


                m = new Message();

                m.SenderId = message.SenderId;
                m.MessageType = MessageType.Talk;
                m.Talk = new Talk
                {
                    Id = message.Talk.Id,
                    Type = TType.AcceptTalk,
                    Name = message.Talk.Name
                };

                List<long> receivers = c.Interlocutors
                    .Where(i => i.Id != m.SenderId && i.Status == InterlocutorStatus.Accepted)
                    .Select(i => i.Id).ToList();

                SendMessage(receivers, m);
            }
            else
            {
                Message m = new Message();
                m.MessageType = MessageType.Talk;
                m.Talk = new Talk();
                m.Talk.Type = TType.Error;

                SendMessage(message.SenderId, m);
            }

        }

        public void AcceptConversation(Message message)
        {
            Message m = new Message();

            m.SenderId = message.SenderId;
            m.MessageType = MessageType.Talk;
            m.Talk = new Talk
            {
                Id = message.Talk.Id,
                Type = TType.AcceptTalk
            };

            Conversation c = _conversations[message.Talk.Id];
            c.Interlocutors.Find(i => i.Id == message.SenderId).Status = InterlocutorStatus.Accepted;
            List<long> receivers = c.Interlocutors
                                .Where(i => i.Id != m.SenderId && i.Status == InterlocutorStatus.Accepted)
                                .Select(i => i.Id).ToList();

            SendMessage(receivers, m);

            m = new Message();

            m.MessageType = MessageType.Talk;
            m.Talk = new Talk()
            {
                Id = message.Talk.Id,
                Type = TType.Status,
                Status = c.Interlocutors
            };

            SendMessage(message.SenderId, m);
        }

        public void RejectConversation(Message message)
        {
            Message m = new Message();

            m.SenderId = message.SenderId;
            m.MessageType = MessageType.Talk;
            m.Talk = new Talk
            {
                Id = message.Talk.Id,
                Type = TType.RejectTalk
            };

            Conversation c = _conversations[message.Talk.Id];
            List<long> receivers = c.Interlocutors
                                .Where(i => i.Id != m.SenderId && i.Status == InterlocutorStatus.Accepted)
                                .Select(i => i.Id).ToList();

            SendMessage(receivers, m);
            Interlocutor interlocutor = c.Interlocutors.Find(i => i.Id == message.SenderId && i.Status != InterlocutorStatus.Rejected && i.Status != InterlocutorStatus.Stopped);
            c.Interlocutors.Find(i => i.Id == message.SenderId).Status = InterlocutorStatus.Rejected;

            if (interlocutor != null)
            {
                interlocutor.Status = InterlocutorStatus.Stopped;

                if (
                    c.Interlocutors.Where(
                        i => i.Status != InterlocutorStatus.Rejected && i.Status != InterlocutorStatus.Stopped)
                        .ToList()
                        .Count < 2)
                {
                    _conversationManager.EndConversation(c.Id);
                    c.ended = true;
                }
            }

            m = new Message();

            m.MessageType = MessageType.Talk;
            m.Talk = new Talk()
            {
                Id = message.Talk.Id,
                Type = TType.Status,
                Status = c.Interlocutors
            };

            SendMessage(message.SenderId, m);
        }

        public void StopTalk(Message message)
        {
            Conversation con = _conversations[message.Talk.Id];


            Interlocutor interlocutor = con.Interlocutors.Find(i => i.Id == message.SenderId && i.Status != InterlocutorStatus.Rejected && i.Status != InterlocutorStatus.Stopped);
            con.Interlocutors.Find(i => i.Id == message.SenderId).Status = InterlocutorStatus.Stopped;

            if (interlocutor != null)
            {
                interlocutor.Status = InterlocutorStatus.Stopped;

                if (
                    con.Interlocutors.Where(
                        i => i.Status != InterlocutorStatus.Rejected && i.Status != InterlocutorStatus.Stopped)
                        .ToList()
                        .Count < 2)
                {
                    _conversationManager.EndConversation(con.Id);
                    con.ended = true;
                }
            }

            TalkForwardMessage(message);
        }

        public void TalkForwardMessage(Message message)
        {
            Conversation c = _conversations[message.Talk.Id];
            List<long> receivers = c.Interlocutors.Where(i => i.Id != message.SenderId).Select(i => i.Id).ToList();

            SendMessage(receivers, message);
        }

        public void ForwardMessage(Message message)
        {
            List<long> receivers = message.ReceiversIdentities.Where(i => i != message.SenderId).ToList();

            SendMessage(receivers, message);
        }

        public void BroadcastUserStatusChanged(long id, UserStatus userStatus)
        {
            Message message = new Message();

            message.MessageType = MessageType.UsersStatusChanged;
            message.UsersStatusChanged = new List<UserStatusChanged>();
            message.UsersStatusChanged.Add(new UserStatusChanged(id, userStatus));

            List<long> receivers = _connections.Where(c => c.Key != id).Select(c => c.Key).ToList();

            SendMessage(receivers, message);
        }

        public void BroadcastUsersStatus(long from)
        {
            Message message = new Message();

            message.MessageType = MessageType.UsersStatusChanged;
            message.UsersStatusChanged = _connections.Where(c => c.Key != from).Select(c => new UserStatusChanged(c.Key, UserStatus.Connected)).ToList();

            SendMessage(from, message);
        }

        public void BroadcastUserMediaAvailbility(long id)
        {
            Message message = new Message();
            LightUser user = _connections[id].User;

            message.MessageType = MessageType.UsersMediaAvailbility;
            message.UsersMediaAvailbility = new List<UserMediaAvailbility>();
            message.UsersMediaAvailbility.Add(new UserMediaAvailbility(id, user.WebcamAvaibility, user.MicrophoneAvaibility));

            List<long> receivers = _connections.Where(c => c.Key != id).Select(c => c.Key).ToList();

            SendMessage(receivers, message);
        }

        public void BroadcastUsersMediaAvailbility(long from)
        {
            Message message = new Message();

            message.MessageType = MessageType.UsersMediaAvailbility;
            message.UsersMediaAvailbility = _connections.Where(c => c.Key != from && c.Value.User.MicrophoneAvaibility.HasValue && c.Value.User.WebcamAvaibility.HasValue)
                .Select(c => new UserMediaAvailbility(c.Key, c.Value.User.WebcamAvaibility, c.Value.User.MicrophoneAvaibility)).ToList();

            SendMessage(from, message);
        }

        public void UpdateUserMediaAvailbility(Message message)
        {
            if (message.UsersMediaAvailbility.Count == 1)
            {
                LightUser user = _connections[message.SenderId].User;

                user.WebcamAvaibility = message.UsersMediaAvailbility[0].WebcamAvaibility;
                user.MicrophoneAvaibility = message.UsersMediaAvailbility[0].MicrophoneAvaibility;
            }
        }

        public void SendMessage(long id, Message message)
        {
            string data = JsonConvert.SerializeObject(message);
            _connections[id].Send(data);
        }

        private void SendMessage(List<long> users, Message message)
        {
            string data = JsonConvert.SerializeObject(message);
            _connections.Where(c => users.Contains(c.Value.User.Id)).Select(c => c.Value).ToList().ForEach(u => u.Send(data));
        }

    }
}
