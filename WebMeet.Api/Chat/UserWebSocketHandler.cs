﻿using System.Data;
using System.Web.Script.Serialization;
using Microsoft.Web.WebSockets;
using Newtonsoft.Json;
using WebMeet.Helpers.Chat;
using WebMeet.Helpers.Chat.Message;

namespace WebMeet.Api.Chat
{
    public class UserWebSocketHandler : WebSocketHandler
    {
        public LightUser User { get; set; }
        public bool PeerConnectionSupported { get; set; }

        private readonly JavaScriptSerializer _javaScriptSerializer;

        public UserWebSocketHandler(LightUser user)
        {
            User = user;
            _javaScriptSerializer = new JavaScriptSerializer();
        }

        public override void OnOpen()
        {
            ChatSingleton.Instance.ConnectUser(this);

            if (User.Id > 0)
            {
                ChatSingleton.Instance.BroadcastUsersStatus(User.Id);
                ChatSingleton.Instance.BroadcastUserStatusChanged(User.Id, UserStatus.Connected);
                ChatSingleton.Instance.BroadcastUsersMediaAvailbility(User.Id);
            }
            
        }

        public override void OnClose()
        {
            ChatSingleton.Instance.DisconnectUser(User.Id);
            ChatSingleton.Instance.BroadcastUserStatusChanged(User.Id, UserStatus.Disconnected);
        }

        public override void OnError()
        {
            base.OnError();
        }

        public override void OnMessage(string data)
        {
            ProcessMessage(JsonConvert.DeserializeObject<Helpers.Chat.Message.Message>(data));
        }

        private void ProcessMessage(Helpers.Chat.Message.Message message)
        {
            switch (message.MessageType)
            {
                case MessageType.UserMessage:
                    ChatSingleton.Instance.ForwardMessage(message);                 
                    break;

                case MessageType.UsersMediaAvailbility:
                    ChatSingleton.Instance.UpdateUserMediaAvailbility(message);
                    ChatSingleton.Instance.BroadcastUserMediaAvailbility(User.Id);
                    break;

                case MessageType.Talk:
                    switch (message.Talk.Type)
                    {
                        case TType.RequestTalk:
                            if (User.Id > 0)
                            {
                                ChatSingleton.Instance.CreateConversation(message);
                            }
                            else
                            {
                                ChatSingleton.Instance.JoinGuestToConversation(message);
                            }

                            break;

                        case TType.AcceptTalk:
                            ChatSingleton.Instance.AcceptConversation(message);
                            break;

                        case TType.RejectTalk:
                            ChatSingleton.Instance.RejectConversation(message);
                            break;

                        case TType.Stopped:
                            ChatSingleton.Instance.StopTalk(message);
                            break;

                    }
                    break;

                case MessageType.TalkHandshake:
                    ChatSingleton.Instance.ForwardMessage(message);
                    break;

            }
        }

    }
}
