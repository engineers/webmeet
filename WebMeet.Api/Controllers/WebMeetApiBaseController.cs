﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Abp.UI;
using Abp.WebApi.Controllers;
using WebMeet.Core.Services;

namespace WebMeet.Api.Controllers
{
    /// <summary>
    /// Bazwoy kontroler
    /// </summary>
    public class WebMeetApiBaseController : AbpApiController
    {
        public WebMeetApiBaseController()
        {
            LocalizationSourceName = WebMeetConsts.LocalizationSourceName;
        }

        protected virtual void CheckModelState()
        {
            if (!ModelState.IsValid)
            {
                throw new UserFriendlyException("Invalid request!");
            }
        }
    }
}