﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Abp.Authorization;
using Abp.Dependency;
using Microsoft.Web.WebSockets;
using WebMeet.Api.Chat;
using WebMeet.Core.Services.Persons;
using WebMeet.Core.Services.Users;
using WebMeet.Friends.Invitation;
using WebMeet.Helpers.Chat;
using WebMeet.Persons;

namespace WebMeet.Api.Controllers
{
    [RoutePrefix("api/WebSocket")]
    public class WebSocketController : WebMeetApiBaseController
    {
        public HttpResponseMessage Get(string Id)
        {
            if (HttpContext.Current.IsWebSocketRequest)
            {
                HttpContext.Current.AcceptWebSocketRequest(new UserWebSocketHandler(new LightUser(long.Parse(Id))));
                return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}