﻿import {BaseService} from 'Services/BaseService';

export class PersonsServiceClass extends BaseService {
    GetMe(){
        return this.execute('api/services/app/person/getme', {});
    }
}

export var PersonsService = new PersonsServiceClass();