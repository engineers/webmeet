﻿import {BaseService} from 'Services/BaseService';

export class CalendarServiceClass extends BaseService {
    GetMyEvents(request) {
        return this.execute('api/services/app/event/getforme', request);
    }

    GetGroupEvents(request) {
        return this.execute('api/services/app/event/getforgroup', request);
    }

    CreateMyEvent(request) {
        return this.execute('api/services/app/event/createforme', request);
    }

    CreateGroupEvent(request){
        return this.execute('api/services/app/event/createforgroup', request);
    }

    UpdateMyEvent(request, eventId) {
        return this.execute('api/services/app/event/updateforme?id=' + eventId, request);
    }

    UpdateGroupEvent(request, eventId) {
        return this.execute('api/services/app/event/updateforgroup?id=' + eventId, request);
    }

    RemoveMyEvent(eventId){
        return this.execute('api/services/app/event/deleteforme?id=' + eventId, {});
    }

    RemoveGroupEvent(eventId){
        return this.execute('api/services/app/event/deleteforgroup?id=' + eventId, {});
    }
}

export var CalendarService = new CalendarServiceClass();