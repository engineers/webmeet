﻿import {BaseService} from 'Services/BaseService';

export class ContactsServiceClass extends BaseService {
    GetMyContacts(){
        var request = {};

        return this.execute('api/services/app/acquaintance/get', request);
    }

    RemoveContact(contactId) {
        return this.execute('api/services/app/acquaintance/delete?id=' + contactId, { });
    }

    Search(searchModel){
        
        var request = {
            LastName: searchModel.Surname,
            UserName: searchModel.Login,
            SkipCount: searchModel.SkipCount,
            MaxResultCount: searchModel.MaxResultCount
        };

        return this.execute('api/services/app/person/get', request);
    }

    Invite(userId){
        var request = {
            ReceiverId: userId
        };

        return this.execute('api/services/app/invitation/create', request);
    }

    GetBlackList() {
        return this.execute('api/services/app/blacklist/get', { });
    }

    AddToBlackList(personId) {
        return this.execute('api/services/app/blacklist/create', { PersonId: personId });
    }

    RemoveFromBlackList(personId) {
        return this.execute('api/services/app/blacklist/delete?id=' + personId, { });
    }

    GetMyReceivedInvitations() {
        return this.execute('api/services/app/invitation/inbox', { State: 0 });
    }

    GetSentInvitations(request){
        request.State = 0;
        return this.execute('api/services/app/invitation/outbox', request);
    }

    UpdateInvitation(invitationId, state){
        return this.execute('api/services/app/invitation/update?id=' + invitationId, { State: state });
    }

    AcceptInvitation(invitationId) {
        return this.UpdateInvitation(invitationId, 'Accepted');
    }

    RejectInvitation(invitationId) {
        return this.UpdateInvitation(invitationId, 'Rejected');
    }

    CancelInvitation(invitationId) {
        return this.UpdateInvitation(invitationId, 'Canceled');
    }

    InviteFriend(email) {
        return this.execute('api/services/app/account/invite', {EmailAddress: email});
    }
}

export var ContactsService = new ContactsServiceClass();