﻿import {BaseService} from 'Services/BaseService';

export class GroupsServiceClass extends BaseService {
    GetMyGroups(name) {
        return this.execute('api/services/app/group/get', {Name: name});
    }

    CreateGroup(name, persons) {
        var request = {
            Name: name,
            Persons: persons
        };

        return this.execute('api/services/app/group/create', request);
    }

    DeleteGroup(id) {
        return this.execute('api/services/app/group/delete?groupId=' + id);
    }

    UpdateGroup(id, name) {
        var request = {
            Name: name,
            Avatar: null
        };

        return this.execute('api/services/app/group/update?id=' + id, request);
    }

    AddPersons(id, persons) {
        var request = {
            GroupId: id,
            Persons: persons
        };

        return this.execute('api/services/app/group/addpersons', request);
    }

    DeletePersons(id, persons) {
        var request = {
            GroupId: id,
            Persons: persons
        };

        return this.execute('api/services/app/group/deletepersons', request);
    }
}

export var GroupsService = new GroupsServiceClass();