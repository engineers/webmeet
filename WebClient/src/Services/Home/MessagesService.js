﻿import {BaseService} from 'Services/BaseService';

export class MessagesServiceClass extends BaseService {
    GetMessages(receiverId) {
        return this.execute('api/services/app/message/get', {ReceiverId: receiverId});
    }

    CreateMessage(id, content) {
        return this.execute('api/services/app/message/create', {ReceiverId: id, Content: content});
    }
}

export var MessagesService = new MessagesServiceClass();