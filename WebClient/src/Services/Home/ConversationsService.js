﻿import {BaseService} from 'Services/BaseService';

export class ConversationsServiceClass extends BaseService {
    GetMyConversations(personBaseId) {
        return this.execute('api/services/app/conversation/get', {PersonBaseId: personBaseId});
    }

    CreateConversation(personBaseId) {
        return this.execute('api/services/app/conversation/create', {PersonBaseId: personBaseId});
    }
}

export var ConversationsService = new ConversationsServiceClass();