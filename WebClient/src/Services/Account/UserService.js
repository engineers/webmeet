﻿import {BaseService} from 'Services/BaseService';

export class Service extends BaseService {
    GetMyInfo = () => {
        return this.execute('api/services/app/person/getme', {});
    }

    UpdateMyInfo = (model) => {
        var request = {
            FirstName: model.Name,
            LastName: model.Surname,
            Address: model.Address,
            EmailAddress: model.EmailAddress
        };

        return this.execute('api/services/app/person/updateme', request);
    }
}

export var UserService = new Service();
