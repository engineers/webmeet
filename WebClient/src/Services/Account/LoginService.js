﻿import {BaseService} from 'Services/BaseService';
import {CommonHelper} from 'Helpers/CommonHelper';
import {Session} from 'Helpers/SessionHelper';
import {Local} from 'Helpers/LocalHelper';

export class Service extends BaseService {
    UrlBaseUrl = window.location.protocol + '//' +  document.location.host + '/#';
    ConfirmEmailHash = 'ConfirmEmail';
    UrlForConfirmEmail =  this.UrlBaseUrl + this.ConfirmEmailHash;

    ResetPasswordHash = 'ResetPassword';
    UrlForResetPassword = this.UrlBaseUrl + this.ResetPasswordHash;

    Login(username, password, rememberMe){
        var request = {
            username: username,
            password: password,
            grant_type: 'password'
        };

        return this.execute('oauth2/token', request, 'application/x-www-form-urlencoded')
                .then((response) => {
                    if(response.access_token){
                        Session.SetString('auth_token', response.access_token);
                        Service.IsAuth = true;

                        if(rememberMe){
                            Local.SetString('auth_token', response.access_token);
                        }
                    }
                    return response;
                });
    }

    GetToken(){
        if(!Service.Token){
            Service.Token = Session.GetString('auth_token');
        }

        return Service.Token;
    }

    ConfirmEmail() {
        var request = {
            UserId: Session.GetString('userId'),
            Token: Session.GetString('token')
        };
        
        sessionStorage.clear();
        return this.execute('/api/services/app/account/confirmemail', request);
    }

    IsAuthenticated() {
        if(Service.IsAuth == undefined){
            Service.IsAuth = Session.GetString('auth_token') != undefined;

            if(!Service.IsAuth){
                var localAuth = Local.GetString('auth_token');

                if(localAuth){
                    Session.SetString('auth_token', localAuth);
                    Service.IsAuth = true;
                }
            }
        }

        return Service.IsAuth;
    }

    Logout(){
        sessionStorage.clear();
        localStorage.clear();
    }

    Register(request) {
        request.BaseUrl = this.UrlForConfirmEmail;

        return this.execute('api/services/app/account/register', request);
    }

    IsEmailConfirmation(){
        var url = window.location.href;

        if(url.indexOf(this.ConfirmEmailHash) >= 0){
            Session.SetString('userId', CommonHelper.GetQueryVariable('userId'));
            Session.SetString('token', CommonHelper.GetQueryVariable('token'));
            return true;
        }

        return false;
    }

    IsResetPassword(){
        var url = window.location.href;

        if(url.indexOf(this.ResetPasswordHash) >= 0){
            Session.SetString('userId', CommonHelper.GetQueryVariable('userId'));
            Session.SetString('token', CommonHelper.GetQueryVariable('token'));
            return true;
        }

        return false;
    }

    ResendConfirmationEmail(email, login) {
        var request = {
            UserName: login,
            EmailAddress: email,
            BaseUrl: this.UrlForConfirmEmail
        };

        return this.execute('api/services/app/account/resendconfirmationemail', request);
    }

    ForgotPassword(email, login){
        var request = {
            UserName: login,
            EmailAddress: email,
            BaseUrl: this.UrlForResetPassword
        };

        return this.execute('api/services/app/account/sendpasswordresetemail', request);
    }

    ChangePassword(password, confirmPassword) {
        var request = {
            UserId: Session.GetString('userId'),
            Token: Session.GetString('token'),
            Password: password,
            ConfirmPassword: confirmPassword
        };

        return this.execute('api/services/app/account/resetpassword', request)
                .then((res) => {
                    if(res.success){
                        sessionStorage.clear();
                    }
                    return res;
                });
    }
}

export var LoginService = new Service();