﻿import {LoginService} from 'Services/Account/LoginService';

export class BaseService {

//    baseUrl = window.location.protocol + '//' +  document.location.host + '/WebMeet.Api/';
    baseUrl = window.location.protocol + '//' +  document.location.host + ':6010/';

    execute(url, request, contentType){
        var self = this;
        var deferred = Q.defer();

        var innerData = '';

        if(contentType){
            innerData = request;
        } else {
            innerData = JSON.stringify(request);
        }

        $.ajax({
            async: true,
            method: 'POST',
            url: self.baseUrl + url,
            accepts: 'application/json; charset=utf-8',
            contentType: contentType || 'application/json; charset=utf-8',
            data: innerData,
            beforeSend: function(xhr, settings){
                var token = LoginService.GetToken();

                if(token){
                    xhr.setRequestHeader('Authorization', 'Bearer ' + token);
                }

                if(navigator.language != 'pl') {
                    xhr.setRequestHeader('Accept-Language', 'eu');
                }
            }, 
            success: function(data){
                deferred.resolve(data);
            },
            
            error: function(xhr, textStatus, errorThrown){
                deferred.reject(errorThrown);
            }
        });
            
        return deferred.promise;
    }
}