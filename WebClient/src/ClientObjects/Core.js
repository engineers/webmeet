﻿export class GridPager {
    constructor() {
        this.SetDefaultValues();
    }

    SetDefaultValues = () => {
        this.SkipCount = 0;
        this.MaxResultCount = 10;
    }
}