﻿import {ensure} from 'aurelia-validation';
import {inject} from 'aurelia-framework';
import {I18N} from 'aurelia-i18n';

@inject(I18N)
export class Login {
    constructor(i18n){
        this.i18 = i18n;
    }

    @ensure(it =>  it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.LoginIsRequired'))
                     .hasLengthBetween(5, 15).withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.LoginMustHasLength')))
    Username: string = undefined;
        
    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.PasswordIsRequired'))
                    .hasLengthBetween(6, 12).withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.PasswordMustHasLength')))
    Password: string = undefined;

    RemeberMe: boolean = false;
}

@inject(I18N)
export class EditUserData {
    constructor(i18n){
        this.i18 = i18n;
    }

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.NameIsRequired')))
    Name: string;

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.SurnameIsRequired')))
    Surname: string;

    Address: string;

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.EmailIsRequired'))
                    .isEmail().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.EmailIsIncorrect')))
    EmailAddress: string;
}

@inject(I18N)
export class Registration {
    constructor(i18n){
        this.i18 = i18n;
    }

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.NameIsRequired')))
    Name: string;

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.SurnameIsRequired')))
    Surname: string;

    Address: string;

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.EmailIsRequired'))
                    .isEmail().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.EmailIsIncorrect')))
    EmailAddress: string;

    @ensure(it =>  it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.LoginIsRequired'))
                     .hasLengthBetween(5, 15).withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.LoginMustHasLength')))
    UserName: string;

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.PasswordIsRequired'))
                    .hasLengthBetween(6, 12).withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.PasswordMustHasLength')))
    Password: string;

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.MustConfirmPassword'))
                    .isEqualTo((parent) => { return it.result.properties.Password.latestValue; }).withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.PasswordsIsDifferent')))
    ConfirmPassword: string;
    
    AcceptRegulations = true;
}

@inject(I18N)
export class ResendEmail {
    constructor(i18n){
        this.i18 = i18n;
    }
    @ensure(it =>  it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.LoginIsRequired'))
                     .hasLengthBetween(5, 15).withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.LoginMustHasLength')))
    Username: string = undefined;

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.EmailIsRequired'))
                    .isEmail().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.EmailIsIncorrect')))
    Email: string;
}

@inject(I18N)
export class ResetPassword {
    constructor(i18n){
        this.i18 = i18n;
    }
    @ensure(it =>  it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.LoginIsRequired'))
                     .hasLengthBetween(5, 15).withMessage(it.subject.i18.tr('Account.Login.ValidationMessages.LoginMustHasLength')))
    Password: string;

    @ensure(it => it.isNotEmpty().withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.MustConfirmPassword'))
                    .isEqualTo((parent) => { return it.result.properties.Password.latestValue; }).withMessage(it.subject.i18.tr('Account.Registration.ValidationMessages.PasswordsIsDifferent')))
    ConfirmPassword: string;
}
