import {LogManager} from 'aurelia-framework';
import {ConsoleAppender} from 'aurelia-logging-console';
import {MaterializeStrategy} from 'Helpers/ValidationStrategy';
import {LoginService} from 'Services/Account/LoginService';
import {CommonHelper} from 'Helpers/CommonHelper';
import {Session} from 'Helpers/SessionHelper';

LogManager.addAppender(new ConsoleAppender());
LogManager.setLevel(LogManager.logLevel.debug);

export function configure(aurelia) {
    aurelia.use
        .plugin('aurelia-i18n', (instance) => {
            // adapt options to your needs (see http://i18next.com/pages/doc_init.html)
            instance.setup({
                resGetPath: 'locale/__lng__/__ns__.json',
                lng: navigator.language == 'pl' ? 'pl' : 'en',
                attributes: ['t', 'i18n'],
                getAsync: false,
                sendMissing: false,
                fallbackLng: 'pl',
                debug: true,
                useLocalStorage: false,
                localStorageExpirationTime: 86400000 
            });
        })
        .defaultBindingLanguage()
        .defaultResources()
        .history()
        .router()
        .eventAggregator()
        .plugin('Resources/Index')
        .plugin('aurelia-computed', { 
            enableLogging: true 
        })
        .plugin('aurelia-validation', (config) => { config.useLocale('pl-PL').useViewStrategy(new MaterializeStrategy()); })
        .plugin('aurelia-animator-css');

    aurelia.start().then(a => {
        var url = window.location.href;
        var isConfirmation = LoginService.IsEmailConfirmation();
        var isResetPassword = LoginService.IsResetPassword();
        var initPage = LoginService.IsAuthenticated() ? 'app' : 'ViewModels/Account/Login';

        if(isConfirmation){
            initPage = 'ViewModels/Account/ConfirmEmail';
        }

        if(isResetPassword){
            initPage = 'ViewModels/Account/ResetPassword';
        }

        if(url.indexOf('Guest') >= 0) {
            initPage = 'ViewModels/Guest/Guest';
            Session.SetString('roomId', CommonHelper.GetQueryVariable('roomId'));
        }

        a.setRoot(initPage, document.body);
    });
}
