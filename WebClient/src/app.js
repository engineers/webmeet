import {AppRouter, RouterConfiguration} from 'aurelia-router';
import {inject} from 'aurelia-framework';
import {I18N} from 'aurelia-i18n';

@inject(AppRouter, I18N)
export class App {
    constructor(router, i18n) {
        this.router = router;
        this.i18n = i18n;
    }
  
    configureRouter(config: RouterConfiguration, router: AppRouter) {
        config.title = 'WebMeet';
    
        config.map([
            { route: ['', 'panel'], name: 'panel', moduleId: 'ViewModels/Home/Panel/Panel', nav: true, title: this.i18n.tr('Nav.MyAccount') },
            { route: 'contacts', name: 'contacts', moduleId: 'ViewModels/Home/Contacts/Contacts', nav: true, title: this.i18n.tr('Nav.MyContacts') },
            { route: 'calendar', name: 'calendar', moduleId: 'ViewModels/Home/Calendar/Calendar', nav: true, title: this.i18n.tr('Nav.MyCalendar') },
            { route: 'help', name: 'help', moduleId: 'ViewModels/Main/Help', nav: false, title: this.i18n.tr('Nav.Help') },
            { route: 'authors', name: 'authors', moduleId: 'ViewModels/Main/Authors', nav: false, title: this.i18n.tr('Nav.Authors') },
            { route: 'regulations', name: 'regulations', moduleId: 'ViewModels/Main/Regulations', nav: false, title: this.i18n.tr('Nav.Regulations') }
        ]);
    
        this.router = router;
    }
}
