export function configure(aurelia) {
    aurelia.globalResources('../../Widgets/Common/MInput');
    aurelia.globalResources('../../Widgets/Common/Preloader');
    aurelia.globalResources('../../Widgets/Common/Loader');
    aurelia.globalResources('../../Widgets/Common/nav-bar');
    aurelia.globalResources('../../Widgets/Common/GridPager');
    aurelia.globalResources('../../Widgets/Common/Tooltip');
    aurelia.globalResources('../../Converters/DateConverter');
}
