﻿
export class BaseViewModel {
   ExecuteFunctionIfPossible(func){
        if(func && typeof func == 'function') {
            func(arguments[1]);
        }
    }
}