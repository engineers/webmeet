﻿
import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {MaterializeHelper} from 'Helpers/MaterializeHelper';
import {Chat} from 'Helpers/Chat';
import {Validation} from 'aurelia-validation';
import {I18N} from 'aurelia-i18n';

@inject(Validation, EventAggregator, I18N)
export class Guest {
    constructor(validation, eventAggregator, i18n) {
        this.validation = validation;
        this.eventAggregator = eventAggregator;
        this.i18n = i18n;

        var self = this;

        this.GuestValidator = this.validation.on(this)
            .ensure('NameInput')
                .isNotEmpty()
                .withMessage('Imię jest wymagane');
        Chat.InjectEventAggregator(this.eventAggregator);
        Chat.InjectI18N(this.i18n);


        this.Initialize();

        this.Talk = null;
        this.NameInput = '';

        this.eventAggregator.subscribe('TalkCreatedEvent', talk => {
            self.Talk = talk;
        });

        this.eventAggregator.subscribe('TalkEndedEvent', contact => {
            self.Talk = null;
        });

        this.eventAggregator.subscribe('GuestErrorEvent', contact => {
            Chat.StopTalk();
            self.Talk = null;
            Chat.talk = null;
            self.error = true;
        });

        this.error = false;
    }

    Initialize() {
        Chat.me = {
            Id: -(new Date().valueOf()),
            Name: '',
            IsGroup: false
        };
        Chat.GuestMode = true;
        Chat.ConnectToWebsocket();
    }

    attached() {
        MaterializeHelper.InitCollapsilable({ accordion: false });
    }

    Chatting() {
        var self = this;

        this.GuestValidator.validate().then(() => {
            Chat.me.Name = self.NameInput;
            Chat.BindWebSocketEvents();
            Chat.RequestTalkAsGuest();

        }).catch(() => {});

    }

    StopTalk() {
        Chat.StopTalkRequest();
    }

}