﻿import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Chat} from 'Helpers/Chat';

@inject(EventAggregator)
export class TalkRequest {

    constructor(eventAggregator) {
        var self = this;

        this.eventAggregator = eventAggregator;

        this.talk = null;

        this.talkRequestSub = this.eventAggregator.subscribe('TalkRequestEvent', data => {
            self.talk = data;

            $('#talkRequestModal').openModal({
                dismissible: false
            });
        });
    }

    unbind() {
        this.talkRequestSub.dispose();
    }

    AcceptTalk() {
        Chat.AcceptTalk();
    }

    RejectTalk() {
        var data = {
            MessageType: 'Talk',
            Talk: {
                Id: this.talk.Id,
                Type: 'RejectTalk'
            }
        };

        Chat.SendToWsServer(data); 
    }
}