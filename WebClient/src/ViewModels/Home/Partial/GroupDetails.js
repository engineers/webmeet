﻿import {Validation} from 'aurelia-validation';
import {inject} from 'aurelia-framework';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';
import {GroupsService} from 'Services/Home/GroupsService';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Chat} from 'Helpers/Chat';

@inject(Validation, I18N, EventAggregator)
export class Create {
    Group = null;
    GroupName = '';
    GroupUsers = [];
    OtherUsers = [];
    error = false;

    constructor(validation, i18n, eventAggregator){
        this.validation = validation;
        this.i18n = i18n;

        this.eventAggregator = eventAggregator;

        this.EventValidators = this.validation.on(this)
            .ensure('GroupName')
                .isNotEmpty()
                .withMessage('Nazwa grupy jest wymagana');

        this.EventValidators.validate().then(() => {}).catch(() => {});

        this.i18nParams = {
            username: '',
            userscount: 0
        };
    }

    attached() {
        var self = this;

        this.SelectContactSub = this.eventAggregator.subscribe('OpenGroupDetailsModal', group => {
            self.InitializeGroup(group);
            $('#groupDetailsModal').openModal();
        });

    }

    InitializeGroup(group) {
        var self = this;
        this.GroupUsers = [];
        this.OtherUsers = [];

        this.i18nParams = {
            username: '',
            userscount: 0
        };

        if (group !== undefined && group !== null) {
            this.Group = group;
        } else {
            this.Group = {
                Name: '',
                IsGroup: true,
                Id: 0, 
                Persons: [],
                Active: false,
                WebcamAvaibility: null,
                MicrophoneAvaibility: null
            };

            this.Group.Persons.push(Chat.me.Id);
        }

        this.GroupName = this.Group.Name;

        Chat.Contacts.forEach((item, index) => {
            if (item.IsGroup === false) {
                var user = {
                    Id: index,
                    Name: item.Name,
                    Class: ''
                };

                if ($.inArray(index, self.Group.Persons) >= 0) {
                    self.GroupUsers.push(user);
                } else {
                    self.OtherUsers.push(user);
                }
            }
            });
    }

    SelectUser(user) {
        if (user.Class === 'active') {
            user.Class = '';
        } else {
            user.Class = 'active';
        }

        var selectedUsers = Enumerable.From(this.OtherUsers).Where((u) => u.Class === 'active').ToArray();

        this.i18nParams.userscount = selectedUsers.length;
        if (selectedUsers.length === 1) {
            this.i18nParams.username = selectedUsers[0].Name;
        }
    }

    AddUsers() {
        for (var i = this.OtherUsers.length - 1; i >= 0; --i) {
            var user = this.OtherUsers[i];

            if (user.Class === 'active') {
                user.Class = '';
                this.OtherUsers.splice(i, 1);
                this.GroupUsers.push(user);
    }
        }

        var selectedUsers = Enumerable.From(this.OtherUsers).Where((u) => u.Class === 'active').ToArray();

        this.i18nParams.userscount = selectedUsers.length;
        if (selectedUsers.length === 1) {
            this.i18nParams.username = selectedUsers[0].Name;
        }
    }
            
    DeleteUser(user) {
        if (user.Id !== Chat.me.Id || this.Group.Id !== 0) {
            this.GroupUsers.splice($.inArray(user, this.GroupUsers), 1 );
            user.Class = '';
            this.OtherUsers.push(user); 
        }
    }

    SaveNew() {
        var self = this;

        if (this.GroupUsers.length < 3) {
            this.error = true;
            return;
        }

        this.error = false;

        var persons = Enumerable.From(this.GroupUsers).Select((u) => u.Id).Where((i) => i !== Chat.me.Id).ToArray();

        this.EventValidators.validate()
            .then(() => {
                GroupsService.CreateGroup(self.GroupName, persons)
                    .then((result) => {
                        if (result.success) {
                            self.eventAggregator.publish('UpdateContactsEvent');
                        }
                        $('#groupDetailsModal').closeModal();
                        self.ShowToastMessage(result.success, 'CREATE');
                    });
            })
            .catch(() => {});
    }

    Save() {
        var self = this;

        if (this.GroupUsers.length < 3) {
            this.error = true;
            return;
        }

        this.error = false;

        this.EventValidators.validate()
            .then(() => {
                GroupsService.UpdateGroup(self.Group.Id, self.GroupName)
                    .then((result) => {
                        self.SaveGroupData();
                    });
            })
            .catch(() => {});
    }

    Delete() {
        var self = this;

        GroupsService.DeleteGroup(self.Group.Id)
                    .then((result) => {
                        if (result.success) {
                            self.eventAggregator.publish('UpdateContactsEvent');
                        }
                        $('#groupDetailsModal').closeModal();
                        self.ShowToastMessage(result.success, 'DELETE');
                    });
    }

    SaveGroupData() {
        var self = this;
        if (this.Group.Name !== this.GroupName) {
            GroupsService.UpdateGroup(self.Group.Id, self.GroupName)
                    .then((result) => {
                        if (result.success) {
                            self.SaveAddedPersons();
                            self.Group.Name = self.GroupName;
                        } else {
                            self.ShowToastMessage(false, 'UPDATE');
                            return;
                        }
                });
        } else {
            self.SaveAddedPersons();
        }
    }

    SaveAddedPersons() {
        var self = this;
        var addedPersons = [];

        for (var i = 0; i < this.GroupUsers.length; ++i) {
            var user = this.GroupUsers[i];

            if (Enumerable.From(this.Group.Persons).Where((i) => i === user.Id).ToArray().length === 0) {
                addedPersons.push(user.Id);
            }
        }

        if (addedPersons.length !== 0) {
            GroupsService.AddPersons(self.Group.Id, addedPersons)
                    .then((result) => {
                        if (result.success) {
                            for (var i = 0; i < addedPersons.length; ++i) {
                                self.Group.Persons.push(addedPersons[i]);
                            }
                            self.SaveDeletedPersons();
                        } else {
                            self.ShowToastMessage(false, 'UPDATE');
                            return;   
                        }
                    });
        } else {
            self.SaveDeletedPersons();
        }
    }

    SaveDeletedPersons() {
        var self = this;
        var deletedPersons = [];
        
        for (var i = 0; i < this.Group.Persons.length; ++i) {
            var uId = this.Group.Persons[i];

            if (Enumerable.From(this.GroupUsers).Where((u) => u.Id === uId).ToArray().length === 0) {
                deletedPersons.push(uId);
            }
        }

        if (deletedPersons.length !== 0) {
            GroupsService.DeletePersons(self.Group.Id, deletedPersons)
                    .then((result) => {
                        if (result.success) {
                            for (var i = 0; i < deletedPersons.length; ++i) {
                                self.Group.Persons.splice($.inArray(deletedPersons[i], self.Group.Persons), 1);
                            }
                        }
                        $('#groupDetailsModal').closeModal();
                        self.ShowToastMessage(result.success, 'UPDATE');
                    });
        } else {
            self.ShowToastMessage(true, 'UPDATE');
            $('#groupDetailsModal').closeModal();
        }
    }

    ShowToastMessage(isSucceed, action) {
        var toastContent = '';

        switch(action) {
            case 'CREATE' :
                toastContent = isSucceed ? this.i18n.tr('Contacts.Groups.CreationSuccess') : this.i18n.tr('Contacts.Groups.CreationFailed');
                break;

            case 'UPDATE' :
                toastContent = isSucceed ? this.i18n.tr('Contacts.Groups.SaveChangesSuccess') : this.i18n.tr('Contacts.Groups.SaveChangesFailed');
                break;

            case 'DELETE' :
                toastContent = isSucceed ? this.i18n.tr('Contacts.Groups.DeletionSuccess') : this.i18n.tr('Contacts.Groups.DeletionFailed');
                break;
        }

        if(isSucceed) {
            Toastr.success(toastContent, this.i18n.tr('Common.SaveSuccessMessage'));
        } else {
            Toastr.error(toastContent, this.i18n.tr('Common.SomethingWentWrong'));
        }
    }
   
}