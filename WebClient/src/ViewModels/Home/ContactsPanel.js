﻿import {GroupsService} from 'Services/Home/GroupsService';
import {ContactsService} from 'Services/Home/ContactsService';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {ObserverLocator} from 'aurelia-binding';
import {AppRouter} from 'aurelia-router';
import {Local} from 'Helpers/LocalHelper';
import {Chat} from 'Helpers/Chat';
import {I18N} from 'aurelia-i18n';

@inject(EventAggregator, ObserverLocator, AppRouter, I18N)
export class ContactsPanel {
    constructor(eventAggregator, observerLocator, router, i18n) {
        var self = this;

        this.eventAggregator = eventAggregator;
        this.observerLocator = observerLocator;
        this.router = router;
        this.i18n = i18n;

        this.SearchName = '';
        this.ActiveContact = null;

        this.observerLocator.getObserver(this, 'SearchName')
                            .subscribe(this.SelectContacts);

        Chat.InjectEventAggregator(this.eventAggregator);
        Chat.InjectRouter(this.router);
        Chat.InjectI18N(this.i18n);

        this.eventAggregator.subscribe('SelectContactRequestEvent', contact => {
            self.SelectContact(contact);
        });

        this.eventAggregator.subscribe('UpdateContactsEvent', data => {
            self.InitializeContacts();
        });

    }

    activate() {
        this.InitializeContacts();
    }

    InitializeContacts() {
        var self = this;

        this.AllContacts = [];

        ContactsService.GetMyContacts()
             .then((res) => {
                 if(res.success){
                     for (var key in res.result.items) {
                         var contact = {
                             Name: res.result.items[key].personName,
                             IsGroup: false,
                             Id: res.result.items[key].personId,
                             Active: false,
                             WebcamAvaibility: false,
                             MicrophoneAvaibility: false, 
                             NewMessagesCount: 0
                         };

                         this.AllContacts.push(contact);
                     }
                 }
             })
            .then(() => {
                GroupsService.GetMyGroups('')
                 .then((res) => {
                     if(res.success){
                         for (var key in res.result.items) {
                             var contact = {
                                 Name: res.result.items[key].name,
                                 IsGroup: true,
                                 Id: res.result.items[key].id,
                                 Active: false,
                                 WebcamAvaibility: false,
                                 MicrophoneAvaibility: false,
                                 Persons: Enumerable.From(res.result.items[key].persons).Select((item) => {
                                     return item.id;
                                 }).ToArray(), 
                                 NewMessagesCount: 0
                             };

                             this.AllContacts.push(contact);
                         }
                     }

                     this.CurrentContacts = this.AllContacts;
                     Chat.InitializeContacts(this.AllContacts);

                     if (this.CurrentContacts && this.CurrentContacts.length > 0 && this.router.history.fragment === '/calendar') {
                         self.SelectContact(this.CurrentContacts[0]);
                     }
                 });
            });
    }

    CreateGroup() {
        this.eventAggregator.publish('OpenGroupDetailsModal', null);
    }

    EditGroup() {
        this.eventAggregator.publish('OpenGroupDetailsModal', this.ActiveContact);
    }

    SelectContacts(name) {
        this.CurrentContacts = Enumerable.From(this.AllContacts).Where((s) => {
            return s.Name.toLowerCase().indexOf(name.toLowerCase()) >= 0;
        }).ToArray();
    }

    SelectContact(contact) {
        this.CurrentContacts.forEach((c) => {
            c.Class = '';
        });

        contact.Class = 'active';
        contact.NewMessagesCount = 0;

        this.ActiveContact = contact;

        this.eventAggregator.publish('SelectContactEvent', contact);

        if(this.router.history.fragment != '/calendar') {
            this.router.navigate('calendar');
            Local.SetObject('SelectedContact', contact);
        } 

    }

}