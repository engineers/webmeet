﻿import {MaterializeHelper} from 'Helpers/MaterializeHelper';
import {Validation} from 'aurelia-validation';
import {LoginService} from 'Services/Account/LoginService';
import {inject} from 'aurelia-framework';
import {ContactsService} from 'Services/Home/ContactsService';
import {CommonHelper} from 'Helpers/CommonHelper';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Chat} from 'Helpers/Chat';

@inject(Validation, I18N, EventAggregator)
export class Panel {
    ShowSupport = false;

    constructor(validate, i18n, eventAggregator) {
        var self = this;

        this.Email = undefined;
        this.i18n = i18n;
        this.eventAggregator = eventAggregator;
        this.MyName = Chat.me !== null ? Chat.me.Name : '';

        this.Validator = validate.on(this).ensure('Email').isNotEmpty().withMessage(this.i18n.tr('Account.Registration.ValidationMessages.EmailIsRequired'))
                    .isEmail().withMessage(this.i18n.tr('Account.Registration.ValidationMessages.EmailIsIncorrect'));

        this.Validator.validate().then(() =>{}).catch(()=>{});

        this.eventAggregator.subscribe('MeUpdatedEvent', function() {
            self.MyName = Chat.me.Name;
        });
    }

    canActivate(){
        return LoginService.IsAuthenticated();
    }

    activate(){
        var support = CommonHelper.PrepareSupportList(this.i18n);

        this.SupportText = support.SupportText;
        this.SupportItems = support.SupportItems;
    }

    attached() {
        MaterializeHelper.InitDropDown();
        MaterializeHelper.InitCollapsilable();
        $('.modal-trigger').leanModal();    
    }

    OpenSupport(){
        this.ShowSupport = true;
    }

    CloseSupport(){
        this.ShowSupport = false;
    }

    InviteFriend(){
        this.Validator.validate().then(() => {
            ContactsService.InviteFriend(this.Email)
                .then( res => {
                    if(res.success) {
                        Toastr.success(this.i18n.tr('Home.Panel.SentInviteSuccess'),
                            this.i18n.tr('Common.ActionFinishedSuccess'),
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                    } else {
                        Toastr.warning(this.i18n.tr('Home.Panel.SentInviteFailed') + Toastr.AddErrorsFromResponse(res, this.i18n),
                            this.i18n.tr('Common.SomethingWentWrong'),
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                    }
                });
        }).catch(()=>{});
    }
}
