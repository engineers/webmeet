﻿import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';

@inject(EventAggregator)
export class Contacts {
    constructor(eventAggregator) {
        this.eventAggregator = eventAggregator;
    }

    attached(){
        $('ul.tabs').tabs();

        this.eventAggregator.publish('CurrentScreenEvent');
    }

    UpdateScreen(screenId) {
        switch(screenId){
            case 1:  this.eventAggregator.publish('CurrentScreenEvent'); break;
            case 2:  this.eventAggregator.publish('ReceivedScreenEvent'); break;
            case 3:  this.eventAggregator.publish('SentScreenEvent'); break;
            case 4:  this.eventAggregator.publish('NewScreenEvent'); break;
            case 5:  this.eventAggregator.publish('BlackListScreenEvent'); break;
            default: break;
        }
    }
}