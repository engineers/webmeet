﻿import {ContactsService} from 'Services/Home/ContactsService';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';

@inject(EventAggregator, I18N)
export class Received {
    constructor(eventAggregator, i18n) {
        this.eventAggregator = eventAggregator;
        this.i18n = i18n;

        this.ReceivedSub = this.eventAggregator.subscribe('ReceivedScreenEvent', payload => {
            this.OnTabEnter();
        });
    }

    unbind() {
        this.ReceivedSub.dispose();
    }

    OnTabEnter() {
        this.SearchTrigger = !this.SearchTrigger;
    }

    Accept(invitation) {
        var self = this;

        ContactsService.AcceptInvitation(invitation.id)
            .then((response) => {
                if(response.success) {
                    Toastr.success(this.i18n.tr('Contacts.Groups.Received.AcceptInvitationSuccess') + invitation.senderName, 
                       this.i18n.tr('Common.ActionFinishedSuccess'),
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                    self.eventAggregator.publish('UpdateContactsEvent');
                    this.Search();
                } else {
                    Toastr.warning(this.i18n.tr('Contacts.Groups.Received.AcceptInvitationFailed') + invitation.senderName + Toastr.AddErrorsFromResponse(response, this.i18n), 
                       this.i18n.tr('Common.SomethingWentWrong'),
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                }
            });
    }

    OnSearch = (gridParam, callback) => {
        ContactsService.GetMyReceivedInvitations()
       .then((response) => {
           this.ListItem = [];

           if(response.success){
               this.ListItem = response.result.items;
               callback(response.result.totalCount);
           } else {
               callback(0);
           }
       });
    }
}
