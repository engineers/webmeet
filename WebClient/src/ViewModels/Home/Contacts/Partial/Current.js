﻿import {ContactsService} from 'Services/Home/ContactsService';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';

@inject(EventAggregator, I18N)
export class Current {
    constructor(eventAggregator, i18n) {
        this.eventAggregator = eventAggregator;
        this.i18n = i18n;

        this.CurrentScreenSub = this.eventAggregator.subscribe('CurrentScreenEvent', payload => {
            this.OnTabEnter();
        });
    }

    attached(){
        setTimeout(this.OnTabEnter, 10);
    }

    unbind() {
        this.CurrentScreenSub.dispose();
    }

    OnTabEnter = () => {
        this.SearchTrigger = !this.SearchTrigger;
    }

    RemoveAcquaintance = (contact) => {
        var self = this;

        ContactsService.RemoveContact(contact.id)
            .then((response) => {
                if(response.success) {
                    Toastr.success(this.i18n.tr('Contacts.Groups.Current.AcquaintanceWithUser') + contact.personName + this.i18n.tr('Contacts.Groups.Current.HasBeenFinished'), 
                       'Usuwanie zakończone sukcesem',
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                    self.eventAggregator.publish('UpdateContactsEvent');
                    this.OnTabEnter();
                } else {
                    Toastr.error(this.i18n.tr('Contacts.Groups.Current.ErrorOccuredDuringRemoveUser') + contact.personName + this.i18n.tr('Contacts.Groups.Current.FromGroupFriends') + Toastr.AddErrorsFromResponse(response, this.i18n), 
                       this.i18n.tr('Common.SomethingWentWrong'),
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                }
            });
    }

    OnSearch = (gridParam, callback) => {
        ContactsService.GetMyContacts()
            .then((response) => {
                this.ListItem = [];

                if(response.success){
                    this.ListItem = response.result.items;
                    callback(response.result.totalCount);
                } else {
                    callback(0);
                }
            });
    }
}