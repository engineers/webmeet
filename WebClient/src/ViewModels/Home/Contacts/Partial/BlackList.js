﻿import {ContactsService} from 'Services/Home/ContactsService';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';

@inject(EventAggregator, I18N)
export class BlackList {
    constructor(eventAggregator, i18n) {
        this.eventAggregator = eventAggregator;
        this.i18n = i18n;

        this.BlackListSub = this.eventAggregator.subscribe('BlackListScreenEvent', payload => {
            this.OnTabEnter();
        });
    }

    unbind() {
        this.BlackListSub.dispose();
    }

    OnTabEnter(){
        this.SearchTrigger = !this.SearchTrigger;
    }

    RemoveFromList = (person) => {
        ContactsService.RemoveFromBlackList(person.id)
           .then((response) => {
               if(response.success) {
                   Toastr.success(this.i18n.tr('Contacts.Groups.BlackList.RemoveFromListSuccessContent'), 
                       this.i18n.tr('Contacts.Groups.BlackList.RemoveFromListSuccessTitle'),
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                   this.OnTabEnter();
               } else {
                   Toastr.warning(this.i18n.tr('Contacts.Groups.BlackList.RemoveFromListFailedContent') + Toastr.AddErrorsFromResponse(response, this.i18n), 
                       this.i18n.tr('Contacts.Groups.BlackList.RemoveFromListFailedTitle'),
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
               }
           });
    }

    OnSearch = (gridParam, callback) => {
        ContactsService.GetBlackList()
          .then((response) => {
              this.ListItem = [];

              if(response.success){
                  this.ListItem = response.result.items;
                  callback(response.result.totalCount);
              } else {
                  callback(0);
              }
          });
    }
}