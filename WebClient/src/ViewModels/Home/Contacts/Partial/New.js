﻿import {NewContacts} from 'ClientObjects/Contacts/Common';
import {ContactsService} from 'Services/Home/ContactsService';
import {Toastr} from 'Helpers/Toastr';
import {BaseViewModel} from 'ViewModels/BaseViewModel';
import {I18N} from 'aurelia-i18n';
import {inject} from 'aurelia-framework';

@inject(I18N)
export class New extends BaseViewModel {
    constructor(i18n) {
        super();
        this.i18n = i18n;
        this.SearchModel = new NewContacts();
    }

    OnSearch = (gridParam, callback) => {

        this.SearchModel.SkipCount = gridParam.SkipCount;
        this.SearchModel.MaxResultCount = gridParam.MaxResultCount;

        ContactsService.Search(this.SearchModel)
           .then((response) => {
               this.SearchResult = [];
               if(response.success){
                   this.SearchResult = response.result.items;
                   callback(response.result.totalCount);
               } else {
                   callback(0);
               }
           });
    }

    Invite(selectedUser) {
        ContactsService.Invite(selectedUser.id)
            .then((response) => {
                if(response.success) {
                    Toastr.success(this.i18n.tr('Contacts.Groups.FindNew.SentInvitationSuccess'), 
                        this.i18n.tr('Common.ActionFinishedSuccess'),
                        {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                    selectedUser.isInvited = true;
                } else {
                    Toastr.warning(this.i18n.tr('Contacts.Groups.FindNew.SentInvitationFailed') + Toastr.AddErrorsFromResponse(response, this.i18n),
                        this.i18n.tr('Common.SomethingWentWrong'),
                        {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                }
            });
    }

    AddToBlackList(selectedUser) {
        ContactsService.AddToBlackList(selectedUser.id)
           .then((response) => {
               if(response.success) {
                   Toastr.success(this.i18n.tr('Contacts.Groups.FindNew.AddToBlackListSuccess'), 
                       this.i18n.tr('Common.ActionFinishedSuccess'),
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
               } else {
                   Toastr.warning(this.i18n.tr('Contacts.Groups.FindNew.AddToBlackListFailed') + Toastr.AddErrorsFromResponse(response, this.i18n), 
                       this.i18n.tr('Common.SomethingWentWrong'),
                       {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
               }
           });
    }
}