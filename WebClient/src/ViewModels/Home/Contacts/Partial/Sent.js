﻿import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {ContactsService} from 'Services/Home/ContactsService';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';

@inject(EventAggregator, I18N)
export class Sent {
    constructor(eventAggregator, i18n) {
        this.eventAggregator = eventAggregator;
        this.i18n = i18n;

        this.SentSub = this.eventAggregator.subscribe('SentScreenEvent', payload => {  
            this.OnTabEnter();
        });
    }

    unbind() {
        this.SentSub.dispose();
    }

    OnTabEnter = () => {
        this.SearchTrigger = !this.SearchTrigger;
    }

    CancelInvitation = (invitation) => {
        ContactsService.CancelInvitation(invitation.id)
            .then((response) => {
                if(response.success) {
                    Toastr.success(this.i18n.tr('Contacts.Groups.Sent.InvitationToGroupFriend') + invitation.receiverName + this.i18n.tr('Contacts.Groups.Sent.HasBeenCanceled'), 
                      this.i18n.tr('Common.ActionFinishedSuccess'),
                      {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});

                    this.OnTabEnter();
                } else {
                    Toastr.warning(this.i18n.tr('Contacts.Groups.Sent.SentInvitationFailed') + invitation.receiverName + Toastr.AddErrorsFromResponse(response, this.i18n),
                     this.i18n.tr('Common.SomethingWentWrong'),
                     {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                }
            });
    }

    OnSearch = (gridParam, callback) => {
        ContactsService.GetSentInvitations(gridParam)
            .then((response) => {
                this.ListItem = [];

                if(response.success){
                    this.ListItem = response.result.items;
                    callback(response.result.totalCount);
                } else {
                    callback(0);
                }
            });
    }
}