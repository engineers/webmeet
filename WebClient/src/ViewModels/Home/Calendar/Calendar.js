﻿import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {ObserverLocator} from 'aurelia-binding';
import {Local} from 'Helpers/LocalHelper';

@inject(EventAggregator, ObserverLocator)
export class Calendar {
    GroupId = 0;
    GroupName = '';
    IsGroup = false;
    Contact = null;

    ShowMessages = true;
    ShowTalks = true;
    ShowEvents = true;

    MessagesPanelClass = 'active';
    TalksPanelClass = '';
    EventsPanelClass = '';

    TabIds = ['MessagesTab', 'TalksTab', 'EventsTab'];


    constructor(eventAggregator, observerLocator) {
        var self = this;

        this.eventAggregator = eventAggregator;
        this.observerLocator = observerLocator;

        var selectedContact = Local.GetObject('SelectedContact');

        if(selectedContact) {
            this.RewriteContact(selectedContact);
        }

        this.SelectContactSub = this.eventAggregator.subscribe('SelectContactEvent', contact => {
            this.RewriteContact(contact);
        });

        this.CalendarSub = this.eventAggregator.subscribe('CalendarEventsScreenEvent', payload => {
            this.IsGroup = false;
            this.ShowEvents = true;
        });

        this.ChangeActiveTabSub = this.eventAggregator.subscribe('ChangeActiveTabEvent', tabId => {
            for (var i = 0; i < self.TabIds.length; ++i) {
                var id = self.TabIds[i];

                if (id === tabId) {
                    $(('#' + id)).addClass('active');
                    $(('#' + id + ' div:nth-child(1)')).addClass('active');
                    $(('#' + id + ' div:nth-child(2)')).css('display', 'block');
                } else {
                    $(('#' + id)).removeClass('active');
                    $(('#' + id + ' div:nth-child(1)')).removeClass('active');
                    $(('#' + id + ' div:nth-child(2)')).css('display', 'none');  
                } 
            }
        });
    }

    attached(){
        $('.collapsible').collapsible({
            accordion: false
        });
        $('#addEventButton').click((e) => {
            e.stopPropagation();
            this.AddNewEvent();
        });
        $('.modal-trigger').leanModal();
    }

    unbind() {
        this.CalendarSub.dispose();
        this.SelectContactSub.dispose();
        this.ChangeActiveTabSub.dispose();

        Local.SetObject('SelectedContact', undefined);
    }

    RewriteContact = (contact) => {
        this.GroupId = contact.Id;
        this.GroupName = contact.Name;
        this.IsGroup = contact.IsGroup;
        this.ShowTalks = true;
        this.ShowEvents = this.IsGroup;
        this.Contact = contact;
    }

    AddNewEvent(){
        this.eventAggregator.publish('CreateNewEvent');
        $('#createEvent').openModal();
    }
  
    UpdateScreen(screenId) {
        switch(screenId){
            case 1:  this.eventAggregator.publish('CalendarTalksScreenEvent'); break;
            case 2:  this.eventAggregator.publish('CalendarEventsScreenEvent'); break;
            default: break;
        }
    }
}