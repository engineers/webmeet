﻿import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {ConversationsService} from 'Services/Home/ConversationsService';
import {Chat} from 'Helpers/Chat';
import {AppRouter} from 'aurelia-router';

@inject(EventAggregator, AppRouter)
export class Talks {
    constructor(eventAggregator, router) {
        var self = this;

        this.eventAggregator = eventAggregator;
        this.router = router;

        this.ActiveContact = null;
        this.talk = Chat.talk;

        this.SelectContactSub = this.eventAggregator.subscribe('SelectContactEvent', contact => {
            self.ActiveContact = contact;
            self.ReceiverId = contact.Id;
            self.GetConversations();
        }); 
        
        this.AcceptTalkSub = this.eventAggregator.subscribe('AcceptTalkEvent', contact => {
            self.talk = Chat.talk;
            self.talk = self.talk;
            self.link = location.protocol + '//' + document.location.host + '#Guest?roomId=' + self.talk.Id;
        });  
        
        this.TalkClearSub = this.eventAggregator.subscribe('TalkEndedEvent', contact => {
            self.talk = null;
        });

        this.TalkCreatedSub = this.eventAggregator.subscribe('TalkCreatedEvent', talk => {
            self.talk = talk;
            self.link = location.protocol + '//' + document.location.host + '#Guest?roomId=' + talk.Id;
        });

        $('#startConversation').click((e) => {
            e.stopPropagation();

            if ($('#startConversation').attr('class').indexOf('disabled') === -1 && Chat.talk == null) {
                Chat.CreateNewTalk();
                self.talk = Chat.talk;
                self.eventAggregator.publish('ChangeActiveTabEvent', 'TalksTab');
            }
        });
        this.link = '';
    }

    unbind() {
        this.TalkCreatedSub.dispose();
        this.SelectContactSub.dispose();
        this.AcceptTalkSub.dispose();
        this.TalkClearSub.dispose();
    }

    StopTalk() {
        Chat.StopTalkRequest();
    }

    GetConversations() {
        var self = this;

        ConversationsService.GetMyConversations(self.ReceiverId)
            .then((res) => {
                self.Conversations = [];
                if(res.success) {
                    res.result.items.forEach((item) => {
                        item.calleeName = Chat.Contacts[item.personBaseId].Name;
                        item.startDate = new Date(item.startDate).toLocaleString();
                        item.endDate = item.endDate !== null ? new Date(item.endDate).toLocaleString() : item.endDate;
                    });

                    self.Conversations = res.result.items.reverse();
                }
            });
    }



}