﻿import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {I18N} from 'aurelia-i18n';
import {computedFrom} from 'aurelia-binding';
import {CalendarService} from 'Services/Home/CalendarService';
import {Toastr} from 'Helpers/Toastr';
import {Local} from 'Helpers/LocalHelper';

@inject(EventAggregator, I18N)
export class Events {
    constructor(eventAggregator, i18n) {
        this.eventAggregator = eventAggregator;
        this.i18n = i18n;
        this.SetDefaultDate();

        var selectedContact = Local.GetObject('SelectedContact');

        if(selectedContact) {
            this.RewriteContact(selectedContact);
        }

        this.GetMyEvents();
    }

    attached(){
        this.CalendarSub = this.eventAggregator.subscribe('CalendarEventsScreenEvent', payload => {
            if(payload.OwnEvent){
                this.SetDefaultDate();
                this.GetMyEvents();
            }
        });

        this.SelectContactSub = this.eventAggregator.subscribe('SelectContactEvent', contact => {
            this.events = [];
            this.RewriteContact(contact);
            this.GetMyEvents();
        });

        this.RefreshSub = this.eventAggregator.subscribe('RefreshGridEvent', contact => {
            this.GetMyEvents();
        });
    }

    unbind() {
        this.CalendarSub.dispose();
        this.SelectContactSub.dispose();
        this.RefreshSub.dispose();
    }

    RewriteContact = (contact) => {
        this.GroupId = contact.Id;
        this.GroupName = contact.Name;
        this.IsGroup = contact.IsGroup;
    }

    @computedFrom('CurrentMonth')
    get MonthName() {
        return this.i18n.tr('Months.' + this.CurrentMonth);
    }

    SetDefaultDate = () => {
        this.CurrentMonth = new Date().getMonth() + 1;
        this.CurrentYear = new Date().getFullYear();
        this.GroupId = 0;
    }

    PreviousMonth() {
        this.CurrentMonth -= 1;

        if(this.CurrentMonth == 0) {
            this.CurrentMonth = 12;
            this.CurrentYear -= 1;
        }

        this.GetMyEvents();
    }

    NextMonth() {
        this.CurrentMonth += 1;

        if(this.CurrentMonth == 13) {
            this.CurrentMonth = 1;
            this.CurrentYear += 1;
        }

        this.GetMyEvents();
    }

    EditEvent = (event) => {
        this.eventAggregator.publish('EditEvent', event);
        $('#createEvent').openModal();
    }

    GetMyEvents = () => {
        if(this.GroupId > 0){
            this.GetGroupEvents();
            return;
        }

        if(this.IsBusy){
            return;
        }

        this.IsBusy = true;

        var startDate = new Date(this.CurrentYear,  this.CurrentMonth - 1, 1, 1);
        var endDate = new Date( this.CurrentMonth < 12 ? this.CurrentYear : this.CurrentYear + 1,  this.CurrentMonth < 12 ? this.CurrentMonth : 0, 0, 1);

        startDate.setHours(0, -startDate.getTimezoneOffset(), 0, 0);
        endDate.setHours(0, -endDate.getTimezoneOffset(), 0, 0);

        var request = {
            StartDateTime: startDate,
            EndDateTime: endDate
        };

        CalendarService.GetMyEvents(request)
            .then((res) => {
                this.events = [];
                if(res.success){
                    res.result.items.forEach((e) => {
                        this.events.push({
                            Title: e.title,
                            Description: e.description,
                            Date: e.date,
                            Id: e.id
                        });
                    }); 
                }

                this.IsBusy = false;
            });
    }

    GetGroupEvents = () => {
        if(this.GroupId == 0){
            this.GetMyEvents();
            return;
        }

        if(this.IsBusy){
            return;
        }

        this.IsBusy = true;

        var startDate = new Date(this.CurrentYear,  this.CurrentMonth - 1, 1, 1);
        var endDate = new Date( this.CurrentMonth < 12 ? this.CurrentYear : this.CurrentYear + 1,  this.CurrentMonth < 12 ? this.CurrentMonth : 0, 0, 1);

        startDate.setHours(0, -startDate.getTimezoneOffset(), 0, 0);
        endDate.setHours(0, -endDate.getTimezoneOffset(), 0, 0);

        var request = {
            StartDateTime: startDate,
            EndDateTime: endDate,
            GroupId: this.GroupId
        };

        CalendarService.GetGroupEvents(request)
            .then((res) => {
                this.events = [];
                if(res.success){
                    res.result.items.forEach((e) => {
                        this.events.push({
                            Title: e.title,
                            Description: e.description,
                            Date: e.date,
                            Id: e.id
                        });
                    }); 
                }

                this.IsBusy = false;
            });
    }

    RemoveEvent = (event) => {
        if(this.IsGroup){
            CalendarService.RemoveGroupEvent(event.Id)
            .then((res) => {
                if(res.success) {
                    this.events = Enumerable.From(this.events).Where(e => e.Id != event.Id).ToArray();
                }
                this.RemoveEventShowToast(res);
            });
        } 
        else {
            CalendarService.RemoveMyEvent(event.Id)
            .then((res) => {
                if(res.success) {
                    this.events = Enumerable.From(this.events).Where(e => e.Id != event.Id).ToArray();
                }
                this.RemoveEventShowToast(res);
            });
        }
    }

    RemoveEventShowToast = (res) => {
        if(res.success){
            Toastr.success(this.i18n.tr('Calendar.Events.RemoveEventSuccessContent'),
                this.i18n.tr('Calendar.Events.RemoveEventSuccessTitle'));
        }
        else {
            Toastr.error(this.i18n.tr('Calendar.Events.RemoveEventFailed') + Toastr.AddErrorsFromResponse(res, this.i18n),
                        this.i18n.tr('Common.SomethingWentWrong'));
        }
    }
}