﻿import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {MessagesService} from 'Services/Home/MessagesService';
import {Chat} from 'Helpers/Chat';

@inject(EventAggregator)
export class Messages {
    constructor(eventAggregator) {
        var self = this;

        this.Messages = [];
        this.ReceiverId = null;
        this.ActiveContact = null;
        this.messageContent = '';

        this.eventAggregator = eventAggregator;

        this.ActiveContact = Chat.ActiveContact;

        if (this.ActiveContact !== null) {
            this.ReceiverId = this.ActiveContact.Id;
            this.GetMessages();
        }

        this.SelectContactSub = this.eventAggregator.subscribe('SelectContactEvent', contact => {
            self.ActiveContact = contact;
            self.ReceiverId = contact.Id;
            self.GetMessages();
        });

        this.UserMessageSub = this.eventAggregator.subscribe('UserMessageEvent', message => {
            self.Messages.unshift(message);
        });
    }

    unbind() {
        this.SelectContactSub.dispose();
        this.UserMessageSub.dispose();
    }

    GetMessages() {
        var self = this;

        MessagesService.GetMessages(self.ReceiverId)
            .then((res) => {
                self.Messages = [];
                if(res.success) {
                    res.result.items.forEach((item) => {
                        item.From = item.senderId !== Chat.me.Id;
                        item.SenderName = Chat.Contacts[item.senderId].Name;
                        item.creationTime = new Date(item.creationTime).toLocaleString();
                    });

                    self.Messages = res.result.items.reverse();
                }
            });
    }

    OnEnter(e) {
        if(e.keyCode === 13) {
            if (this.messageContent) {
                var self = this;

                Chat.SendUserMessage(this.ReceiverId, this.messageContent, (message) => {
                    self.Messages.unshift(message);
                    self.messageContent = '';
                });
            }
        }

        return true;
    }

}