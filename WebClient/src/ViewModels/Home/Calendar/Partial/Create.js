﻿import {MaterializeHelper} from 'Helpers/MaterializeHelper';
import {Validation} from 'aurelia-validation';
import {inject} from 'aurelia-framework';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';
import {CalendarService} from 'Services/Home/CalendarService';
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(Validation, I18N, EventAggregator)
export class Create {
    EventId = 0;
    Title = '';
    Description = '';
    Date = '';
    DateIsValid = undefined;
    Counter = 0;
    GroupId = 0;

    constructor(validation, i18n, eventAggregator){
        this.validation = validation;
        this.i18n = i18n;

        this.eventAggregator = eventAggregator;

        this.EventValidators = this.validation.on(this)
                    .ensure('Title')
                        .isNotEmpty()
                        .withMessage(this.i18n.tr('Calendar.Create.ValidationMesssages.TitleIsRequired'))
                    .ensure('DateIsValid')
                        .isNotEmpty()
                        .withMessage(this.i18n.tr('Calendar.Create.ValidationMesssages.DateIsRequired'));

        this.EventValidators.validate().then(() => {}).catch(() => {});
    }

    attached() {
        $('#description').characterCounter();

        this.picker = MaterializeHelper.InitCalendar({ 
            selectMonths: true, 
            selectYears: 150,
            container: 'body',
            min: [2016, 0, 1],
            max: [2025, 12, 0],
            today: false,
            onChange: (date) => {
                date.setHours(0, -date.getTimezoneOffset(), 0, 0);
                this.Date = date;

                setTimeout(() => {
                    var eventDateVal = $('#eventDate').val();
                    if(eventDateVal){
                        this.DateIsValid = eventDateVal;
                    }
                }, 30);
            }
        });

        this.EditSub = this.eventAggregator.subscribe('EditEvent', (event) => {
            this.EventId = event.Id;
            this.Title = event.Title;
            this.Description = event.Description;
            var date = new Date(Date.parse(event.Date.substring(0, 10)));
            this.picker.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);    
        });

        this.CreateNewSub = this.eventAggregator.subscribe('CreateNewEvent', (event) => {
            this.EventId = 0;
            this.Title = '';
            this.Description = '';   
            this.DateIsValid = '';
        });

        this.CalendarSub = this.eventAggregator.subscribe('CalendarEventsScreenEvent', payload => {
            if(payload.OwnEvent){
                this.GroupId = 0;
            }
        });

        this.SelectContactSub = this.eventAggregator.subscribe('SelectContactEvent', contact => {
            this.GroupId = contact.Id;
            this.GroupName = contact.Name;
            this.IsGroup = contact.IsGroup;
        });
    }

    unbind() {
        this.EditSub.dispose();
        this.CreateNewSub.dispose();
        this.CalendarSub.dispose();
        this.SelectContactSub.dispose();
    }

    Save = () => {
        this.ValidateDate = true;
        this.EventValidators.validate()
            .then(() => {
                var request = {
                    Title: this.Title,
                    Description: this.Description,
                    Date: this.Date
                };

                if(this.GroupId > 0){
                    request.GroupId = this.GroupId;
                    if(this.EventId){
                        CalendarService.UpdateGroupEvent(request, this.EventId)
                       .then((res) => {
                           if(res.success){
                               this.RefreshGrid();
                               this.CloseModal();
                           }
                           this.ShowToastMessage(res);
                       });
                    } else {
                        CalendarService.CreateGroupEvent(request)
                       .then((res) => {
                           if(res.success){
                               this.RefreshGrid();
                               this.CloseModal();
                           }
                           this.ShowToastMessage(res);
                       });
                    }
                } 
                else {
                    if(this.EventId){
                        CalendarService.UpdateMyEvent(request, this.EventId)
                       .then((res) => {
                           if(res.success){
                               this.RefreshGrid();
                               this.CloseModal();
                           }
                           this.ShowToastMessage(res);
                       });
                    } else {
                        CalendarService.CreateMyEvent(request)
                       .then((res) => {
                           if(res.success){
                               this.RefreshGrid();
                               this.CloseModal();
                           }
                           this.ShowToastMessage(res);
                       });
                    }
                }
            })
            .catch(() => {});
    }

    RefreshGrid = () => {
        this.eventAggregator.publish('RefreshGridEvent');
    }

    ShowToastMessage = (res) => {
        if(res.success) {
            Toastr.success(this.i18n.tr('Calendar.Create.SaveChangesSuccess'),
                this.i18n.tr('Common.SaveSuccessMessage'));

        } 
        else {
            Toastr.error(this.i18n.tr('Calendar.Create.SaveChangesFailed') + Toastr.AddErrorsFromResponse(res, this.i18n),
                this.i18n.tr('Common.SomethingWentWrong'));
        }
    }

    CloseModal = () => {
        $('#createEvent').closeModal();
    }
}