﻿import {LoginService} from 'Services/Account/LoginService';

export class ConfirmEmail {
    Loader = true;
    Success = false;


    attached(){
        this.ConfirmEmail();
    }

    ConfirmEmail(){
        LoginService.ConfirmEmail()
            .then((response) => {

                if(response.success){
                    this.Success = true;

                    setTimeout(function(){
                        window.location.href = '';
                    }, 5000);
                }
            },
                (errorResponse) => {
                }
            )
            .fin(() => {
                this.Loader = false;
            });
    }

    GoToMainPage() {
        window.location.href = '';
    }
}