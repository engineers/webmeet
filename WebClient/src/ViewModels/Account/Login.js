﻿import {Validation} from 'aurelia-validation';
import {inject, Aurelia} from 'aurelia-framework';
import {MaterializeHelper} from 'Helpers/MaterializeHelper';
import {Login, Registration} from 'ClientObjects/Account/Login';
import {LoginService} from 'Services/Account/LoginService';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';


@inject(Validation, Login, Aurelia, Registration, I18N)
export class LoginModel {
    LoginMode = true;
    constructor(validation, login, aurelia, registration, i18n) {
        this.validation = validation;
        this.Login = login;
        this.Registration = registration;
        this.ValidatorsLogin = this.validation.on(this.Login);
        this.ValidatorsRegister = this.validation.on(this.Registration);
        this.app = aurelia;
        this.i18n = i18n;

        this.ValidatorsLogin.validate().then(() =>{}).catch(() =>{});
        this.ValidatorsRegister.validate().then(() =>{}).catch(() =>{});

        this.IsBusy = false;
        this.BeforeRegister = true;
        this.jQuery = $;
    }

    activate() {
    }

    attached() {
        MaterializeHelper.InitCalendar({ 
            selectMonths: true, 
            selectYears: 150,
            min: [1950, 1, 1],
            max: [2010, 12, 0],
            today: false,
            onChange: (date) => {
                this.Registration.DateOfBirth = date;
            }
        });
        MaterializeHelper.InitCollapsilable({ accordion: true });

        $('.modal-trigger').leanModal();           
    }
   
    SignIn() {
        this.ValidatorsLogin.validate()
            .then(() => {           
                this.IsBusy = true;
                LoginService.Login(this.Login.Username, this.Login.Password, this.Login.RemeberMe)
                    .then(() => {
                        this.app.setRoot('app');
                    })
                     .catch(() => {
                         Toastr.error('<p>' + this.i18n.tr('Account.Login.NotifyMessages.FailedMessage') + '</p>',
                             this.i18n.tr('Common.SomethingWentWrong'), 
                             {'closeButton': true, timeOut: 12000});
                         
                     })
                     .fin(() => {
                         this.IsBusy = false;
                     });
                })
            .catch(() => {
            });
    }

    Register() {
        this.RegisterErrorMessage = [];
        this.ValidatorsRegister.validate()
        .then(() => {
            this.IsBusy = true;
            LoginService.Register(this.Registration)
            .then((response) => {
                if(response.success){
                    Toastr.success('<p>' + this.i18n.tr('Account.Registration.NotifyMessages.SuccessMessage') + '</p> <p>' + this.i18n.tr('Account.Registration.NotifyMessages.CheckMailbox') + '</p>', 
                        this.i18n.tr('Account.Registration.NotifyMessages.Welcome'));
                    this.BeforeRegister = false;
                } else {
                    if(response.error.validationErrors && response.error.validationErrors.length > 0){
                        for(var i = 0; i < response.error.validationErrors.length; i++) {
                            this.RegisterErrorMessage.push(response.error.validationErrors[i].message);
                        }
                    }

                    if((!response.error.validationErrors || response.error.validationErrors.length == 0) && response.error.message){
                        this.RegisterErrorMessage.push(response.error.message);
                    }
                }
            })
            .catch(() => {
            })
            .fin(() => {
                this.IsBusy = false;
            });
        })
        .catch(() => {});
    }

    Back() {
        this.BeforeRegister = true;
    }
}
