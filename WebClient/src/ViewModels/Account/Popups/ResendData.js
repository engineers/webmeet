﻿import {ResendEmail} from 'ClientObjects/Account/Login';
import {inject} from 'aurelia-framework';
import {Validation} from 'aurelia-validation';
import {Toastr} from 'Helpers/Toastr';
import {LoginService} from 'Services/Account/LoginService';
import {I18N} from 'aurelia-i18n';

@inject(ResendEmail, Validation, I18N)
export class ResendData {
    constructor(resentEmail, validation, i18n){

        this.ResendEmail = resentEmail;
        this.Validation = validation;
        this.i18n = i18n;

        this.EmailValidator = this.Validation.on(this.ResendEmail);

        this.EmailValidator.validate().then(() => {}).catch(() => {});
    }

    ResendConfirmationEmail() {
        this.EmailValidator.validate()
            .then((res) => {
                LoginService.ResendConfirmationEmail(this.ResendEmail.Email, this.ResendEmail.Username)
                    .then((response) => {
                        if(response.success){
                            Toastr.info(this.i18n.tr('Account.ResendData.NotifyMessages.NewActivationLinkWasSent'),
                                this.i18n.tr('Account.ResendData.NotifyMessages.SuccessGenerateNewActivationLink'),
                                {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                        } else{
                            Toastr.warning(this.i18n.tr('Account.ResendData.NotifyMessages.ErrorSendNewActivationLink') + Toastr.AddErrorsFromResponse(response, this.i18n),
                            this.i18n.tr('Common.SomethingWentWrong'),
                            {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                        }
                    })
                    .catch(() =>{
                        Toastr.warning(this.i18n.tr('Account.ResendData.NotifyMessages.ErrorSendNewActivationLink') + Toastr.AddErrorsFromResponse(response, this.i18n),
                            this.i18n.tr('Common.SomethingWentWrong'),
                            {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                    })
                    .fin(() =>{
                        $('#resendData').closeModal();
                    });
            })
        .catch((err) =>{          
        });
    }

    ForgotPassword(){
        this.EmailValidator.validate()
            .then((res) => {
                LoginService.ForgotPassword(this.ResendEmail.Email, this.ResendEmail.Username)
                    .then((response) => {
                        if(response.success){
                            Toastr.info(this.i18n.tr('Account.ResendData.NotifyMessages.NewPasswordLinkWasSent'),
                                this.i18n.tr('Account.ResendData.NotifyMessages.SuccessGenerateNewPasswordLink'),
                                {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                        } else{
                            Toastr.warning(this.i18n.tr('Account.ResendData.NotifyMessages.ErrorSendNewPasswordLink') + Toastr.AddErrorsFromResponse(response, this.i18n),
                           this.i18n.tr('Common.SomethingWentWrong'),
                           {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                        }
                    })
                    .catch(() =>{
                        Toastr.warning(this.i18n.tr('Account.ResendData.NotifyMessages.ErrorSendNewPasswordLink') + Toastr.AddErrorsFromResponse(response, this.i18n),
                            this.i18n.tr('Common.SomethingWentWrong'),
                            {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                    })
                    .fin(() =>{
                        $('#resendData').closeModal();
                    });
            })
        .catch((err) =>{          
        });
    }
}