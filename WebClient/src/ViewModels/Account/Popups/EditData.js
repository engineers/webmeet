﻿import {EditUserData} from 'ClientObjects/Account/Login';
import {Validation} from 'aurelia-validation';
import {inject} from 'aurelia-framework';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';
import {UserService} from 'Services/Account/UserService';
import {Chat} from 'Helpers/Chat';
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(Validation, EditUserData, I18N, EventAggregator)
export class EditData {
    constructor(validation, userModel, i18n, eventAggregator) {
        this.i18n = i18n;
        this.validation = validation;
        this.eventAggregator = eventAggregator;
        this.EditModel = userModel;

        this.ValidatorsEdit = this.validation.on(this.EditModel);
        

        this.SuccessDownloadData = false;
    }

    activate() {
        UserService.GetMyInfo()
            .then(response => {
                if(response.success) {
                    this.SuccessDownloadData = true;

                    this.EditModel.Name = response.result.firstName;
                    this.EditModel.Surname = response.result.lastName;
                    this.EditModel.Address = response.result.address;
                    this.EditModel.EmailAddress = response.result.emailAddress;
                }
            });
    }

    attached(){
        this.ValidatorsEdit.validate().then(() =>{}).catch(() =>{});
    }

    SaveData = () => {
        var self = this;

        this.ValidatorsEdit.validate()
            .then(() => {
                UserService.UpdateMyInfo(this.EditModel)
                    .then(response => {
                        if(response.success) {
                            Chat.me.Name = response.result.firstName + ' ' + response.result.lastName;
                            self.eventAggregator.publish('MeUpdatedEvent');

                            Toastr.success(this.i18n.tr('Home.EditUser.SaveChangesSuccess'), 
                              this.i18n.tr('Common.ActionFinishedSuccess'),
                              {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});

                            $('#editUserData').closeModal();
                        } else {
                            Toastr.warning(this.i18n.tr('Home.EditUser.SaveChangesFailed') + Toastr.AddErrorsFromResponse(response, this.i18n), 
                              this.i18n.tr('Common.SomethingWentWrong'),
                              {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                        }
                    });
            })
            .catch(() => {});
    }
}