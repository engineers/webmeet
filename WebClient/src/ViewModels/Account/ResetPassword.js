﻿import {ResetPassword} from 'ClientObjects/Account/Login';
import {LoginService} from 'Services/Account/LoginService';
import {inject} from 'aurelia-framework';
import {Validation} from 'aurelia-validation';
import {Toastr} from 'Helpers/Toastr';
import {I18N} from 'aurelia-i18n';

@inject(ResetPassword, Validation, I18N)
export class ResetPasswordClass {
    Loader = false;

    constructor(resetPassword, validation, i18n) {
        this.ResetModel = resetPassword;
        this.Validation = validation;
        this.Validator = this.Validation.on(this.ResetModel);
        this.Validator.validate().then(() => {}).catch(() => {});
        this.i18n = i18n;
    }

    ChangePassword(){
        this.Validator.validate()
        .then((response) =>{
            this.Loader = true;
            LoginService.ChangePassword(this.ResetModel.Password, this.ResetModel.ConfirmPassword)
                    .then((response) => {
                        if(response.success){
                            Toastr.info(this.i18n.tr('Account.ResetPassword.NotifyMessages.SuccessAndGoToLogin'),
                                this.i18n.tr('Account.ResetPassword.NotifyMessages.SuccessMessage'),
                                {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                            setTimeout(this.GoToMainPage, 5000);
                        } else{
                            throw 'Send email fail';
                        }
                    })
                    .catch(() =>{
                        Toastr.error(this.i18n.tr('Account.ResetPassword.NotifyMessages.FailedAndCheckData'),
                            this.i18n.tr('Account.ResetPassword.NotifyMessages.FailedMessage'),
                            {closeButton: true, timeOut: 20000, positionClass: 'toast-top-full-width'});
                    })
                    .fin(() =>{
                        this.Loader = false;
                    });
        })
        .catch(() => {
        });
    }

    GoToMainPage() {
        window.location.href = '';
    }
}