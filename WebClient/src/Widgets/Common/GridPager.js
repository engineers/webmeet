﻿import {customElement, bindable, inject} from 'aurelia-framework';
import {computedFrom, ObserverLocator} from 'aurelia-binding';

@customElement('grid')
@inject(ObserverLocator, Element)
export class GridPager { 
    @bindable items;
    @bindable searchbutton;
    @bindable searchtrigger;
    @bindable source;
    @bindable autofill = false;

    CurrentPage = 1;
    SelectPage = 1;
    PageSize = 10;
    TotalCount = 10;

    IsBusy = false;

    constructor(observerLocator, element) {
        this.observerLocator = observerLocator;
        this.element = element;
    }

    bind(context){
        this.$parent = context;
    }

    attached() {
        if(this.searchbutton) {
           this.searchbutton.addEventListener('click', this.SearchAction);
        }
        if(this.autofill) {
            this.SearchAction();
        }

        this.observerLocator.getObserver(this, 'searchtrigger')
                            .subscribe(this.SearchAction);
    }

    unbind() {
        if(this.searchbutton) {
            this.searchbutton.removeEventListener('click', this.SearchAction);
        }
    }

    @computedFrom('TotalCount', 'PageSize')
    get TotalPage() {
        return Math.ceil(this.TotalCount / this.PageSize);
    }

    @computedFrom('CurrentPage')
    get HasPreviousPage() {
        return this.CurrentPage > 1;
    }

    @computedFrom('CurrentPage', 'PageSize', 'TotalPage')
    get pages() {
        var startPage;
        var endPage;
        var result = [];
        this.HasMoreBefore = false;

        switch(this.CurrentPage)
        {
            case 1: startPage = 1; break;
            case 2: startPage = 1; break;
            case 3: startPage = 1; break;
            default: startPage = this.CurrentPage - 2; this.HasMoreBefore = true; break;
        }

        this.HasMoreAfter = false;

        switch(this.CurrentPage)
        {
            case this.TotalPage: endPage = this.CurrentPage; break;
            case this.TotalPage - 1: endPage = this.CurrentPage + 1; break;
            case this.TotalPage - 2: endPage = this.CurrentPage + 2; break;
            default: endPage = this.CurrentPage + 2; this.HasMoreAfter = true; break;
        }

        for (var i = startPage; i <= endPage; i++) {
            result.push({
                Text: i, Class: i == this.CurrentPage ? 'active' : ''
            });
        }

        return result;
    }

    HasMoreBefore = false;
    HasMoreAfter = false;

    @computedFrom('CurrentPage', 'TotalPage')
    get HasNextPage() {
        return this.CurrentPage < this.TotalPage;
    }


    @computedFrom('SelectPage', 'PageSize')
    get SkipCount() {
        return this.PageSize * (this.SelectPage - 1);
    }

    ChangePage(pageNumber) {
        if(this.CurrentPage != pageNumber && pageNumber > 0 && pageNumber <= this.TotalPage) {
            this.SelectPage = pageNumber;
            this.InvokeSearch();
        }
    }

    SearchAction = () => {
        this.SelectPage = 1;
        this.InvokeSearch();
    }

    InvokeSearch() {
        if(this.IsBusy) {
            return;
        }

        this.IsBusy = true;
        
        if(this.searchbutton){
            this.searchbutton.disabled = true;
            this.searchbutton.classList.add('disabled');
        }

        setTimeout(() => {
            this.source({
                SkipCount: this.SkipCount,
                MaxResultCount: this.PageSize
            }, (totalCount, isSucceed = true) => {
                this.TotalCount = totalCount;
                this.IsBusy = false;

                if(this.searchbutton){
                    this.searchbutton.disabled = true;
                    this.searchbutton.classList.remove('disabled');
                }
                

                if(isSucceed) {
                    this.CurrentPage = this.SelectPage;
                    if(this.TotalPage < this.SelectPage){
                        this.SelectPage = this.TotalPage;
                        this.InvokeSearch();
                    }
                }

               
            });
        }, 250);
    }
    
    GoToFirstPage() {
        this.ChangePage(1);
    }

    GoToPreviousPage() {
        this.ChangePage(this.CurrentPage - 1);
    }

    GoToLastPage(){
        this.ChangePage(this.TotalPage);
    }

    GoToNextPage() {
        this.ChangePage(this.CurrentPage + 1);
    }
}