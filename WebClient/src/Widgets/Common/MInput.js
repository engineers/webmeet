import {customElement, bindable} from 'aurelia-framework';
import {inject} from 'aurelia-framework';

@customElement('minput')
@inject(Element)
export class MInuput {
    @bindable model: any = null;
    @bindable icon: string = null;
    @bindable gridclass: string;
    @bindable mtype: string = 'text';
    @bindable label: string;
    @bindable len: any;
    @bindable iclass: any;

    constructor(element) {
      if(typeof MInuput.IdNumber == 'undefined'){
        MInuput.IdNumber = 1;
      }

        this.inputId = 'input_' + MInuput.IdNumber++;
        this.element = element;
    }

    attached() {
        if (this.len) {
            this.element.children[0].children[2].setAttribute('length', this.len);
            $('#' + this.inputId).characterCounter();
        } 
    }
}
