﻿import {inject} from 'aurelia-framework';
import {I18N} from 'aurelia-i18n';

@inject(Element, I18N)
export class TipCustomAttribute {
    constructor(element, i18n) {
        this.element = element;
        this.i18n = i18n;
    }

    valueChanged(newValue){
        var text = this.i18n.tr(newValue);
        if(!this.element.hasAttribute('data-position')){
            this.element.setAttribute('data-position', 'top');
        }

        this.element.setAttribute('tooltip', text);
        $(this.element).tooltip({delay: 50});
    }

    unbind(){
        $(this.element).tooltip('remove');
    }
}