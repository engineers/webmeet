import {bindable, inject, Aurelia} from 'aurelia-framework';
import {LoginService} from 'Services/Account/LoginService';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AppRouter} from 'aurelia-router';
import {Chat} from 'Helpers/Chat';

@inject(Aurelia, EventAggregator, AppRouter)
export class NavBar {
    @bindable router = null;

    constructor(aurelia, eventAggregator, router) {
        this.app = aurelia;
        this.eventAggregator = eventAggregator;
        this.router = router;

        this.MyName = Chat.me !== null ? Chat.me.Name : '';
    }

    attached() {
        var self = this;

        $('.modal-trigger').leanModal();
        $('.button-collapse').sideNav();

        this.eventAggregator.subscribe('MeUpdatedEvent', function() {
            self.MyName = Chat.me.Name;
        });
    }

    SignOut(){
        LoginService.Logout();
        window.location.href = '';
    }

    OnNavigate = (item) => {
        if(item.relativeHref == 'calendar' && this.router.currentInstruction.config.name == 'calendar') {
            this.eventAggregator.publish('CalendarEventsScreenEvent', {OwnEvent: true});
        }

        this.router.navigate(item.relativeHref);
    }
}
