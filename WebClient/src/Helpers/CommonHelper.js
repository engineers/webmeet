﻿export class CommonHelperClass {
    GetQueryVariable(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.hash);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    PrepareSupportList(i18n){
        var supportItem = 0;
        var arr = [];
        var item = new Object();
        item.Name = 'WebSockets';

        if (window.WebSocket){
            item.HasSupport = true;
            supportItem++;
        } else {
            item.HasSupport = false;
        }
        arr.push(item);

        item = new Object();
        item.Name = 'Geolocation';

        if (navigator.geolocation) {
            item.HasSupport = true;
            supportItem++;
        } else {
            item.HasSupport = false;
        }
        arr.push(item);

        item = new Object();
        item.Name = 'File API';

        if (!!('File' in window && 'FileReader' in window && 'FileList' in window && 'Blob' in window)) {
            item.HasSupport = true;
            supportItem++;
        } else {
            item.HasSupport = false;
        }
        arr.push(item);

        item = new Object();
        item.Name = 'Web Storage';

        if(typeof(Storage) !== 'undefined') {
            item.HasSupport = true;
            supportItem++;
        } else {
            item.HasSupport = false;
        }
        arr.push(item);

        item = new Object();
        item.Name = 'Web RTC';

        if(window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection || window.msRTCPeerConnection) {
            item.HasSupport = true;
            supportItem++;
        } else {
            item.HasSupport = false;
        }
        arr.push(item);


        item = new Object();
        item.Name = 'User Media';

        if(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia) {
            item.HasSupport = true;
            supportItem++;
        } else {
            item.HasSupport = false;
        }
        arr.push(item);

        var percentage = parseInt((100 * supportItem / 6).toFixed(0));
        this.SupportPercentage = 'width: ' + percentage + '%;'; 

        this.SupportText = '';

        if(percentage == 100) {
            this.SupportText = i18n.tr('Home.Panel.FullBrowserSupport');
        } else if(percentage > 45) {
            this.SupportText = i18n.tr('Home.Panel.PartialBrowserSupport');
        } else {
            this.SupportText = i18n.tr('Home.Panel.NoBrowserSupport');
        }

        return {
            SupportText: this.SupportText + ' (' + percentage + '%)',
            SupportItems: arr
        };
    }
}

export var CommonHelper = new CommonHelperClass();