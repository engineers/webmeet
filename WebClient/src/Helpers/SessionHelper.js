﻿export class SessionHelper {
    Password = 'Eujj/m#p%]qs%G,mw+vw<>2j"ZU_;54E';

    SetObject(key, obj) {
        this.SetString(key, JSON.stringify(obj));
    }

    SetString(key, str) {
        var encryptedKey = CryptoJS.MD5(key);
        var encryptedItem = CryptoJS.AES.encrypt(str, this.Password);
        sessionStorage.setItem(encryptedKey, encryptedItem);
    }

    GetObject(key) {
        var obj = this.GetString(key);
        return obj ? JSON.parse(obj) : undefined;
    }

    GetString(key) {
        var encryptedKey = CryptoJS.MD5(key);

        var sessionItem = sessionStorage.getItem(encryptedKey);

        if(!sessionItem)
        {
            return undefined;
        }

        var decryptedItem = CryptoJS.AES.decrypt(sessionItem, this.Password);
        return decryptedItem.toString(CryptoJS.enc.Utf8);
    }
}

export var Session = new SessionHelper();