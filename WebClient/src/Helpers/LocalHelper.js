﻿export class LocalHelper {
    Password = '<S3N@SN5@A>2[bp&EN#XM5h!_Lk5a==R';

    SetObject(key, obj) {
        this.SetString(key, JSON.stringify(obj));
    }

    SetString(key, str) {
        var encryptedKey = CryptoJS.MD5(key);
        var encryptedItem = CryptoJS.AES.encrypt(str, this.Password);
        localStorage.setItem(encryptedKey, encryptedItem);
    }

    GetObject(key) {
        var obj = this.GetString(key);
        return obj ? JSON.parse(obj) : undefined;
    }

    GetString(key) {
        var encryptedKey = CryptoJS.MD5(key);

        var localItem = localStorage.getItem(encryptedKey);

        if(!localItem)
        {
            return undefined;
        }

        var decryptedItem = CryptoJS.AES.decrypt(localItem, this.Password);
        return decryptedItem.toString(CryptoJS.enc.Utf8);
    }
}

export var Local = new LocalHelper();