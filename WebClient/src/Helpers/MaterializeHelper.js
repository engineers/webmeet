export class MaterializeHelperClass {
    InitCalendar(options: any) {
        var $input =  $('.datepicker').pickadate(options);
        return $input.pickadate('picker');
    }

    InitCollapsilable(options: any){
        $('.collapsible').collapsible(options);
    }

    InitCounterInput(){
        $('input, textarea').characterCounter();
    }

    InitDropDown(){
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: false,
            hover: true,
            gutter: 0,
            belowOrigin: true,
            alignment: 'left'
        });
    }

}

export var MaterializeHelper = new MaterializeHelperClass();
