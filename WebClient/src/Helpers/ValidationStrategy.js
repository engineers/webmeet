export class MaterializeStrategy {
    constructor() {
        this.bindingPathAttributes = ['validate', 'value.bind', 'value.two-way', 'model.two-way'];
    }

    getValidationProperty(validation, element) {
        let atts = element.attributes;
        for (let i = 0; i < this.bindingPathAttributes.length; i++) {
            let attributeName = this.bindingPathAttributes[i];
            let bindingPath;
            let validationProperty;
            if (atts[attributeName]) {
                bindingPath = atts[attributeName].value.trim();
                if (bindingPath.indexOf('|') !== -1) {
                    bindingPath = bindingPath.split('|')[0].trim();
                }
                var splitResult = bindingPath.split('.');
                var propertyName = splitResult[splitResult.length - 1];
                validationProperty = validation.result.properties[propertyName];
                if (attributeName === 'validate' && (validationProperty === null || validationProperty === undefined)) {
                    validation.ensure(bindingPath);
                    validationProperty = validation.result.properties[bindingPath];
                }
                return validationProperty;
            }
        }

        return null;
    }

    prepareElement(validationProperty, element) {
        this.AppendUi(validationProperty, element, true);
    }
    updateElement(validationProperty, element) {
        this.AppendUi(validationProperty, element, false);
    }

    AppendUi(validationProperty, currentElement, init) {
        if (init) {
            return;
        }

        var label = undefined;
        var div = undefined;

        if(currentElement.children.length != 0){
            div = currentElement.children[0];
        } 
        else {
            div = currentElement.parentNode;
        }

        for (var i = 0; i < div.children.length; i++) {
            if (div.children[i].nodeName == 'LABEL') {
                label = div.children[i];
            }
        }
        
        if (!label) {
            return;
        }

        var validClass = 'valid';
        var invalidClass = 'invalid';

        label.textContent = validationProperty.message;

        if (validationProperty && validationProperty.isDirty) {
            if (validationProperty.isValid) {
                div.classList.add(validClass);
                div.classList.remove(invalidClass);
            } else {
                div.classList.remove(validClass);
                div.classList.add(invalidClass);
            }
        } else {
            div.classList.remove(validClass);
            div.classList.remove(invalidClass);
        }
    }
}
