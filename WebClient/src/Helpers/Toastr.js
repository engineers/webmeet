﻿export class ToastrClass {
    defaultOption = {
        'closeButton': true,
        'debug': false,
        'newestOnTop': false,
        'progressBar': true,
        'positionClass': 'toast-top-center',
        'preventDuplicates': false,
        'showDuration': '300',
        'hideDuration': '1000',
        'timeOut': 0,
        'extendedTimeOut': 0,
        'showEasing': 'swing',
        'hideEasing': 'linear',
        'showMethod': 'fadeIn',
        'hideMethod': 'fadeOut',
        'tapToDismiss': false
    };

    copyOptions(options){
        if(!options){
            return;
        }

        for (var key in options) {
            toastr.options[key] = options[key];
        }
    }

    success(message, title, options){      
        this.copyOptions(options);
        toastr.success(message, title);
    }

    info(message, title, options){
        this.copyOptions(options);
        toastr.info(message, title);
    }

    error(message, title, options){
        this.copyOptions(options);
        toastr.error(message, title);
    }

    warning(message, title, options){
        this.copyOptions(options);
        toastr.warning(message, title);
    }

    AddErrorsFromResponse(response, i18n) {
        if(response.error && response.error.validationErrors && response.error.validationErrors.length == 0 && (!response.error.message || response.error.message == '')) {
            return '';
        }


        var message = '<p><strong>' + i18n.tr('Common.ReasonsFailure') + '</strong></p><ul>';

        if(response.error && (!response.error.validationErrors || response.error.validationErrors.length == 0)){
            message = message + '<li>' + response.error.message + '</li>';
        } else {
            for(var i = 0; i < response.error.validationErrors.length; i++) {
                message = message + '<li>' + response.error.validationErrors[i].message + '</li>';
            }
        }

        message = message + '</ul>';
       
        return message;
    }
}

export var Toastr = new ToastrClass();