﻿import {PersonsService} from 'Services/Home/PersonsService';
import {MessagesService} from 'Services/Home/MessagesService';
import {ConversationsService} from 'Services/Home/ConversationsService';
import {Toastr} from 'Helpers/Toastr';
import {Session} from 'Helpers/SessionHelper';

export class ChatClass { 
    constructor() {
        this.me = null;
        this.ActiveContact = null;
        this.ActivePersons = [];
        this.Contacts = null;

        this.EventListener = new EventListenerClass();
        this.WSSConnection = null;
        this.talk = null;
        this.myStream = null;


        window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
        window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;
        window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

        this.peerConnectionConfig = {
                iceServers: [
                    {urls: 'stun:stun.l.google.com:19302'},
                    {urls: 'stun:stun.anyfirewall.com:3478'},
                    {urls: 'turn:turn.bistri.com:80', credential: 'homeo', username: 'homeo'},
                    {urls: 'turn:turn.anyfirewall.com:443?transport=tcp', credential: 'webrtc', username: 'webrtc'}
                ]
        };

        this.mediaConstraints = {
            audio: true,
            video: true
        };

        this.GuestMode = false;
    }

    InjectEventAggregator(eventAggregator) {
        var self = this;
        this.eventAggregator = eventAggregator;

        this.eventAggregator.subscribe('SelectContactEvent', contact => {
            self.ActiveContact = contact;
        });
    }

    InjectRouter(router) {
        this.router = router;
    }

    InjectI18N(i18n) {
        this.i18n = i18n;
    }

    InitializeContacts(contacts) {
        var self = this;

        this.BindWebSocketEvents();

        this.Contacts = [];
        this.self = '';

        contacts.forEach((c) => {
            self.AddContact(c);
        });

        PersonsService.GetMe().then((result) => {
            if (result.success) {
                self.me = {
                    Id: result.result.id,
                    Name: result.result.firstName + ' ' + result.result.lastName,
                    IsGroup: false
                };
                self.eventAggregator.publish('MeUpdatedEvent');

                self.AddContact(self.me);
                if (self.WSSConnection === null) {
                    self.ConnectToWebsocket();
                }
                this.UpdateUsersStatuses();
            }
        });

    }

    UpdateUsersStatuses() {
        var self = this;

        this.Contacts.forEach(function(item) {
            if (!item.IsGroup) {
                item.Active = self.ActivePersons[item.Id] !== undefined && self.ActivePersons[item.Id] !== null;

                if (item.Active) {
                    item.MicrophoneAvaibility = self.ActivePersons[item.Id].MicrophoneAvaibility;
                    item.WebcamAvaibility = self.ActivePersons[item.Id].WebcamAvaibility;
                } else {
                    item.MicrophoneAvaibility = false;
                    item.WebcamAvaibility = false;
                }
            } else {
                var active = false;
                var mic = false;
                var webcam = false;

                for (var i = 0; i < item.Persons.length; ++i) {
                    var id = item.Persons[i];
                    var person = self.ActivePersons[id];
                    
                    if (id !== self.me.Id && person !== undefined && person !== null) {
                        active = true;
                        mic = mic || person.MicrophoneAvaibility;
                        webcam = webcam || person.WebcamAvaibility;
                    }
                }

                item.Active = active;
                item.MicrophoneAvaibility = mic;
                item.WebcamAvaibility = webcam;
            }
        });
    }

    BindWebSocketEvents() {
        var self = this;

        if (!this.GuestMode) {
            this.BindWebSocketEvent('UsersStatusChanged', function(data) {
                data.UsersStatusChanged.forEach(function(item) {
                    if (item.UserStatus === 'Connected') {
                        if (self.ActivePersons[item.Id] === undefined || self.ActivePersons[item.Id] === null) {
                            self.ActivePersons[item.Id] = {
                                MicrophoneAvaibility: false,
                                WebcamAvaibility: false
                            };
                        }
                    } else {
                        if (self.ActivePersons[item.Id] !== undefined && self.ActivePersons[item.Id] !== null) {
                            delete self.ActivePersons[item.Id];
                        }

                        if (self.talk !== null) {
                            var interlocutor = self.talk.GetInterlocutor(item.Id);

                            if (interlocutor != null && interlocutor.Status !== 'Rejected' && interlocutor.Status !== 'Stopped') {
                                self.OnInterlocutorDisconnect(interlocutor);
                            }
                        } 
                    }
                });

                self.UpdateUsersStatuses();
            });

            this.BindWebSocketEvent('UsersMediaAvailbility', function(data) {
                data.UsersMediaAvailbility.forEach(function(item) {
                    var person = self.ActivePersons[item.Id];

                    if (person !== undefined && person !== null) {
                        person.MicrophoneAvaibility = item.MicrophoneAvaibility;
                        person.WebcamAvaibility = item.WebcamAvaibility;
                    }
                });

                self.UpdateUsersStatuses();
            });

            this.BindWebSocketEvent('UserMessage', function(data) {
                var messageTitle;
                var isGroup = Chat.Contacts[data.UserMessage.ReceiverId].IsGroup;

                if (!isGroup) {
                    messageTitle = self.i18n.tr('Calendar.Messages.MessageFrom') + ' ' + Chat.Contacts[data.SenderId].Name;
                } else {
                    messageTitle = self.i18n.tr('Calendar.Messages.MessageFrom') + ' ' + Chat.Contacts[data.SenderId].Name + ' '
                        + self.i18n.tr('Calendar.Messages.InGroup') + ' ' + Chat.Contacts[data.UserMessage.ReceiverId].Name;
                }

                Toastr.info(escape(data.UserMessage.Content), messageTitle);

                if (self.router.history.fragment === '/calendar' 
                        && (isGroup ? self.ActiveContact.Id === data.UserMessage.ReceiverId : self.ActiveContact.Id === data.SenderId)) {
                    var message = {
                        senderId: data.SenderId,
                        receiverId: data.UserMessage.ReceiverId,
                        content: data.UserMessage.Content,
                        From: true,
                        SenderName: Chat.Contacts[data.SenderId].Name,
                        creationTime: new Date().toLocaleString()
                    };

                    self.eventAggregator.publish('UserMessageEvent', message);
                } else {
                    if (isGroup) {
                        ++self.Contacts[data.UserMessage.ReceiverId].NewMessagesCount;
                    } else {
                        ++self.Contacts[data.SenderId].NewMessagesCount;
                    }
                }

            });
        }

        this.BindWebSocketEvent('Talk', function(data) {
            switch(data.Talk.Type) {
                case 'RequestTalk' :
                    if (self.talk == null) {
                        var contact = null;

                        var isGroup = Chat.Contacts[data.Talk.ReceiverId].IsGroup;

                        if (isGroup) {
                            contact = Chat.Contacts[data.Talk.ReceiverId];
                        } else {
                            contact = Chat.Contacts[data.SenderId];
                        }

                        var persons = isGroup ? contact.Persons : [data.SenderId];

                        self.talk = new TalkClass(contact);
                        self.talk.Id = data.Talk.Id;

                        for (var i = 0; i < persons.length; ++i) {
                            if (persons[i] !== Chat.me.Id) {
                                self.talk.AddInterlocutor(Chat.Contacts[persons[i]]);
                            }
                        }

                        self.talk.GetInterlocutor(data.SenderId).Status = 'Accepted';
                        self.eventAggregator.publish('TalkRequestEvent', self.talk);
                    } else {
                        var data = {
                            MessageType: 'Talk',
                            Talk: {
                                Id: data.Talk.Id,
                                Type: 'RejectTalk'
                            }
                        };

                        self.SendToWsServer(data); 
                    }
                    break;

                case 'AcceptTalk' :
                    var interlocutor = self.talk.GetInterlocutor(data.SenderId);
                    interlocutor.Status = 'Accepted';
                    self.talk.Accepted = true;

                    if (interlocutor.User.Id < 0) {
                        interlocutor.User.Name = data.Talk.Name;
                    }

                    var acceptSize = Enumerable.From(self.talk.interlocutors)
                                        .Where((i) => i.Status === 'New' || i.Status === 'Accepted'  && i.User.Id !== self.me.Id).ToArray().length;

                    if (acceptSize === 1) {
                        self.talk.Class = 'col l12';
                    } else if (acceptSize === 2) {
                        self.talk.Class = 'col l6';
                    } else if (acceptSize === 3) {
                        self.talk.Class = 'col l4';
                    } else if (acceptSize === 4) {
                        self.talk.Class = 'col l3';
                    } else {
                        self.talk.Class = 'col l1';
                    }

                    self.GetRTCPeerConnection(self.myStream, interlocutor);
                    self.CreateOffer(interlocutor);
                    break;

                case 'RejectTalk' :
                    self.OnInterlocutorDisconnect(self.talk.GetInterlocutor(data.SenderId));
                    break;

                case 'Status' :
                    if (!self.GuestMode) {
                        for (var i = 0; i < self.talk.interlocutors.length; ++i) {
                            for (var j = 0; j < data.Talk.Status.length; ++j) {
                                if (self.talk.interlocutors[i].User.Id === data.Talk.Status[j]) {
                                    self.talk.interlocutors[i].Status = data.Talk.Status[j].Status;
                                    break;
                                }
                            }
                        }

                        var acceptSize = Enumerable.From(self.talk.interlocutors)
                                            .Where((i) => i.Status === 'New' || i.Status === 'Accepted'  && i.User.Id !== self.me.Id).ToArray().length;

                        if (acceptSize === 1) {
                            self.talk.Class = 'col l12';
                        } else if (acceptSize === 2) {
                            self.talk.Class = 'col l6';
                        } else if (acceptSize === 3) {
                            self.talk.Class = 'col l4';
                        } else if (acceptSize === 4) {
                            self.talk.Class = 'col l3';
                        } else {
                            self.talk.Class = 'col l1';
                        }
                    } 
                    break;
                    

                    case 'TalkInfoInterlocutors' :
                    {
                        for (var j = 0; j < data.Talk.TalkInfoInterlocutors.length; ++j) {
                            if (self.me.Id !== data.Talk.TalkInfoInterlocutors[j].Id) {
                                var interlocutor = {
                                    Status: data.Talk.TalkInfoInterlocutors[j].Status,
                                    User: {
                                        Id: data.Talk.TalkInfoInterlocutors[j].Id,
                                        Name: data.Talk.TalkInfoInterlocutors[j].Name
                                    },
                                    PeerConnection: null,
                                    Stream: null,
                                    VideoURL: null,
                                    PendingIceCandidates: [], 
                                    AllowedToPropagateIceCandidates: false,
                                    Accepted: false
                                };

                                self.talk.interlocutors.push(interlocutor);
                            }
                        }  

                        var acceptSize = Enumerable.From(self.talk.interlocutors)
                                .Where((i) => i.Status === 'New' || i.Status === 'Accepted'  && i.User.Id !== self.me.Id).ToArray().length;

                        if (acceptSize === 1) {
                            self.talk.Class = 'col l12';
                        } else if (acceptSize === 2) {
                            self.talk.Class = 'col l6';
                        } else if (acceptSize === 3) {
                            self.talk.Class = 'col l4';
                        } else if (acceptSize === 4) {
                            self.talk.Class = 'col l3';
                        } else {
                            self.talk.Class = 'col l1';
                        }

                    }

                case 'Stopped' :
                    self.OnInterlocutorDisconnect(self.talk.GetInterlocutor(data.SenderId));
                    break;

                case 'Error' :
                    self.eventAggregator.publish('GuestErrorEvent', self.talk);
                    break;
            }

        });

        this.BindWebSocketEvent('TalkHandshake', function(data) {
            var interlocutor = self.talk.GetInterlocutor(data.SenderId);

            switch(data.TalkHandshake.Type) {
                case 'Offer':
                    self.GetRTCPeerConnection(self.myStream, interlocutor);
                    self.CreateAnswer(interlocutor, JSON.parse(data.TalkHandshake.Offer));
                    break;

                case 'Answer':
                    self.PropagateIceCandidates(interlocutor);
                    self.HandshakeDone(interlocutor, JSON.parse(data.TalkHandshake.Answer));
                    break;

                case 'IceCandidate' :
                    if (!interlocutor.AllowedToPropagateIceCandidates) {
                        self.PropagateIceCandidates(interlocutor);
                    }

                    self.AddRemoteIceCandidate(interlocutor, JSON.parse(data.TalkHandshake.IceCandidate));
                    break;
            }
        });

    }

    AcceptTalk() {
        var self = this;

        if (self.myStream === null) {
            console.log('media got');

            window.navigator.getUserMedia(self.mediaConstraints, function(stream) {
                self.myStream = stream;

                var message = {
                    MessageType: 'Talk',
                    Talk: {
                        Id: self.talk.Id,
                        Type: 'AcceptTalk'
                    }
                };
        
                self.eventAggregator.publish('AcceptTalkEvent', null);
                self.eventAggregator.publish('SelectContactRequestEvent', self.talk.destination);
                self.eventAggregator.publish('ChangeActiveTabEvent', 'TalksTab');
                Chat.SendToWsServer(message);
            }, function(e) {
                console.error(e);
                return;
            });
        } else {
            var message = {
                MessageType: 'Talk',
                Talk: {
                    Id: self.talk.Id,
                    Type: 'AcceptTalk'
                }
            };
        
            self.eventAggregator.publish('AcceptTalkEvent', null);
            self.eventAggregator.publish('SelectContactRequestEvent', self.talk.destination);
            self.eventAggregator.publish('ChangeActiveTabEvent', 'TalksTab');
            Chat.SendToWsServer(message);
        }
        
        
    }

    RequestTalkAsGuest() {
        var self = this;

        window.navigator.getUserMedia(self.mediaConstraints, function(stream) {
            self.myStream = stream;

            self.talk = new TalkClass(null);
            self.talk.Id = parseInt(Session.GetString('roomId'));
            self.talk.Accepted = true;

            var data = {
                MessageType: 'Talk',
                Talk: {
                    Type: 'RequestTalk',
                    Id: self.talk.Id,
                    ReceiverId: 0, 
                    Name: self.me.Name
                },
                ReceiversIdentities: []
            };

            self.SendToWsServer(data);
            self.eventAggregator.publish('TalkCreatedEvent', self.talk);
        }, function(e) {
            console.error(e);
        });
    }

    ConnectToWebsocket() {
        var self = this;

        var wsProtocol = location.protocol === 'http:' ? 'ws://' : 'wss://';

//        var apiServerAddress = document.location.host + '/WebMeet.Api/';
        var apiServerAddress = document.location.host + ':6010/';

        console.log('connecting to websocket server');
        this.WSSConnection = new WebSocket(wsProtocol + apiServerAddress +  'api/WebSocket?Id=' + this.me.Id);

        this.WSSConnection.onopen = function() {
            console.log('connection success');

            self.CheckBrowserMediaAvailbility();
        };

        this.WSSConnection.onerror = function(e) {
            console.log('connection fail');
            console.log(e);
        };

        this.WSSConnection.onclose = function(e) {
            console.log('connection closed');
            console.log(e);
        };

        this.WSSConnection.onmessage = function(message) {
            var parsedData = JSON.parse(message.data);
            console.log(parsedData.MessageType + ' message received');
            self.EventListener.OnEvent(parsedData.MessageType, parsedData);
        };
    }

    CheckBrowserMediaAvailbility() {
        var self = this;
        var microphone = false;
        var webcam = false;

        window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
        window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;
        window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
        navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);

        if(window.RTCPeerConnection && navigator.getUserMedia && window.RTCSessionDescription && window.RTCIceCandidate) {
            if((navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) || window.MediaStreamTrack.getSources) {
                if(navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
                    navigator.mediaDevices.enumerateDevices().then(function(devices) {
                        devices.forEach(function(device) {
                            microphone = microphone || device.kind === 'audioinput';
                            webcam = webcam || device.kind === 'videoinput';
                        });

                        self.SendMediaAvailability(microphone, webcam);
                    }).catch(function(err) {
                        console.log(err);

                        self.SendMediaAvailability(false, false);
                    });
                } else {
                    window.MediaStreamTrack.getSources(function getDevices(devices) {
                        devices.forEach(function(device) {
                            microphone = microphone || device.kind === 'audio';
                            webcam = webcam || device.kind === 'video';
                        });

                        self.SendMediaAvailability(microphone, webcam);
                    });
                }
            } else {
                self.SendMediaAvailability(false, false);
            }
        } else {
            self.SendMediaAvailability(false, false);
        }
    }

    SendMediaAvailability(microphoneAvaibility, webcamAvaibility) {
        var browserMediaAvailbility = {
            MessageType: 'UsersMediaAvailbility',
            UsersMediaAvailbility: [{
                Id: 0, 
                WebcamAvaibility: webcamAvaibility, 
                MicrophoneAvaibility: microphoneAvaibility
            }]
        };

        this.SendToWsServer(browserMediaAvailbility); 
    }

    BindWebSocketEvent(eventName, fun) {
        this.EventListener.BindEvent(eventName, fun);
    }

    UnbindWebSocketEvent(eventName) {
        this.EventListener.UnbindEvent(eventName);
    }

    AddContact(contact) {
        if (this.Contacts === null) {
            this.Contacts = [];
        }

        this.Contacts[contact.Id] = contact;
    }

    SendUserMessage(personBaseId, content, onSuccess) {
        var self = this;

        MessagesService.CreateMessage(personBaseId, content).then(function(result) {
            if (result.success) {
                var receiversIdentities = self.Contacts[personBaseId].IsGroup ? self.Contacts[personBaseId].Persons : [personBaseId];

                var data = {
                    MessageType: 'UserMessage',
                    UserMessage: {
                        Content: content,
                        ReceiverId: personBaseId
                    },
                    ReceiversIdentities: receiversIdentities 
                };

                self.SendToWsServer(data);

                var message = {
                    senderId: self.me.Id,
                    receiverId: personBaseId, 
                    content: content,
                    From: false,
                    SenderName: self.Contacts[self.me.Id].Name,
                    creationTime: new Date(result.result.creationTime).toLocaleString()
                };

                onSuccess(message);
            }
        }, function(err) {
            console.error(err);
        });
     
    }

    CreateNewTalk() {
        var self = this;
        self.talk = new TalkClass(self.ActiveContact);
        self.talk.Accepted = true;

        window.navigator.getUserMedia(self.mediaConstraints, function(stream) {
            console.log('media got');

            self.myStream = stream;

            ConversationsService.CreateConversation(self.ActiveContact.Id)
                .then((result) => {
                    if (result.success) {
                        self.talk.Id = result.result.id;

                        console.log(self.talk.Id);

                        var persons = self.ActiveContact.IsGroup ? self.ActiveContact.Persons : [self.ActiveContact.Id];
                        for (var i = 0; i < persons.length; ++i) {
                            if (persons[i] !== Chat.me.Id) {
                                self.talk.AddInterlocutor(Chat.Contacts[persons[i]]);
                            }
                        }

                        var data = {
                            MessageType: 'Talk',
                            Talk: {
                                Type: 'RequestTalk',
                                Id: self.talk.Id,
                                ReceiverId: self.talk.destination.Id
                            },
                            ReceiversIdentities: Enumerable.From(self.talk.interlocutors).Select((i) => i.User.Id).ToArray()
                        };

                        self.eventAggregator.publish('TalkCreatedEvent', self.talk);
                        self.SendToWsServer(data);
                    } else {
                        self.talk = null;
                    }
                });
        }, function(e) {
            self.talk = null;
        });
    }

    StopTalkRequest() {
        if(this.talk != null) {
            var data = {
                MessageType: 'Talk',
                Talk: {
                    Type: 'Stopped',
                    Id: this.talk.Id,
                    ReceiverId: 0
                },
                ReceiversIdentities: []
            };

            this.SendToWsServer(data);
            this.StopTalk();
        }
    }

    StopTalk() {
        var self = this;

        for (var i = 0; i < this.talk.interlocutors.length; ++i) {
            var inte = this.talk.interlocutors[i];

            if (inte.PeerConnection !== null) {
                inte.PeerConnection.close();
                inte.PeerConnection = null;
            }
        }

        this.talk = null;

        if (self.myStream != null) {
            if (!self.myStream.stop) {
                self.myStream.getAudioTracks().forEach(function(track) {
                    track.stop();
                });

                self.myStream.getVideoTracks().forEach(function(track) {
                    track.stop();
                });
            } else {
                self.myStream.stop();
            }
        }
        self.eventAggregator.publish('TalkEndedEvent');
    }

    GetRTCPeerConnection(stream, interlocutor) {
        var self = this;

        console.log('creating new RTCPeerConnection');

        var peerConnection = new window.RTCPeerConnection(this.peerConnectionConfig);

        peerConnection.addStream(stream);

        peerConnection.onaddstream = function(e) {
            console.log('remote stream received');
            interlocutor.Stream = e.stream;
            interlocutor.VideoURL = URL.createObjectURL(interlocutor.Stream);
        };

        peerConnection.onicecandidate = function(event) {
            if(event) {
                console.log('Retrieving ICE data status changed : ' + event.target.iceGatheringState);

                if(event.candidate) {
                    if (interlocutor.AllowedToPropagateIceCandidates) {
                        var data = {
                            MessageType: 'TalkHandshake',
                            TalkHandshake: {
                                Type: 'IceCandidate',
                                IceCandidate: JSON.stringify(event.candidate)
                            },
                            ReceiversIdentities: [interlocutor.User.Id]
                        };

                        self.SendToWsServer(data);
                    } else {
                        interlocutor.PendingIceCandidates.push(event.candidate);
                    }
                }
            }
        };

        peerConnection.oniceconnectionstatechange = function(event) {
            console.log('ICE connection status changed : ' + event.target.iceConnectionState);

            if (event.target.iceConnectionState === 'disconnected') {
                self.OnInterlocutorDisconnect(interlocutor);
            }
        };

        interlocutor.PeerConnection = peerConnection;
    }

    OnInterlocutorDisconnect(interlocutor) {
        if (this.talk != null && interlocutor != null && (interlocutor.Status === 'New' || interlocutor.Status === 'Accepted')) {

            var remoteInterlocutorsCount = Enumerable.From(this.talk.interlocutors)
                                        .Where((i) => i.Status !== 'Rejected' && i.Status !== 'Stopped'  && i.User.Id !== this.me.Id).ToArray().length;

            if (remoteInterlocutorsCount === 2) {
                this.talk.Class = 'col l12';
            } else if (remoteInterlocutorsCount === 3) {
                this.talk.Class = 'col l6';
            } else if (remoteInterlocutorsCount === 4) {
                this.talk.Class = 'col l4';
            }

            if (remoteInterlocutorsCount === 1 ) {
                if (this.GuestMode) {
                    Toastr.info(this.i18n.tr('Calendar.Talks.Talk.TalkEnded'));
                } else {
                    if (this.talk.destination.IsGroup) {
                        Toastr.info(this.i18n.tr('Calendar.Talks.Talk.GroupTalkEnded').replace('__groupname__', this.talk.destination.Name), 
                            this.i18n.tr('Calendar.Talks.Talk.TalkEnded'));
                    } else {
                        if (interlocutor.Status === 'New') {
                            Toastr.info(this.i18n.tr('Calendar.Talks.Talk.TalkRejectedByUser').replace('__username__', interlocutor.User.Name));
                        } else {
                            Toastr.info(this.i18n.tr('Calendar.Talks.Talk.UserTalkEnded').replace('__username__', this.talk.destination.Name), 
                            this.i18n.tr('Calendar.Talks.Talk.TalkEnded'));
                        }
                    }
                }

                this.StopTalk();
            } else {
                if (interlocutor.Status === 'New') {
                    Toastr.info(this.i18n.tr('Calendar.Talks.Talk.TalkRejectedByUser').replace('__username__', interlocutor.User.Name));
                } else {
                    Toastr.info(this.i18n.tr('Calendar.Talks.Talk.TalkEndedByUser').replace('__username__', interlocutor.User.Name));
                }
            }

            interlocutor.Status = 'Stopped';

            if (remoteInterlocutorsCount === 0) {
                this.StopTalk();
            }
        }
    }

    CreateOffer(interlocutor) {
        var self = this;

        var peerConnection = interlocutor.PeerConnection;

        peerConnection.createOffer(function(description) {
            console.log('Offer created');

            peerConnection.setLocalDescription(description, function() {
                console.log('Local description set');

                var data = {
                    MessageType: 'TalkHandshake',
                    TalkHandshake: {
                        Type: 'Offer',
                        Offer: JSON.stringify(description)
                    },
                    ReceiversIdentities: [interlocutor.User.Id]
                };

                self.SendToWsServer(data);
            }, function(e) { console.error(e); });
        }, function(e) { console.error(e); });
    }

    CreateAnswer(interlocutor, description) {
        var self = this;

        var peerConnection = interlocutor.PeerConnection;

        peerConnection.setRemoteDescription(new RTCSessionDescription(description), function() {
            console.log('Remote description set');

            peerConnection.createAnswer(function(localDescription) {
                console.log('Answer created');

                peerConnection.setLocalDescription(localDescription, function() {
                    console.log('Local description set');

                    var data = {
                        MessageType: 'TalkHandshake',
                        TalkHandshake: {
                            Type: 'Answer',
                            Answer: JSON.stringify(localDescription)
                        },
                        ReceiversIdentities: [interlocutor.User.Id]
                    };

                    self.SendToWsServer(data);
                }, function(e) { console.error(e); });
            }, function(e) { console.error(e); });
        }, function(e) { console.error(e); });
    }

    HandshakeDone(interlocutor, description) {
        interlocutor.PeerConnection.setRemoteDescription(new window.RTCSessionDescription(description), function() {
            //TODO
        }, function(e) { console.error(e); });
    }

    AddRemoteIceCandidate(interlocutor, candidate) {
        interlocutor.PeerConnection.addIceCandidate(new window.RTCIceCandidate(candidate));
    }

    PropagateIceCandidates(interlocutor) {
        interlocutor.AllowedToPropagateIceCandidates = true;

        var data = null;

        for (var i = 0; i < interlocutor.PendingIceCandidates.length; ++i) {
            data = {
                MessageType: 'TalkHandshake',
                TalkHandshake: {
                    Type: 'IceCandidate',
                    IceCandidate: JSON.stringify(interlocutor.PendingIceCandidates[i])
                },
                ReceiversIdentities: [interlocutor.User.Id]
            };

            this.SendToWsServer(data);
        }
        interlocutor.PendingIceCandidates = [];
    }

    SendToWsServer(data) {
        if (data !== null && data !== undefined) {
            try {
                data.SenderId = this.me.Id;
                this.WSSConnection.send(JSON.stringify(data));
            } catch (e) {
                console.error(e);
            }
        }
    }

}

export class EventListenerClass {

    constructor() {
        this.events = [];
    }

    BindEvent(eventName, fun) {
        for (var i = 0; i < this.events.length; ++i) {
            if (this.events[i].eventName === eventName) {
                return;
            }
        }

        this.events.push(this.CreateEvent(eventName, fun));
    }

    UnbindEvent(eventName) {
        for(var i = this.events.length - 1; i >= 0; i--) {
            if(this.events.eventName === eventName) {
                this.events.splice(i, 1);
            }
        }
    }

    OnEvent(eventName, data) {
        this.events.forEach(function(event) {
            if(event.eventName === eventName) {
                event.fun(data);
            }
        });
    }

    CreateEvent(eventName, fun) {
        return {
            eventName: eventName,
            fun: fun
        };
    }
}

export class TalkClass {
    constructor(destination) {
        this.localStream = null;
        this.interlocutors = [];
        this.destination = destination;
        this.Id = null;
        this.Class = '';
    }

    AddInterlocutor(contact) {
        var interlocutor = {
            Status: 'New',
            User: contact,
            PeerConnection: null,
            Stream: null,
            VideoURL: null,
            PendingIceCandidates: [], 
            AllowedToPropagateIceCandidates: false,
            Accepted: false
        };
        this.interlocutors.push(interlocutor);
    }

    GetInterlocutor(id) {
        for (var i = 0; i < this.interlocutors.length; ++i) {
            if (this.interlocutors[i].User.Id === id) {
                return this.interlocutors[i];
            }
        }

        if (id < 0) {
            var interlocutor = {
                Status: 'New',
                User: {
                    Id: id,
                    Name: 'Guest'
                },
                PeerConnection: null,
                Stream: null,
                VideoURL: null,
                PendingIceCandidates: [], 
                AllowedToPropagateIceCandidates: false,
                Accepted: false
            };

            this.interlocutors.push(interlocutor);
            return interlocutor;
        }

        return null;
    }
}

export var Chat = new ChatClass();
