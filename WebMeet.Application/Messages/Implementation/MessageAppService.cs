﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using AutoMapper;
using WebMeet.Core.Services.Messages;
using WebMeet.Extensions;
using WebMeet.Groups;
using WebMeet.Messages.Dto;
using WebMeet.Persons;
using WebMeet.Shared;

namespace WebMeet.Messages.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na wiadomościach
    /// </summary>
    [AbpAuthorize]
    public class MessageAppService : ApplicationBaseService, IMessageAppService
    {
        protected readonly IMessageManager MessageManager;
        protected readonly IRepository<Message, long> MessageRepository;
        protected readonly IRepository<Group, long> GroupRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="personRepository">Repozytorium osób</param>
        /// <param name="messageManager">Serwis domenowy wiadomości</param>
        /// <param name="messageRepository">Repozytorium wiadomości</param>
        /// <param name="groupRepository">Repozytorium grup</param>
        public MessageAppService(IRepository<Person, long> personRepository, 
            IMessageManager messageManager, 
            IRepository<Message, long> messageRepository, 
            IRepository<Group, long> groupRepository) : base(personRepository)
        {
            MessageManager = messageManager;
            MessageRepository = messageRepository;
            GroupRepository = groupRepository;
        }

        /// <summary>
        /// Utworzenie wiadomości
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworozna wiadomość</returns>
        public async Task<CreateMessageOutput> Create(CreateMessageInput model)
        {
            var message = new Message
            {           
                Content = model.Content,
                ReceiverId = model.ReceiverId,
                SenderId = PersonId,
            };
            message =await MessageManager.Create(PersonId, message);
            return message.MapTo<CreateMessageOutput>();
        }

        /// <summary>
        /// Pobranie wiadomości zgodnie z parametrami
        /// </summary>
        /// <param name="query">Model</param>
        public async Task<GetMessagesOutput> Get(GetMessagesInput query)
        {
            var group = await GetGroup(query.ReceiverId);
            var queryable = MessageRepository.GetAll();
            if (group == null)
                queryable = queryable.Where(m => (m.SenderId == PersonId && m.ReceiverId == query.ReceiverId) ||
                                                 (m.SenderId == query.ReceiverId && m.ReceiverId == PersonId));
            else
                queryable = queryable.Where(m => m.ReceiverId == group.Id);
            var count = queryable.Count();

            var items = queryable
               .FilterPage(query)
               .ToList()
               .Select(Mapper.Map<MessageDto>);

            return new GetMessagesOutput()
            {
                TotalCount = count,
                Items = items.ToList()
            };
        }

        private async Task<Group> GetGroup(long groupId)
        {
            return await GroupRepository.FirstOrDefaultAsync(g => g.Id == groupId);
        }
    }
}
