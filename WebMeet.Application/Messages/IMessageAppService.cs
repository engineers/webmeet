﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Messages.Dto;

namespace WebMeet.Messages
{
    /// <summary>
    /// Interfejs definiujący operacje na wiadomościach
    /// </summary>
    public interface IMessageAppService : IApplicationService
    {
        /// <summary>
        /// Utworzenie wiadomości
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworozna wiadomość</returns>
        Task<CreateMessageOutput> Create(CreateMessageInput model);
        /// <summary>
        /// Pobranie wiadomości zgodnie z parametrami
        /// </summary>
        /// <param name="query">Model</param>
        Task<GetMessagesOutput> Get(GetMessagesInput query);
    }
}
