﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Messages.Dto
{
    /// <summary>
    /// Klasa reprezentująca DTO dla typu <see cref="Message"/>
    /// </summary>
    public class MessageDto : EntityDto<long>
    {
        /// <summary>
        /// Zawartość wiadomości
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Id odbiorcy
        /// </summary>
        public long ReceiverId { get; set; }
        /// <summary>
        /// Id nadawcy
        /// </summary>
        public long SenderId { get; set; }
        /// <summary>
        /// Czas wysłania
        /// </summary>
        public DateTime CreationTime { get; set; }
    }
}
