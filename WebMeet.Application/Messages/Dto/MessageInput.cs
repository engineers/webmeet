﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Shared;

namespace WebMeet.Messages.Dto
{
    /// <summary>
    /// Dane dla operacji wysłania wiadomości
    /// </summary>
    public class CreateMessageInput : IInputDto
    {
        /// <summary>
        /// Max długość zawartości
        /// </summary>
        public const int MaxContentLength = 300;

        /// <summary>
        /// Zawartość wiadomości
        /// </summary>
        [Required]
        [StringLength(MaxContentLength)]
        public string Content { get; set; }

        /// <summary>
        /// Id odbiorcy
        /// </summary>
        [Required]
        public long ReceiverId { get; set; }
    }

    /// <summary>
    /// Dane dla operacji pobrania własnych wiadomosci
    /// </summary>
    public class GetMessagesInput : SearchInputBase
    {
        /// <summary>
        /// Id odbiorcy
        /// </summary>
        [Required]
        public long ReceiverId { get; set; }
    }
}
