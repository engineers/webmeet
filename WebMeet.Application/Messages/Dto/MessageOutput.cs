﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Messages.Dto
{
    /// <summary>
    /// Wynik operacji wysłania wiadomości
    /// </summary>
    public class CreateMessageOutput :MessageDto, IOutputDto
    {
    }

    /// <summary>
    /// Wynik operacji pobrania wiadomości
    /// </summary>
    public class GetMessagesOutput : PagedResultOutput<MessageDto> { }
}
