﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using AutoMapper;
using WebMeet.Calendar.Dto;
using WebMeet.Core.Services.Calendar;
using WebMeet.Extensions;
using WebMeet.Groups;
using WebMeet.Helpers.Extensions;
using WebMeet.Persons;
using WebMeet.Shared;

namespace WebMeet.Calendar.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na wydarzeniach
    /// </summary>
    [AbpAuthorize]
    public class EventAppService : ApplicationBaseService, IEventAppService
    {
        protected readonly IRepository<Event, long> EventRepository;
        protected readonly IEventManager EventManager;
        protected readonly IRepository<GroupUser, long> GroupUserRepository;

        /// <summary>
        /// Konstrukotr
        /// </summary>
        /// <param name="personRepository">Repozytorium osób</param>
        /// <param name="eventRepository">Repozytorium wydarzeń</param>
        /// <param name="eventManager">Srwis domenowy wydarzeń</param>
        /// <param name="groupUserRepository">Repozytorium osób w grupach</param>
        public EventAppService(IRepository<Person, long> personRepository, 
            IRepository<Event, long> eventRepository, IEventManager eventManager, 
            IRepository<GroupUser, long> groupUserRepository) : base(personRepository)
        {
            EventRepository = eventRepository;
            EventManager = eventManager;
            GroupUserRepository = groupUserRepository;
        }

        /// <summary>
        /// Pobiera własne wydarzenia z podanego przedziału czasowego
        /// </summary>
        /// <param name="query">Model</param>
        public GetEventsOutput GetForMe(GetEventsInput query)
        {
            var queryable = EventRepository
                .GetAll()
                .FilterIfNotNull(query.StartDateTime, @event => @event.Date >= query.StartDateTime)
                .FilterIfNotNull(query.EndDateTime, @event => @event.Date <= query.EndDateTime)
                .Where(e => e.PersonBaseId == PersonId);
            var count = queryable.Count();
            var items = queryable
               .FilterPage(query)
               .ToList()
               .Select(Mapper.Map<EventDto>);

            return new GetEventsOutput
            {
                TotalCount = count,
                Items = items.ToList()
            };
        }

        /// <summary>
        /// Pobiera wydarzenia dla grupy z przedziału czasowego
        /// </summary>
        /// <param name="query">Model</param>
        public async Task<GetEventsOutput> GetForGroup(GetGroupEventsInput query)
        {
            await CheckIfPersonBelongToGroup(PersonId, query.GroupId);
            var queryable = EventRepository
               .GetAll()
               .Where(@event => @event.PersonBaseId == query.GroupId)
               .FilterIfNotNull(query.StartDateTime, @event => @event.Date >= query.StartDateTime)
               .FilterIfNotNull(query.EndDateTime, @event => @event.Date <= query.EndDateTime);
            var count = queryable.Count();
            var items = queryable
               .FilterPage(query)
               .ToList()
               .Select(Mapper.Map<EventDto>);

            return new GetEventsOutput
            {
                TotalCount = count,
                Items = items.ToList()
            };
        }

        /// <summary>
        /// Tworzy własne wydarzenie
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworzone wydarzenie</returns>
        public async Task<CreateEventOutput> CreateForMe(CreateSelfEventInput model)
        {
            var createdEvent = await EventManager.CreateForPerson(PersonId, new Event
            {
                PersonBaseId = PersonId,
                Date = model.Date,
                Description = model.Description,
                Title = model.Title
            });
            return Mapper.Map<CreateEventOutput>(createdEvent);
        }

        /// <summary>
        /// Tworzy nowe wydarzenie dla grupy
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworzone wydarzenie</returns>
        public async Task<CreateEventOutput> CreateForGroup(CreateGroupEventInput model)
        {
            var createdEvent = await EventManager.CreateForGroup(PersonId, new Event
            {
                PersonBaseId = model.GroupId,
                Date = model.Date,
                Description = model.Description,
                Title = model.Title
            });
            return Mapper.Map<CreateEventOutput>(createdEvent);
        }

        /// <summary>
        /// Aktualizuje własne wydarzenie
        /// </summary>
        /// <param name="id">Id wydarzenia</param>
        /// <param name="model">Model</param>
        /// <returns>Zaktualizowane wydarzenie</returns>
        public async Task<UpdateEventOutput> UpdateForMe(long id, UpdateEventOutput model)
        {
            var @event = await EventManager.GetAsync(id);
            @event.Date = model.Date;
            @event.Description = model.Description;
            @event.Title = model.Title;
            var updatedEvent = await EventManager.UpdateForPerson(PersonId, @event);
            return Mapper.Map<UpdateEventOutput>(updatedEvent);
        }

        /// <summary>
        /// Aktualziuje wydarenie grupy
        /// </summary>
        /// <param name="id">Id wydarzenia</param>
        /// <param name="model">Model</param>
        /// <returns>Zaktualizowane wydarzenie</returns>
        public async Task<UpdateEventOutput> UpdateForGroup(long id, UpdateEventOutput model)
        {
            var @event = await EventManager.GetAsync(id);
            @event.Date = model.Date;
            @event.Description = model.Description;
            @event.Title = model.Title;
            var updatedEvent = await EventManager.UpdateForGroup(PersonId, @event);
            return Mapper.Map<UpdateEventOutput>(updatedEvent);
        }

        /// <summary>
        /// Usunięcie własnego wydarzenia
        /// </summary>
        /// <param name="id">Id wydarzenia</param>
        public async Task DeleteForMe(long id)
        {
            var @event = await EventManager.GetAsync(id);
            await EventManager.DeleteForPerson(PersonId, @event);
        }
        /// <summary>
        /// Usunięcie wydarzenia grupy
        /// </summary>
        /// <param name="id">Id wydarzenia</param>
        public async Task DeleteForGroup(long id)
        {
            var @event = await EventManager.GetAsync(id);
            await EventManager.DeleteForGroup(PersonId, @event);
        }

        private async Task CheckIfPersonBelongToGroup(long personId, long groupId)
        {
            if ((await GroupUserRepository.FirstOrDefaultAsync(gu => gu.PersonId == personId && gu.GroupId == groupId)) == null)
                throw new UserFriendlyException(L("YouDontBelongToGroup"));
        }
    }
}
