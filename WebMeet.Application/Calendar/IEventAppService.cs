﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Calendar.Dto;

namespace WebMeet.Calendar
{
    /// <summary>
    /// Interfejs definiujący operacje na wydarzeniach
    /// </summary>
    public interface IEventAppService : IApplicationService
    {
        /// <summary>
        /// Pobiera własne wydarzenia z podanego przedziału czasowego
        /// </summary>
        /// <param name="query">Model</param>
        GetEventsOutput GetForMe(GetEventsInput query);
        /// <summary>
        /// Pobiera wydarzenia dla grupy z przedziału czasowego
        /// </summary>
        /// <param name="query">Model</param>
        Task<GetEventsOutput> GetForGroup(GetGroupEventsInput query);
        /// <summary>
        /// Tworzy własne wydarzenie
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworzone wydarzenie</returns>
        Task<CreateEventOutput> CreateForMe(CreateSelfEventInput model);
        /// <summary>
        /// Aktualizuje własne wydarzenie
        /// </summary>
        /// <param name="id">Id wydarzenia</param>
        /// <param name="model">Model</param>
        /// <returns>Zaktualizowane wydarzenie</returns>
        Task<CreateEventOutput> CreateForGroup(CreateGroupEventInput model);
        /// <summary>
        /// Aktualizuje własne wydarzenie
        /// </summary>
        /// <param name="id">Id wydarzenia</param>
        /// <param name="model">Model</param>
        /// <returns>Zaktualizowane wydarzenie</returns>
        Task<UpdateEventOutput> UpdateForMe(long id, UpdateEventOutput model);
        /// <summary>
        /// Aktualziuje wydarenie grupy
        /// </summary>
        /// <param name="id">Id wydarzenia</param>
        /// <param name="model">Model</param>
        /// <returns>Zaktualizowane wydarzenie</returns>
        Task<UpdateEventOutput> UpdateForGroup(long id, UpdateEventOutput model);
        /// <summary>
        /// Usunięcie własnego wydarzenia
        /// </summary>
        /// <param name="id">Id wydarzenia</param>
        Task DeleteForMe(long id);
        /// <summary>
        /// Usunięcie wydarzenia grupy
        /// </summary>
        /// <param name="id">Id wydarzenia</param>
        Task DeleteForGroup(long id);
    }
}
