﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using WebMeet.Friends.Invitations;
using WebMeet.Helpers.Extensions;
using WebMeet.Shared;

namespace WebMeet.Calendar.Dto
{
    /// <summary>
    /// Klasa zawierająca informacje do pobrania wydarzeń
    /// </summary>
    public class GetEventsInput : SearchInputBase, ICustomValidate, IShouldNormalize
    {
        /// <summary>
        /// Startowa data wydarzeń
        /// </summary>
        public DateTime? StartDateTime { get; set; }
        /// <summary>
        /// Końcowa data wydarzeń
        /// </summary>
        public DateTime? EndDateTime { get; set; }

        /// <summary>
        /// Dodaje błędy walidacji jęsli startowa data > końcowej daty"/>
        /// </summary>
        public void AddValidationErrors(List<System.ComponentModel.DataAnnotations.ValidationResult> results)
        {
            if(StartDateTime.HasValue && EndDateTime.HasValue && EndDateTime < StartDateTime)
                results.Add(new ValidationResult("Startowa data musi być mniejsza od końcowej"));
        }

        /// <summary>
        /// Normalizuje dane obiektu, jesli start i end nie zostały podane to ustawiane są na początek oraz koniec bieżącego miesiąca
        /// </summary>
        public void Normalize()
        {
            if (StartDateTime.HasValue && !EndDateTime.HasValue)
                EndDateTime = StartDateTime.Value.GetMonthEnd();
            else if (EndDateTime.HasValue && !StartDateTime.HasValue)
                StartDateTime = EndDateTime.Value.GetMonthStart();
            else if (!StartDateTime.HasValue && !EndDateTime.HasValue)
            {
                StartDateTime = DateTime.Now.GetMonthStart();
                EndDateTime = DateTime.Now.GetMonthEnd();
            }
        }
    }

    /// <summary>
    /// Klasa zawierająca dane do pobrania wydarzeń grupy
    /// </summary>
    public class GetGroupEventsInput : GetEventsInput
    {
        /// <summary>
        /// Id grupy
        /// </summary>
        [Required]
        public long GroupId { get; set; }
    }

    /// <summary>
    /// Klasa bazowa zawierająca dane do utworzenia wydarzenia
    /// </summary>
    public abstract class CreateEventInput : UpdateEventInput { }

    /// <summary>
    /// Klasa zawierająca dane do utworzenie własnego wydarzenia
    /// </summary>
    public class CreateSelfEventInput : CreateEventInput { }

    /// <summary>
    /// Klasa zawierająca dane do utworzenia wydarzenia dla grupy
    /// </summary>
    public class CreateGroupEventInput : CreateEventInput
    {
        [Required]
        public virtual long GroupId { get; set; }
    }

    /// <summary>
    /// Klasa zawierająca dane do aktualizacji wydarzenia
    /// </summary>
    public class UpdateEventInput : IInputDto
    {
        /// <summary>
        /// Max długość opisu
        /// </summary>
        public const int MaxDescriptionLength = 500;
        /// <summary>
        /// Max długość tytułu
        /// </summary>
        public const int MaxTitleLength = 100;

        /// <summary>
        /// Data wydarzenia
        /// </summary>
        [Required]
        public virtual DateTime Date { get; set; }
        /// <summary>
        /// Opis wydarzenia
        /// </summary>
        [Required]
        [StringLength(MaxDescriptionLength)]
        public virtual string Description { get; set; }
        /// <summary>
        /// Tytuł wydarzenia
        /// </summary>
        [Required]
        [StringLength(MaxTitleLength)]
        public virtual string Title { get; set; }
    }
}
