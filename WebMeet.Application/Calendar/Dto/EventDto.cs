﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Calendar.Dto
{
    /// <summary>
    /// Klasa reprezentująca DTO dla typu <see cref="Event"/>
    /// </summary>
    public class EventDto : EntityDto<long>
    {
        /// <summary>
        /// Id osoby lug grupy
        /// </summary>
        public virtual long PersonBaseId { get; set; }
        /// <summary>
        /// Data wydarzenia
        /// </summary>
        public virtual DateTime Date { get; set; }
        /// <summary>
        /// Opis wydarzenia
        /// </summary>
        public virtual string Description { get; set; }
        /// <summary>
        /// Tytuł wydarzenia
        /// </summary>
        public virtual string Title { get; set; }
    }
}
