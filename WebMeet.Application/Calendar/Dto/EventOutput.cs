﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Calendar.Dto
{
    /// <summary>
    /// Klasa zawierająca utworzone wydarzenie
    /// </summary>
    public class CreateEventOutput : UpdateEventOutput { }

    /// <summary>
    /// Klasa zawierające zaktualizowane wydarzneie
    /// </summary>
    public class UpdateEventOutput : EventDto, IOutputDto { }

    /// <summary>
    /// Klasa zawierająca wynik operacji wyszukiwania
    /// </summary>
    public class GetEventsOutput : PagedResultOutput<EventDto>
    {
    }
}
