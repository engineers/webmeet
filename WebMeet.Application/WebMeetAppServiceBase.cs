﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using WebMeet.Core.Services;
using WebMeet.Core.Services.MultiTenancy;
using WebMeet.Core.Services.Users;
using WebMeet.Core.Services.Users.Abp;
using WebMeet.MultiTenancy;
using WebMeet.Users;
using Microsoft.AspNet.Identity;

namespace WebMeet
{
    /// <summary>
    /// Klasa  bazowa dla serwisów aplikacji
    /// </summary>
    public abstract class WebMeetAppServiceBase : ApplicationService
    {
        /// <summary>
        /// TenantManager
        /// </summary>
        public TenantManager TenantManager { get; set; }

        /// <summary>
        /// UserManager
        /// </summary>
        public AbpUserManager UserManager { get; set; }

        protected WebMeetAppServiceBase()
        {
            LocalizationSourceName = WebMeetConsts.LocalizationSourceName;
        }

        protected virtual Task<User> GetCurrentUserAsync()
        {
            var user = UserManager.FindByIdAsync(AbpSession.GetUserId());
            if (user == null)
            {
                throw new ApplicationException("There is no current user!");
            }

            return user;
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}