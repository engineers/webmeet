﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Roles.Dto;

namespace WebMeet.Roles
{
    public interface IRoleAppService : IApplicationService
    {
        /// <summary>
        /// Aktualizacja uprawnień roli
        /// </summary>
        /// <param name="input">Model</param>
        Task UpdateRolePermissions(UpdateRolePermissionsInput input);
    }
}
