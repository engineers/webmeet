﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace WebMeet.Roles.Dto
{
    /// <summary>
    /// Dane dla operacji aktualizacji uprawnień roli
    /// </summary>
    public class UpdateRolePermissionsInput : IInputDto
    {
        /// <summary>
        /// Id roli
        /// </summary>
        [Range(1, int.MaxValue)]
        public int RoleId { get; set; }

        /// <summary>
        /// Lista uprawnień
        /// </summary>
        [Required]
        public List<string> GrantedPermissionNames { get; set; }
    }
}