﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace WebMeet.Users.Dto
{
    /// <summary>
    /// Klasa zawierająca dane dal operacji usunięcia uprawnienia użytkownikowi
    /// </summary>
    public class ProhibitPermissionInput : IInputDto
    {
        /// <summary>
        /// Id użytkownika
        /// </summary>
        [Range(1, long.MaxValue)]
        public int UserId { get; set; }

        /// <summary>
        /// Nazwa uprawnienia
        /// </summary>
        [Required]
        public string PermissionName { get; set; }
    }
}