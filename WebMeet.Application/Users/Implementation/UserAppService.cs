﻿using System.Threading.Tasks;
using Abp.Authorization;
using WebMeet.Core.Services.Users;
using WebMeet.Core.Services.Users.Abp;
using WebMeet.Users.Dto;

namespace WebMeet.Users.Implementation
{
    public class UserAppService : WebMeetAppServiceBase, IUserAppService
    {
        private readonly AbpUserManager _userManager;
        private readonly IPermissionManager _permissionManager;

        public UserAppService(AbpUserManager userManager, IPermissionManager permissionManager)
        {
            _userManager = userManager;
            _permissionManager = permissionManager;
        }

        /// <summary>
        /// Usunięcie uprawnienia użytkownikowi
        /// </summary>
        /// <param name="input">Model</param>
        public async Task ProhibitPermission(ProhibitPermissionInput input)
        {
            var user = await _userManager.GetUserByIdAsync(input.UserId);
            var permission = _permissionManager.GetPermission(input.PermissionName);

            await _userManager.ProhibitPermissionAsync(user, permission);
        }

        /// <summary>
        /// Usunięcie użytkownika z roli
        /// </summary>
        /// <param name="userId">Id użytkowika</param>
        /// <param name="roleName">Nazwa roli</param>
        public async Task RemoveFromRole(long userId, string roleName)
        {
            CheckErrors(await _userManager.RemoveFromRoleAsync(userId, roleName));
        }
    }
}