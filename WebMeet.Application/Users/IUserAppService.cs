﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Users.Dto;

namespace WebMeet.Users
{
    public interface IUserAppService : IApplicationService
    {
        /// <summary>
        /// Usunięcie uprawnienia użytkownikowi
        /// </summary>
        /// <param name="input">Model</param>
        Task ProhibitPermission(ProhibitPermissionInput input);

        /// <summary>
        /// Usunięcie użytkownika z roli
        /// </summary>
        /// <param name="userId">Id użytkowika</param>
        /// <param name="roleName">Nazwa roli</param>
        Task RemoveFromRole(long userId, string roleName);
    }
}