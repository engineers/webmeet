﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Conversations.Dto;

namespace WebMeet.Conversations
{
    /// <summary>
    /// Interfejs definiujący operacje na rozmowach
    /// </summary>
    public interface IConversationAppService : IApplicationService
    {
        /// <summary>
        /// Pobiera historię rozmów grupy/uzytkownika
        /// </summary>
        /// <param name="query">Model</param>
        Task<GetConversationsOutput> Get(GetConversationsInput query);
        /// <summary>
        /// Utworzenie rozmowy
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworzony model</returns>
        Task<CreateConversationOutput> Create(CreateConversationInput model);
        /// <summary>
        /// Zakonczenie rozmowy
        /// </summary>
        /// <param name="conversationId">Id rozmowy</param>
        /// <returns>Zaktualizowany model</returns>
        Task<UpdateConversationOutput> End(long conversationId);
    }
}
