﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Conversations.Dto
{
    /// <summary>
    /// Klasa zawierająca utworzoną rozmowę
    /// </summary>
    public class CreateConversationOutput : UpdateConversationOutput { }

    /// <summary>
    /// Klasa zawierająca zaktualizowaną rozmowę
    /// </summary>
    public class UpdateConversationOutput : ConversationDto, IOutputDto { }

    /// <summary>
    /// Klasa zawierająca wynik operacji wyszukiwania
    /// </summary>
    public class GetConversationsOutput : PagedResultOutput<ConversationDto>
    {
    }
}
