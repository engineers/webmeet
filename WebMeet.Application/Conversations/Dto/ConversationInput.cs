﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Shared;

namespace WebMeet.Conversations.Dto
{
    /// <summary>
    /// Klasa zawieraająca dane do utworzenia rozmowy
    /// </summary>
    public class CreateConversationInput : IInputDto
    {
        /// <summary>
        /// Id osoby/grupy do ktorej dzwonimy
        /// </summary>
        [Required]
        public long PersonBaseId { get; set; }
    }

    /// <summary>
    /// Klasa zawierająca dane do pobrania rozmów
    /// </summary>
    public class GetConversationsInput : SearchInputBase
    {
        /// <summary>
        /// Id osoby/grupy dla której pobrać przeprowadzone rozmowy
        /// </summary>
        [Required]
        public long PersonBaseId { get; set; }
    }
}
