﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Conversations.Dto
{
    /// <summary>
    /// Klasa reprezentująca DTO dla typu <see cref="Conversation"/>
    /// </summary>
    public class ConversationDto : EntityDto<long>
    {
        /// <summary>
        /// Id osoby dzwoniącej
        /// </summary>
        public long CallerId { get; set; }
        /// <summary>
        /// Nazwa osoby dzwoniącej
        /// </summary>
        public string CallerName { get; set; }
        /// <summary>
        /// Id osoby/grupy do ktorej dzwonimy
        /// </summary>
        public long PersonBaseId { get; set; }
        /// <summary>
        /// Czas rozpoczęcia rozmowy
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Czas zakończenia rozmowy
        /// </summary>
        public DateTime? EndDate { get; set; }
    }
}
