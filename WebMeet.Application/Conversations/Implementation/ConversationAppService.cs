﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using AutoMapper;
using WebMeet.Conversations.Dto;
using WebMeet.Core.Services.Conversations;
using WebMeet.Extensions;
using WebMeet.Groups;
using WebMeet.Persons;
using WebMeet.Shared;

namespace WebMeet.Conversations.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na rozmowach
    /// </summary>
    [AbpAuthorize]
    public class ConversationAppService :ApplicationBaseService, IConversationAppService
    {
        protected readonly IConversationManager ConversationManager;
        protected readonly IRepository<Conversation, long> ConversationRepository;
        protected readonly IRepository<Group, long> GroupRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="personRepository">Repozytorium osób</param>
        /// <param name="conversationManager">Serwis domenowy rozmów</param>
        /// <param name="conversationRepository">Repozytorium rozmów</param>
        /// <param name="groupRepository">Repozytorium grup</param>
        public ConversationAppService(IRepository<Person, long> personRepository, 
            IConversationManager conversationManager, 
            IRepository<Conversation, long> conversationRepository, 
            IRepository<Group, long> groupRepository) : base(personRepository)
        {
            ConversationManager = conversationManager;
            ConversationRepository = conversationRepository;
            GroupRepository = groupRepository;
        }

        /// <summary>
        /// Pobiera historię rozmów grupy/uzytkownika
        /// </summary>
        /// <param name="query">Model</param>
        public async Task<Dto.GetConversationsOutput> Get(Dto.GetConversationsInput query)
        {
            var group = await GetGroup(query.PersonBaseId);
            var queryable = ConversationRepository.GetAll();
            if (group == null)
                queryable = queryable.Where(c => (c.CallerId == PersonId && c.PersonBaseId == query.PersonBaseId) ||
                                                 (c.CallerId == query.PersonBaseId && c.PersonBaseId == PersonId));
            else
                queryable = queryable.Where(c => c.PersonBaseId == query.PersonBaseId);
            var count = queryable.Count();

            var items = queryable
               .FilterPage(query)
               .ToList()
               .Select(Mapper.Map<ConversationDto>);

            return new GetConversationsOutput()
            {
                TotalCount = count,
                Items = items.ToList()
            };
        }

        /// <summary>
        /// Utworzenie rozmowy
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworzony model</returns>
        public async Task<Dto.CreateConversationOutput> Create(Dto.CreateConversationInput model)
        {
            var conversation = await ConversationManager.Call(PersonId, model.PersonBaseId);
            return conversation.MapTo<CreateConversationOutput>();
        }

        /// <summary>
        /// Zakonczenie rozmowy
        /// </summary>
        /// <param name="conversationId">Id rozmowy</param>
        /// <returns>Zaktualizowany model</returns>
        public async Task<Dto.UpdateConversationOutput> End(long conversationId)
        {
            var conversation =await ConversationManager.GetAsync(conversationId);
            conversation = await ConversationManager.EndConversation(PersonId, conversation);
            return conversation.MapTo<UpdateConversationOutput>();
        }

        private async Task<Group> GetGroup(long groupId)
        {
            return await GroupRepository.FirstOrDefaultAsync(g => g.Id == groupId);
        }
    }
}
