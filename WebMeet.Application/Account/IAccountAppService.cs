﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.UI;
using WebMeet.Account.Dto;

namespace WebMeet.Account
{
    /// <summary>
    /// Interfejs odpowiedzialny za operacje dotyczące konta w systemie
    /// </summary>
    public interface IAccountAppService : IApplicationService
    {
        /// <summary>
        /// Logowanie do systemu
        /// </summary>
        /// <param name="model">Dane do logowania</param>
        /// <returns>Rezultat logowania</returns>
        Task<ClaimsIdentity> Login(LoginInput model);
        /// <summary>
        /// Rejestracja
        /// </summary>
        /// <param name="model">Dane do rejestracji</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy login lub email jest zajęty</exception>
        Task Register(RegistrationInput model);
        /// <summary>
        /// Ponowne wysłanie maila potwierdzającego
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie istenieje lub już ma aktywowane konto</exception>
        Task ReSendConfirmationEmail(ReSendConfirmationEmailInput model);
        /// <summary>
        /// Potwierdzenie maila
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie istnieje</exception>
        Task ConfirmEmail(ConfirmEmailInput model);
        /// <summary>
        /// Reset hasłą
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie istenieje</exception>
        Task ResetPassword(ResetPasswordInput model);
        /// <summary>
        /// Wysłanie maila do resetu hasła
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie istenieje</exception>
        Task SendPasswordResetEmail(SendPasswordResetEmailInput model);
        /// <summary>
        /// Wysłanie maila z zaproszeniem do systemu
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy email istnieje w systemie</exception>
        Task Invite(InviteFriendDto model);
    }
}
