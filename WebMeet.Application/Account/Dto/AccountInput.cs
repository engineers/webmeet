﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Account.Dto
{
    /// <summary>
    /// Klasa reprezentująca adres email
    /// </summary>
    public class EmailDto : IInputDto
    {
        /// <summary>
        /// Max dlugość adresu email
        /// </summary>
        public const int MaxEmailLength = 100;

        /// <summary>
        ///     Adres email użytkownika
        /// </summary>
        [Required(ErrorMessage = "Email jest wymagany")]
        [DataType(DataType.EmailAddress)]
        [StringLength(MaxEmailLength)]
        public virtual string EmailAddress { get; set; }
    }

    /// <summary>
    /// Klasa zawierająca podstawowe informacje o użytkowniku
    /// </summary>
    public class UserInfoSendEmailLinkInput : EmailDto
    {
        /// <summary>
        /// Max długość nazwy użytkownika
        /// </summary>
        public const int MaxUserNameLength = 20;

        /// <summary>
        ///     Nazwa użytkownika
        /// </summary>
        [Required(ErrorMessage = "Nazwa jest wymagana")]
        [StringLength(MaxUserNameLength)]
        public virtual string UserName { get; set; }
    }

    /// <summary>
    /// Klasa z informacjami potrzebnymi do wiadomości przekierowującej
    /// </summary>
    public class UserSendRedirectEmailInput : UserInfoSendEmailLinkInput
    {
        /// <summary>
        ///     Adres zwarty w wiadomości
        /// </summary>
        [Required]
        [DataType(DataType.Url)]
        public virtual string BaseUrl { get; set; }
    }

    /// <summary>
    /// Klasa reprezentująca dane użytkownika oraz token do wykonania operacji
    /// </summary>
    public class UserTokenInput : IInputDto
    {
        /// <summary>
        ///     Identyfikator użytkownika
        /// </summary>
        [Required]
        public virtual long UserId { get; set; }

        /// <summary>
        ///     Token do operacji
        /// </summary>
        [Required(ErrorMessage = "Token jest wymagany")]
        public virtual string Token { get; set; }
    }

    /// <summary>
    /// Klasa reprezentująca dane dla operacji ponownego wysłania maila potwierdzającego
    /// </summary>
    public class ReSendConfirmationEmailInput : UserSendRedirectEmailInput
    {
        
    }

    /// <summary>
    /// Klasa reprezentująca dane dla operacji wysłania maila z reset hasła
    /// </summary>
    public class SendPasswordResetEmailInput : UserSendRedirectEmailInput
    {
        
    }

    /// <summary>
    /// Klasa reprezentująca dane dla operacji potwierdzenia maila
    /// </summary>
    public class ConfirmEmailInput : UserTokenInput
    {
        
    }

    /// <summary>
    /// Klasa reprezentująca dane dla operacji resetu hasła
    /// </summary>
    public class ResetPasswordInput : UserTokenInput
    {
        /// <summary>
        /// Max dlugosc hasła
        /// </summary>
        public const int MaxPasswordLength = 100;
        /// <summary>
        /// Min dlugosc hasła
        /// </summary>
        public const int MinPasswordLength = 6;

        /// <summary>
        ///     Hasło
        /// </summary>
        [Required(ErrorMessage = "Hasło jest wymagane")]
        [DataType(DataType.Password)]
        [StringLength(MaxPasswordLength, ErrorMessage = "The {0} must be between {2} and {1} characters long.", MinimumLength = MinPasswordLength)]
        public virtual string Password { get; set; }

        /// <summary>
        ///     Potwierdzenia hasła, musi być takie samo jak Password
        /// </summary>
        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(MaxPasswordLength, ErrorMessage = "The {0} must be between {2} and {1} characters long.", MinimumLength = MinPasswordLength)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }

    /// <summary>
    /// Klasa reprezentująca dane dla operacji rejestracji
    /// </summary>
    public class RegistrationInput : UserSendRedirectEmailInput
    {
        /// <summary>
        /// Max długość imienia
        /// </summary>
        public const int MaxFirstNameLength = 100;
        /// <summary>
        /// Max dlugość nazwiska
        /// </summary>
        public const int MaxLastNameLength = 100;
        /// <summary>
        /// Max długość adresu
        /// </summary>
        public const int MaxAddressLength = 150;
        /// <summary>
        /// Max długość hasła
        /// </summary>
        public const int MaxPasswordLength = 100;
        /// <summary>
        /// Min długość hasła
        /// </summary>
        public const int MinPasswordLength = 6;

        /// <summary>
        /// Imię
        /// </summary>
        [Required]
        [StringLength(MaxFirstNameLength)]
        public string Name { get; set; }
        /// <summary>
        /// Nazwisko
        /// </summary>
        [Required]
        [StringLength(MaxLastNameLength)]
        public string Surname { get; set; }
        /// <summary>
        /// Adres
        /// </summary>
        [StringLength(MaxAddressLength)]
        public string Address { get; set; }

        /// <summary>
        ///     Hasło
        /// </summary>
        [Required(ErrorMessage = "Hasło jest wymagane")]
        [DataType(DataType.Password)]
        [StringLength(MaxPasswordLength, ErrorMessage = "The {0} must be between {2} and {1} characters long.", MinimumLength = MinPasswordLength)]
        public virtual string Password { get; set; }

        /// <summary>
        ///     Potwierdzenie hasła, musi być równe Password
        /// </summary>
        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(MaxPasswordLength, ErrorMessage = "The {0} must be between {2} and {1} characters long.", MinimumLength = MinPasswordLength)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }

    /// <summary>
    /// Dane do logowania
    /// </summary>
    public class LoginInput : IInputDto
    {
        public string TenancyName { get; set; }

        /// <summary>
        /// Nazwa użytkowika lub email
        /// </summary>
        [Required]
        public string UsernameOrEmailAddress { get; set; }

        /// <summary>
        /// Hasło
        /// </summary>
        [Required]
        public string Password { get; set; }

        public string AuthenticationType { get; set; }
    }

    /// <summary>
    /// Klasa reprezentująca dane dla operacji zaproszenia osoby do systemu
    /// </summary>
    public class InviteFriendDto : EmailDto
    {

    }
}
