﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using Newtonsoft.Json.Serialization;
using WebMeet.Account.Dto;
using WebMeet.Authorization.Roles;
using WebMeet.Core.Services.Mail;
using WebMeet.Core.Services.Users;
using WebMeet.Core.Services.Users.Abp;
using WebMeet.Groups;
using WebMeet.Helpers.Extensions;
using WebMeet.Mail;
using WebMeet.MultiTenancy;
using WebMeet.Persons;
using WebMeet.Shared;
using WebMeet.Users;

namespace WebMeet.Account.Implementations
{
    /// <summary>
    /// Klasa odpowiedzialna za operacje dotyczące konta w systemie
    /// </summary>
    public class AccountAppService : ApplicationBaseService, IAccountAppService
    {
        private const string WebMeetWebUrl = "http://91.237.52.249";

        protected readonly AbpUserManager UserManager;
        protected readonly IMailSerivce MailService;
        protected readonly IMailTemplateProvider MailTemplateProvider;
        protected readonly IRepository<Group, long> GroupRepository;

        /// <summary>
        /// Konstrukotr
        /// </summary>
        /// <param name="userManager">UserManager</param>
        /// <param name="mailService">Serwis do wysyłania maili</param>
        /// <param name="mailTemplateProvider">Provider z wiadomościami</param>
        /// <param name="personRepository">Repozytorium osób</param>
        /// <param name="groupRepository">Repozytorium grup</param>
        public AccountAppService(AbpUserManager userManager,
            IMailSerivce mailService,
            IMailTemplateProvider mailTemplateProvider, 
            IRepository<Person, long> personRepository,
            IRepository<Group, long> groupRepository) : base(personRepository)
        {
            UserManager = userManager;
            MailService = mailService;
            MailTemplateProvider = mailTemplateProvider;
            GroupRepository = groupRepository;
        }

        /// <summary>
        /// Logowanie do systemu
        /// </summary>
        /// <param name="model">Dane do logowania</param>
        /// <returns>Rezultat logowania</returns>
        public async Task<ClaimsIdentity> Login(Dto.LoginInput model)
        {
            var result = await GetLoginResultAsync(model.UsernameOrEmailAddress, model.Password, model.TenancyName);
            return await UserManager.CreateIdentityAsync(result.User, model.AuthenticationType);
        }

        /// <summary>
        /// Rejestracja
        /// </summary>
        /// <param name="model">Dane do rejestracji</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy login lub email jest zajęty</exception>
        [UnitOfWork]
        public async Task Register(Dto.RegistrationInput model)
        {
            var user = await UserManager.FindByNameAsync(model.UserName);
            if (user != null)
                throw new UserFriendlyException(L("UsernameExistYet"));
            user = await UserManager.FindByNameOrEmailAsync(model.EmailAddress);
            if (user != null)
                throw new UserFriendlyException(L("EmailExistYet"));

            var person = model.MapTo<Person>();
            person.User.Password = UserManager.PasswordHasher.HashPassword(model.Password);
            person.Id = GetIdForInsert();

            await PersonRepository.InsertAsync(person);
            await UnitOfWorkManager.Current.SaveChangesAsync();
   
            await SendTokenEmail(
                model,
                UserManager.GenerateEmailConfirmationTokenAsync,
                MailTemplateProvider.EmailConfirmationTemplate);
        }

        /// <summary>
        /// Ponowne wysłanie maila potwierdzającego
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie istenieje lub już ma aktywowane konto</exception>
        public async Task ReSendConfirmationEmail(Dto.ReSendConfirmationEmailInput model)
        {
            var user = await UserManager.FindByNameAsync(model.UserName);
            if (user == null)
                throw new UserFriendlyException(L("UserNotExist"));
            if (user.EmailAddress != model.EmailAddress)
                throw new UserFriendlyException(L("NotMatch"), "Email");
            if (user.IsEmailConfirmed)
                throw new UserFriendlyException(L("EmailConfirmedYet"));

            await SendTokenEmail(
                model,
                UserManager.GenerateEmailConfirmationTokenAsync,
                MailTemplateProvider.EmailConfirmationTemplate);
        }

        /// <summary>
        /// Potwierdzenie maila
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie istnieje</exception>
        public async Task ConfirmEmail(Dto.ConfirmEmailInput model)
        {
            if ( ( await UserManager.FindByIdAsync(model.UserId) ) == null)
                throw new UserFriendlyException(L("UserNotExist"));

            var result = await UserManager.ConfirmEmailAsync(model.UserId, model.Token);
            if(!result.Succeeded)
                throw new UserFriendlyException(L("ErrorOccured"), result.Errors.Join());
        }

        /// <summary>
        /// Reset hasłą
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie istenieje</exception>
        public async Task ResetPassword(Dto.ResetPasswordInput model)
        {
            if ((await UserManager.FindByIdAsync(model.UserId)) == null)
                throw new UserFriendlyException(L("UserNotExist"));

            var result = await UserManager.ResetPasswordAsync(model.UserId, model.Token, model.Password);
            if (!result.Succeeded)
                throw new UserFriendlyException(L("ErrorOccured"), result.Errors.Join());
        }

        /// <summary>
        /// Wysłanie maila do resetu hasła
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy użytkownik nie istenieje</exception>
        public async Task SendPasswordResetEmail(Dto.SendPasswordResetEmailInput model)
        {
            var user = await UserManager.FindByNameAsync(model.UserName);
            if (user == null)
                throw new UserFriendlyException(L("UserNotExist"));
            if (user.EmailAddress != model.EmailAddress)
                throw new UserFriendlyException(L("NotMatch"), "Email");

            await SendTokenEmail(
                model,
                UserManager.GeneratePasswordResetTokenAsync,
                MailTemplateProvider.PasswordResetTemplate);
        }

        /// <summary>
        /// Wysłanie maila z zaproszeniem do systemu
        /// </summary>
        /// <param name="model">Dane</param>
        /// <exception cref="UserFriendlyException">Rzucany gdy email istnieje w systemie</exception>
        [AbpAuthorize]
        public async Task Invite(InviteFriendDto model)
        {
            //Check if user with given mail doesn't exist
            if((await UserManager.FindByEmailAsync(model.EmailAddress))!= null)
                throw new UserFriendlyException(L("EmailExistYet"));
            var loggedPerson = GetLoggedPerson();

            var messageProvider = MailTemplateProvider.FriendInvitationToSystemTemplate(model.EmailAddress,string.Format("{0} {1}", loggedPerson.FirstName, loggedPerson.LastName), loggedPerson.User.UserName, WebMeetWebUrl);
            await MailService.SendMailAsync(messageProvider);
        }

        protected async Task SendTokenEmail(
            UserSendRedirectEmailInput model,
            Func<long, Task<string>> generateTokenFunc,
            Func<string, string, string, Func<MailMessage>> templateProviderFunc)
        {
            if (string.IsNullOrWhiteSpace(model.EmailAddress))
                throw new UserFriendlyException(L("EmailEmpty"));
            if (string.IsNullOrWhiteSpace(model.BaseUrl)
                || !Uri.IsWellFormedUriString(model.BaseUrl, UriKind.Absolute))
                throw new UserFriendlyException(L("UrlNotValid"));
            var user = await UserManager.FindByEmailAsync(model.EmailAddress);
            if (user == null)
            {
                throw new UserFriendlyException();
            }
            var userId =user.Id;
            var token = await generateTokenFunc(userId);
            var linkUrl = new Url(model.BaseUrl).SetQueryParams(new { userId, token });
            var messageProvider = templateProviderFunc(user.UserName, model.EmailAddress, linkUrl.Value);
            await MailService.SendMailAsync(messageProvider);            
        }

        private async Task<AbpUserManager<Tenant, Role, User>.AbpLoginResult> GetLoginResultAsync(string usernameOrEmailAddress, string password, string tenancyName)
        {
            var loginResult = await UserManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    return loginResult;
                default:
                    throw CreateExceptionForFailedLoginAttempt(loginResult.Result, usernameOrEmailAddress, tenancyName);
            }
        }

        private Exception CreateExceptionForFailedLoginAttempt(AbpLoginResultType result, string usernameOrEmailAddress, string tenancyName)
        {
            switch (result)
            {
                case AbpLoginResultType.Success:
                    return new ApplicationException("Don't call this method with a success result!");
                case AbpLoginResultType.InvalidUserNameOrEmailAddress:
                case AbpLoginResultType.InvalidPassword:
                    return new UserFriendlyException(L("LoginFailed"), L("InvalidUserNameOrPassword"));
                case AbpLoginResultType.InvalidTenancyName:
                    return new UserFriendlyException(L("LoginFailed"), L("ThereIsNoTenantDefinedWithName{0}", tenancyName));
                case AbpLoginResultType.TenantIsNotActive:
                    return new UserFriendlyException(L("LoginFailed"), L("TenantIsNotActive", tenancyName));
                case AbpLoginResultType.UserIsNotActive:
                    return new UserFriendlyException(L("LoginFailed"), L("UserIsNotActiveAndCanNotLogin", usernameOrEmailAddress));
                case AbpLoginResultType.UserEmailIsNotConfirmed:
                    return new UserFriendlyException(L("LoginFailed"), "Your email address is not confirmed. You can not login"); //TODO: localize message
                default: //Can not fall to default actually. But other result types can be added in the future and we may forget to handle it
                    Logger.Warn("Unhandled login fail reason: " + result);
                    return new UserFriendlyException(L("LoginFailed"));
            }
        }

        private long GetIdForInsert()
        {
            long personMax = 1;
            long groupMax = 1;
            if (PersonRepository.GetAll().Any())
                personMax = PersonRepository.GetAll().Max(p => p.Id) + 1;
            if (GroupRepository.GetAll().Any())
                groupMax = GroupRepository.GetAll().Max(p => p.Id) + 1;
            return personMax > groupMax ? personMax : groupMax;
        }
    }
}
