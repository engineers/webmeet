﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Friends.Invitation.Dto;

namespace WebMeet.Friends.Invitation
{
    /// <summary>
    /// Interfejs definiujący operacje na zaproszeniach do znajomości
    /// </summary>
    public interface IInvitationAppService : IApplicationService
    {
        /// <summary>
        /// Pobranie zaproszeń odebranych
        /// </summary>
        /// <param name="query">Model</param>
        SearchReceivedInvitationsOutput Inbox(SearchReceivedInvitationsInput query);
        /// <summary>
        /// Pobranie zaproszeń wyslanych
        /// </summary>
        /// <param name="query">Model</param>
        SearchSentInvitationsOutput Outbox(SearchSentInvitationsInput query);
        /// <summary>
        /// Wysłanie zaproszenia
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworozne zaproszenie</returns>
        Task<CreateInvitationOutput> Create(CreateInvitationInput model);
        /// <summary>
        /// Uktualizacja zaproszenia
        /// </summary>
        /// <param name="id">Id zaproszenia</param>
        /// <param name="model">Model</param>
        /// <returns>Zaktualizowane zaproszenie</returns>
        Task<UpdateInvitationOutput> Update(long id, UpdateInvitationInput model);
    }
}
