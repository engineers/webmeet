﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Friends.Invitations;

namespace WebMeet.Friends.Invitation.Dto
{
    /// <summary>
    /// Wynik operacji wysłania zaproszenia
    /// </summary>
    public class CreateInvitationOutput : UpdateInvitationOutput { }

    /// <summary>
    /// Wynik operacji aktualizacji zaproszenia
    /// </summary>
    public class UpdateInvitationOutput : InvitationDto, IOutputDto { }

    /// <summary>
    /// Wynik operacji wyszukiwania zaproszeń
    /// </summary>
    public class SearchInvitationsOutput : PagedResultOutput<InvitationDto>
    {
    }

    /// <summary>
    /// Wynik operacji wyszukiwania otrzymanych zaproszeń
    /// </summary>
    public class SearchReceivedInvitationsOutput : PagedResultOutput<InvitationDto>
    {
    }

    /// <summary>
    /// Wynik operacji wyszukiwania wysłanych zaproszeń
    /// </summary>
    public class SearchSentInvitationsOutput : PagedResultOutput<InvitationDto>
    {
    }
}
