﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Friends.Invitations;

namespace WebMeet.Friends.Invitation.Dto
{
    /// <summary>
    /// Klasa reprezentująca DTO dla typu <see cref="Invitation"/>
    /// </summary>
    public class InvitationDto : EntityDto<long>
    {
        /// <summary>
        /// Id nadawcy
        /// </summary>
        public long SenderId { get; set; }
        /// <summary>
        /// Nazwa nadawcy
        /// </summary>
        public string SenderName { get; set; }
        /// <summary>
        /// Id odbiorcy
        /// </summary>
        public long ReceiverId { get; set; }
        /// <summary>
        /// Nazwa odbiorcy
        /// </summary>
        public string ReceiverName { get; set; }
        /// <summary>
        /// Stan zaproszenia
        /// </summary>
        public InvitationState State { get; set; }
    }
}
