﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Friends.Invitations;
using WebMeet.Shared;

namespace WebMeet.Friends.Invitation.Dto
{
    /// <summary>
    /// Klasa zawierająca dane dla operacji wysłania zaproszenia
    /// </summary>
    public class CreateInvitationInput : IInputDto
    {
        public const string InvitationReceiverRequiredErrorMessage = "Odbiorca zaproszenia jest wymagany";

        /// <summary>
        /// Id odbiorcy
        /// </summary>
        [Required(ErrorMessage = InvitationReceiverRequiredErrorMessage)]
        public long ReceiverId { get; set; }
    }

    /// <summary>
    /// Klasa zawierajaca dane do zaktualizowania stanu zaproszenia
    /// </summary>
    public class UpdateInvitationInput : IInputDto
    {
        public const string InvitationStateRequiredErrorMessage = "Stan zaproszenia jest wymagany";

        /// <summary>
        /// Stan zaproszenia
        /// </summary>
        [Required(ErrorMessage = InvitationStateRequiredErrorMessage)]
        public InvitationState State { get; set; }
    }

    /// <summary>
    /// Dane do pobrania zaproszeń
    /// </summary>
    public abstract class SearchInvitationsInput : SearchInputBase
    {
        /// <summary>
        /// Stan zaproszenia
        /// </summary>
        public InvitationState? State { get; set; }
    }

    /// <summary>
    /// Dane do pobrania zaproszeń otrzymanych
    /// </summary>
    public class SearchReceivedInvitationsInput : SearchInvitationsInput
    {        
    }

    /// <summary>
    /// Dane do pobrania zaproszeń wysłanych
    /// </summary>
    public class SearchSentInvitationsInput : SearchInvitationsInput
    {
    }
}
