﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using AutoMapper;
using WebMeet.Core.Services.Invitations;
using WebMeet.Extensions;
using WebMeet.Friends.Invitation.Dto;
using WebMeet.Friends.Invitations;
using WebMeet.Helpers.Extensions;
using WebMeet.Persons;
using WebMeet.Shared;

namespace WebMeet.Friends.Invitation.Implementation
{
    /// <summary>
    /// Serwis odpowiedzilany za zaproszenia do znajomości
    /// </summary>
    [AbpAuthorize]
    public class InvitationAppService : ApplicationBaseService, IInvitationAppService
    {
        protected readonly IInvitationManager InvitationManager;
        
        protected readonly IRepository<Invitations.Invitation, long> InvitationRepository;

        /// <summary>
        /// Konstrukotr
        /// </summary>
        /// <param name="invitationManager">Serwis domenowy zaproszeń</param>
        /// <param name="invitationRepository">Repozytorium zaproszeń</param>
        /// <param name="personRepository">Repozytorium osób</param>
        public InvitationAppService(IInvitationManager invitationManager,
            IRepository<Invitations.Invitation, long> invitationRepository,
            IRepository<Person, long> personRepository) : base(personRepository)
        {
            InvitationManager = invitationManager;
            InvitationRepository = invitationRepository;
        }

        /// <summary>
        /// Pobranie zaproszeń odebranych
        /// </summary>
        /// <param name="query">Model</param>
        public SearchReceivedInvitationsOutput Inbox(SearchReceivedInvitationsInput query)
        {
            var queryable = GetInvitations(query)
                .Where(invitation => invitation.ReceiverId == PersonId);
            var count = queryable.Count();
            var items = queryable.FilterPage(query)
                .ToList()
                .Select(Mapper.Map<InvitationDto>);

            return new SearchReceivedInvitationsOutput
            {
                Items = items.ToList(),
                TotalCount = count
            };
        }

        /// <summary>
        /// Pobranie zaproszeń wyslanych
        /// </summary>
        /// <param name="query">Model</param>
        public SearchSentInvitationsOutput Outbox(SearchSentInvitationsInput query)
        {
            var queryable = GetInvitations(query)
                .Where(invitation => invitation.SenderId == PersonId);
            var count = queryable.Count();
            var items = queryable.FilterPage(query)
                .ToList()
                .Select(Mapper.Map<InvitationDto>);

            return new SearchSentInvitationsOutput
            {
                Items = items.ToList(),
                TotalCount = count
            };
        }
              
        /// <summary>
        /// Wysłanie zaproszenia
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworozne zaproszenie</returns>
        public async Task<CreateInvitationOutput> Create(CreateInvitationInput model)
        {
            var senderPerson = GetLoggedPerson();
            var receiverPerson = await GetPersonById(model.ReceiverId);

            var invitation = await InvitationManager.InvitePerson(senderPerson, receiverPerson);
            return invitation.MapTo<CreateInvitationOutput>();
        }

        /// <summary>
        /// Uktualizacja zaproszenia
        /// </summary>
        /// <param name="id">Id zaproszenia</param>
        /// <param name="model">Model</param>
        /// <returns>Zaktualizowane zaproszenie</returns>
        public async Task<UpdateInvitationOutput> Update(long id, UpdateInvitationInput model)
        {
            var oldEntity = await InvitationManager.GetAsync(id);
            if(oldEntity.State != model.State)
                ChooseActionForByNewInvitationState(oldEntity, model.State);
            return oldEntity.MapTo<UpdateInvitationOutput>();
        }

        private async Task<Person> GetPersonById(long personId)
        {
            var person = await PersonRepository.FirstOrDefaultAsync(personId);
            if(person == null)
                throw new UserFriendlyException(L("NotFound", personId));
            return person;
        }

        private void ChooseActionForByNewInvitationState(Invitations.Invitation entity, InvitationState state)
        {
            var loggedPersonId = GetLoggedPerson().Id;
            switch (state)
            {
                case InvitationState.Accepted:
                    InvitationManager.AcceptInvitation(loggedPersonId, entity);
                    break;
                case InvitationState.Rejected:
                    InvitationManager.RejectInvitation(loggedPersonId,entity);
                    break;
                case InvitationState.Canceled:
                    InvitationManager.CancelInvitation(loggedPersonId, entity);
                    break;
                default:
                    throw new UserFriendlyException(L("IllegalOperation"));
            }
        }

        private IQueryable<Invitations.Invitation> GetInvitations(SearchInvitationsInput query)
        {
            var queryable = InvitationRepository.GetAll();
            return queryable
                .FilterIfNotNull(query.State, invitation => invitation.State == query.State);
        } 
    }
}
