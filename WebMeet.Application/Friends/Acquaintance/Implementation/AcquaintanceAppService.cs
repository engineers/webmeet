﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using AutoMapper;
using WebMeet.Core.Services.Acquaintances;
using WebMeet.Extensions;
using WebMeet.Friends.Acquaintance.Dto;
using WebMeet.Friends.Acquaintance.Extensions;
using WebMeet.Persons;
using WebMeet.Shared;

namespace WebMeet.Friends.Acquaintance.Implementation
{
    /// <summary>
    /// Serwis odpowiedzilany za operacje dotyczące znajomości
    /// </summary>
    [AbpAuthorize]
    public class AcquaintanceAppService : ApplicationBaseService, IAcquaintanceAppService
    {
        protected readonly IRepository<Acquaintances.Acquaintance, long> AcquaintanceRepository;
        protected readonly IAcquaintanceManager AcquaintanceManager;

        /// <summary>
        /// Konstrukotr
        /// </summary>
        /// <param name="personRepository">Repozytorium osób</param>
        /// <param name="acquaintanceRepository">Repozytorium znajomości</param>
        /// <param name="acquaintanceManager">Serwis domenowy znajomości</param>
        public AcquaintanceAppService(IRepository<Person, long> personRepository,
            IRepository<Acquaintances.Acquaintance, long> acquaintanceRepository, 
            IAcquaintanceManager acquaintanceManager)
            : base(personRepository)
        {
            AcquaintanceRepository = acquaintanceRepository;
            AcquaintanceManager = acquaintanceManager;
        }

        /// <summary>
        /// Pobranie znajomości zgodnie z parametrami w modelu
        /// </summary>
        /// <param name="query">Model</param>
        public Dto.GetAcquaintancesOutput Get(GetAcquaintancesInput query)
        {
            var loggedPersonId = PersonId;
            var queryable = AcquaintanceRepository
                .GetAll()
                .Where(acq => acq.FirstPersonId == loggedPersonId || acq.SecondPersonId == loggedPersonId);           
            var count = queryable.Count();

            var items = queryable.FilterPage(query)
                .ToList()
                .ToAcquaintanceDtos(loggedPersonId);

            return new GetAcquaintancesOutput
            {
                Items = items.ToList(),
                TotalCount = count
            };
        }

        /// <summary>
        /// Usuwa znajomość o podanym id
        /// </summary>
        /// <param name="id">Id znajomości</param>
        public async Task Delete(long id)
        {
            var acquaintance = await AcquaintanceManager.GetAsync(id);
            AcquaintanceManager.Delete(PersonId, acquaintance);
        }
    }
}
