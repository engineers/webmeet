﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Friends.Acquaintance.Dto;

namespace WebMeet.Friends.Acquaintance
{
    public interface IAcquaintanceAppService : IApplicationService
    {
        /// <summary>
        /// Pobranie znajomości zgodnie z parametrami w modelu
        /// </summary>
        /// <param name="query">Model</param>
        GetAcquaintancesOutput Get(GetAcquaintancesInput query);
        /// <summary>
        /// Usuwa znajomość o podanym id
        /// </summary>
        /// <param name="id">Id znajomości</param>
        Task Delete(long id);
    }
}
