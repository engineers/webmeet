﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Friends.Acquaintance.Dto
{
    /// <summary>
    /// Wynik operacji wyszukiwania znajomych
    /// </summary>
    public class GetAcquaintancesOutput : PagedResultOutput<AcquaintanceDto>
    {
    }

}
