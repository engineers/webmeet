﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Friends.Acquaintance.Dto
{
    /// <summary>
    /// Klasa reprezentująca DTO dla typu <see cref="WebMeet.Friends.Acquaintance"/>
    /// </summary>
    public class AcquaintanceDto : EntityDto<long>
    {
        /// <summary>
        /// Id znajomego
        /// </summary>
        public long PersonId { get; set; }
        /// <summary>
        /// Nazwa znajomego
        /// </summary>
        public string PersonName { get; set; }
    }
}
