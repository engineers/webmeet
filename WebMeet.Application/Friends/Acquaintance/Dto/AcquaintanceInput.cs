﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMeet.Shared;

namespace WebMeet.Friends.Acquaintance.Dto
{
    /// <summary>
    /// Klasa reprezentująca dane do operacji pobrania znajomych
    /// </summary>
    public class GetAcquaintancesInput : SearchInputBase
    {
    }
}
