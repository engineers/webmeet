﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMeet.Friends.Acquaintance.Dto;

namespace WebMeet.Friends.Acquaintance.Extensions
{
    public static class AcquaintanceExtensions
    {
        /// <summary>
        /// Konwertuje kolekcję typu <see cref="WebMeet.Friends.Acquaintance" /> do kolekcji typu <see cref="AcquaintanceDto"/>/>
        /// </summary>
        /// <param name="acquaintances">Kolekcja</param>
        /// <param name="loggedPersonId">Id zalogowanej osoby</param>
        public static IEnumerable<AcquaintanceDto> ToAcquaintanceDtos(this IEnumerable<Acquaintances.Acquaintance> acquaintances, long loggedPersonId)
        {
            return acquaintances.Select(acquaintance =>
            {
                var secondPerson = acquaintance.FirstPersonId == loggedPersonId
                    ? acquaintance.SecondPerson
                    : acquaintance.FirstPerson;
                return new AcquaintanceDto
                {
                    Id = acquaintance.Id,
                    PersonId = secondPerson.Id,
                    PersonName = string.Format("{0} {1}", secondPerson.FirstName, secondPerson.LastName)
                };
            });
        }
    }
}
