﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Friends.Blacklist.Dto;

namespace WebMeet.Friends.Blacklist
{
    /// <summary>
    /// Interfejs definiujący operacje na czarnej liście
    /// </summary>
    public interface IBlacklistAppService : IApplicationService
    {
        /// <summary>
        /// Pobranie czarnej listy
        /// </summary>
        /// <param name="query">Model</param>
        GetBlacklistItemsOutput Get(GetBlacklistItemsInput query);
        /// <summary>
        /// Usunięcie obiektu z czarnej listy
        /// </summary>
        /// <param name="id">Id obiektu na czarnej liście</param>
        Task Delete(long id);
        /// <summary>
        /// Dodaje obiekt do czarnej listy
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworzony obiekt</returns>
        Task<CreateBlacklistItemOutput> Create(CreateBlacklistItemInput model);
    }
}
