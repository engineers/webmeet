﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using AutoMapper;
using WebMeet.Core.Services.Blacklist;
using WebMeet.Extensions;
using WebMeet.Friends.Blacklist.Dto;
using WebMeet.Friends.Blacklists;
using WebMeet.Persons;
using WebMeet.Shared;

namespace WebMeet.Friends.Blacklist.Implementation
{
    /// <summary>
    /// Serwis odpowiedzilny za operacje na czarnej liście
    /// </summary>
    [AbpAuthorize]
    public class BlacklistAppService :ApplicationBaseService, IBlacklistAppService
    {
        protected readonly IRepository<BlacklistItem, long> BlacklistItemRepository;
        protected readonly IBlacklistItemManager BlacklistItemManager;
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="personRepository">Repozytorium osób</param>
        /// <param name="blacklistItemRepository">Repozytorium czarnych list</param>
        /// <param name="blacklistItemManager">Serwis domenowy czarnej listy</param>
        public BlacklistAppService(IRepository<Person, long> personRepository, 
            IRepository<BlacklistItem, long> blacklistItemRepository, 
            IBlacklistItemManager blacklistItemManager) : base(personRepository)
        {
            BlacklistItemRepository = blacklistItemRepository;
            BlacklistItemManager = blacklistItemManager;
        }

        /// <summary>
        /// Pobranie czarnej listy
        /// </summary>
        /// <param name="query">Model</param>
        public Dto.GetBlacklistItemsOutput Get(Dto.GetBlacklistItemsInput query)
        {
            var queryable = BlacklistItemRepository
                .GetAll()
                .Where(item => item.OwnerId == PersonId);
            var count = queryable.Count();
            var items = queryable
                .FilterPage(query)
                .ToList()
                .Select(Mapper.Map<BlacklistItemDto>);

            return new GetBlacklistItemsOutput
            {
                TotalCount = count,
                Items = items.ToList()
            };
        }

        /// <summary>
        /// Usunięcie obiektu z czarnej listy
        /// </summary>
        /// <param name="id">Id obiektu na czarnej liście</param>
        public async Task Delete(long id)
        {
            var item = await BlacklistItemManager.GetAsync(id);
            BlacklistItemManager.Delete(PersonId, item);
        }

        /// <summary>
        /// Dodaje obiekt do czarnej listy
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworzony obiekt</returns>
        public async Task<Dto.CreateBlacklistItemOutput> Create(Dto.CreateBlacklistItemInput model)
        {
            var person = GetPerson(model.PersonId);
            var item = await BlacklistItemManager.AddPersonToBlacklist(PersonId, person);
            return item.MapTo<CreateBlacklistItemOutput>();
        }

        private Person GetPerson(long id)
        {
            var person = PersonRepository.Get(id);
            if(person == null)
                throw new UserFriendlyException(L("ErrorDetected"), L("NotFound", id));
            return person;
        }
    }
}
