﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Friends.Blacklists;

namespace WebMeet.Friends.Blacklist.Dto
{
    /// <summary>
    /// Klasa reprezentująca DTO dla typu <see cref="BlacklistItem"/>
    /// </summary>
    public class BlacklistItemDto : EntityDto<long>
    {
        /// <summary>
        /// Id wlasciciela czarnej listy
        /// </summary>
        public virtual long OwnerId { get; set; }
        /// <summary>
        /// Nazwa właściciela
        /// </summary>
        public virtual string OwnerName { get; set; }
        /// <summary>
        /// Id osoby na czarnej liście
        /// </summary>
        public virtual long PersonId { get; set; }
        /// <summary>
        /// Nazwa osoby
        /// </summary>
        public virtual string PersonName { get; set; }
    }
}
