﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Friends.Blacklist.Dto
{
    /// <summary>
    /// Wynik operacji pobrania czarnej listy
    /// </summary>
    public class GetBlacklistItemsOutput : PagedResultOutput<BlacklistItemDto>
    {
    }

    /// <summary>
    /// Wynik operacji dodania osoby na czarną listę
    /// </summary>
    public class CreateBlacklistItemOutput : BlacklistItemDto, IOutputDto { }
}
