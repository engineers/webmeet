﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using WebMeet.Shared;

namespace WebMeet.Friends.Blacklist.Dto
{
    /// <summary>
    /// Klasa zawierająca informacje do pobrania czarnej listy
    /// </summary>
    public class GetBlacklistItemsInput : SearchInputBase
    {
    }

    /// <summary>
    /// Klasa zawierająca informacje do dodania osoby na czarną listę
    /// </summary>
    public class CreateBlacklistItemInput : IInputDto
    {
        public const string BlacklistPersonIdRequiredErrorMessage = "Id użytkownika do umieszczenia na czarnej liście jest wymagane";

        /// <summary>
        /// Id osoby do dodania
        /// </summary>
        [Required(ErrorMessage = BlacklistPersonIdRequiredErrorMessage)]
        public long PersonId { get; set; }
    }
}
