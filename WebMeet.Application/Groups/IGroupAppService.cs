﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Groups.Dto;

namespace WebMeet.Groups
{
    /// <summary>
    /// Interfejs definiujący operacje na grupach
    /// </summary>
    public interface IGroupAppService : IApplicationService
    {
        /// <summary>
        /// Tworzy grupę
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworzona grupa</returns>
        Task<CreateGroupOutput> Create(CreateGroupInput model);
        /// <summary>
        /// Aktualizuje grupę
        /// </summary>
        /// <param name="id">Id grupy</param>
        /// <param name="model">Model grupy</param>
        /// <returns>Zaktualizowaną grupę</returns>
        Task<UpdateGroupOutput> Update(long id, UpdateGroupOutput model);
        /// <summary>
        /// Usuwa grupę o podanym id
        /// </summary>
        /// <param name="groupId">Id grupy</param>
        Task Delete(long groupId);
        /// <summary>
        /// Usuwa osobę wykonującą operację z grupy
        /// </summary>
        /// <param name="groupId">Id grupy</param>
        Task DeleteMe(long groupId);
        /// <summary>
        /// Dodaje osoby do grupy
        /// </summary>
        /// <param name="model">Model z osobami</param>
        Task AddPersons(AddPersonsToGroupInput model);
        /// <summary>
        /// Usuwa osoby z grupy
        /// </summary>
        /// <param name="model">Model z osobami</param>
        Task DeletePersons(DeletePersonsFromGroupInput model);
        /// <summary>
        /// Pobiera grupy zgodnie z parametrami w modelu
        /// </summary>
        /// <param name="query">Model</param>
        SearchGroupsOutput Get(SearchGroupsInput query);
        /// <summary>
        /// Pobiera o grupę o danym id
        /// </summary>
        /// <param name="groupId">Id grupy</param>
        /// <returns>Grupę</returns>
        Task<GroupDto> GetById(long groupId);
    }
}
