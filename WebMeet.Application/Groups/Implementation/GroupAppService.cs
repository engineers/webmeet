﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using AutoMapper;
using WebMeet.Core.Services.Groups;
using WebMeet.Extensions;
using WebMeet.Groups.Dto;
using WebMeet.Helpers.Extensions;
using WebMeet.Persons;
using WebMeet.Shared;

namespace WebMeet.Groups.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na grupach
    /// </summary>
    [AbpAuthorize]
    class GroupAppService : ApplicationBaseService, IGroupAppService
    {
        protected readonly IGroupManager GroupManager;
        protected readonly IRepository<Group, long> GroupRepository;
 
        /// <summary>
        /// Konstrukotr
        /// </summary>
        /// <param name="personRepository">Repozytorium osób</param>
        /// <param name="groupManager">Serwis domenowy grup</param>
        /// <param name="groupRepository">Repozytorium grup</param>
        public GroupAppService(IRepository<Person, long> personRepository, 
            IGroupManager groupManager, 
            IRepository<Group, long> groupRepository) : base(personRepository)
        {
            GroupManager = groupManager;
            GroupRepository = groupRepository;
        }

        /// <summary>
        /// Tworzy grupę
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Utworzona grupa</returns>
        public async Task<CreateGroupOutput> Create(CreateGroupInput model)
        {
            var name = string.IsNullOrWhiteSpace(model.Name)
                ? await GenerateGroupName(new List<long>(model.Persons){PersonId})
                : model.Name;
            var group =await GroupManager.Create(PersonId, new Group
            {
                Avatar = model.Avatar,
                Name = name
            });

            UnitOfWorkManager.Current.SaveChanges();           

            foreach (var personId in model.Persons)
            {
                await GroupManager.AddPerson(PersonId, personId, group.Id);
            }
            return group.MapTo<CreateGroupOutput>();
        }

        /// <summary>
        /// Aktualizuje grupę
        /// </summary>
        /// <param name="id">Id grupy</param>
        /// <param name="model">Model grupy</param>
        /// <returns>Zaktualizowaną grupę</returns>
        public async Task<UpdateGroupOutput> Update(long id, UpdateGroupOutput model)
        {
            var group = await GroupManager.GetAsync(id);
            group.Name = model.Name;
            group.Avatar = model.Avatar;
            group = await GroupManager.Update(PersonId, group);
            return group.MapTo<UpdateGroupOutput>();
        }

        /// <summary>
        /// Usuwa osobę wykonującą operację z grupy
        /// </summary>
        /// <param name="groupId">Id grupy</param>
        public async Task DeleteMe(long groupId)
        {
            var group = await GroupManager.GetAsync(groupId);
            await GroupManager.DeletePerson(PersonId, PersonId, group.Id);
        }

        /// <summary>
        /// Dodoaje osoby do grupy
        /// </summary>
        /// <param name="model">Model z osobami</param>
        public async Task AddPersons(AddPersonsToGroupInput model)
        {
            var group = await GroupManager.GetAsync(model.GroupId);
            foreach (var personId in model.Persons)
            {
                var person = await CheckPersonExistence(personId);
                await GroupManager.AddPerson(PersonId, person.Id, group.Id);
            }
        }

        /// <summary>
        /// Usuwa osoby z grupy
        /// </summary>
        /// <param name="model">Model z osobami</param>
        public async Task DeletePersons(DeletePersonsFromGroupInput model)
        {
            var group = await GroupManager.GetAsync(model.GroupId);
            foreach (var personId in model.Persons)
            {
                var person = await CheckPersonExistence(personId);
                await GroupManager.DeletePerson(PersonId, person.Id, group.Id);
            } 
        }

        /// <summary>
        /// Pobiera grupy zgodnie z parametrami w modelu
        /// </summary>
        /// <param name="query">Model</param>
        public SearchGroupsOutput Get(SearchGroupsInput query)
        {
            var queryable = GroupRepository
                .GetAll()
                .FilterIfNotNullOrEmpty(query.Name, group => group.Name.StartsWith(query.Name))
                .Where(group => group.GroupUsers.Any(gu => gu.PersonId == PersonId));
                
            var count = queryable.Count();
            var items = queryable
               .FilterPage(query)
               .ToList()
               .Select(Mapper.Map<GroupDto>);

            return new SearchGroupsOutput
            {
                TotalCount = count,
                Items = items.ToList()
            };
        }

        /// <summary>
        /// Pobiera o grupę o danym id
        /// </summary>
        /// <param name="groupId">Id grupy</param>
        /// <returns>Grupę</returns>
        public async Task<GroupDto> GetById(long groupId)
        {
            var group = await GroupManager.Get(PersonId, groupId);
            return group.MapTo<GroupDto>();
        }

        /// <summary>
        /// Usuwa grupę o podanym id
        /// </summary>
        /// <param name="groupId">Id grupy</param>
        public async Task Delete(long groupId)
        {
            var group = await GroupManager.GetAsync(groupId);
            await GroupManager.Delete(PersonId, group);
        }

        private async Task<Person> CheckPersonExistence(long personId)
        {
            var person = await PersonRepository.FirstOrDefaultAsync(p => p.Id == personId);
            if(person == null)
                throw new UserFriendlyException(L("NotFound", personId));
            return person;
        }

        private async Task<string> GenerateGroupName(IEnumerable<long> personIds)
        {
            var builder = new StringBuilder();
            foreach (var personId in personIds)
            {
                long id = personId;
                var person = await CheckPersonExistence(id);

                if(builder.Length>0)
                    builder.Append(", ");
                builder.Append(string.Format("{0}. {1}", person.FirstName.First(), person.LastName));               
            }

            return builder.ToString();
        }
    }
}
