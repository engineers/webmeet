﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Shared;

namespace WebMeet.Groups.Dto
{
    /// <summary>
    /// Klasa zawierająca dane do aktualizacji grupy
    /// </summary>
    public class UpdateGroupInput : IInputDto
    {
        /// <summary>
        /// Max dlugość nazwy grupy
        /// </summary>
        public const int MaxNameLength = 200;

        /// <summary>
        /// Nazwa grupy
        /// </summary>
        [Required]
        [StringLength(MaxNameLength)]
        public string Name { get; set; }

        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar { get; set; }
    }

    /// <summary>
    /// Dane do utworzenia grupy
    /// </summary>
    public class CreateGroupInput : IInputDto
    {
        /// <summary>
        /// Osoby ktore dodać do grupy
        /// </summary>
        [MinLength(1)]
        public long[] Persons { get; set; }

        /// <summary>
        /// Nazwa grupy
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar { get; set; }
    }

    /// <summary>
    /// Dane dla operacji dodania osób do grupy
    /// </summary>
    public class AddPersonsToGroupInput : IInputDto
    {
        /// <summary>
        /// Id grupy
        /// </summary>
        [Required]
        public long GroupId { get; set; }
        /// <summary>
        /// Osoby do dodania
        /// </summary>
        [MinLength(1)]
        public long[] Persons { get; set; }
    }

    /// <summary>
    /// Dane dla operacji usunięcia osób z grupy
    /// </summary>
    public class DeletePersonsFromGroupInput : AddPersonsToGroupInput { }

    /// <summary>
    /// Dane do pobrania grup
    /// </summary>
    public class SearchGroupsInput : SearchInputBase
    {
        /// <summary>
        /// Nazwa grupy
        /// </summary>
        public string Name { get; set; }
    }
}
