﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Shared;

namespace WebMeet.Groups.Dto
{
    /// <summary>
    /// Klasa reprezentująca DTO dla typu <see cref="Group"/>
    /// </summary>
    public class GroupDto : EntityDto<long>
    {
        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar { get; set; }
        /// <summary>
        /// Nazwa grupy
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Osoby należace do grupy
        /// </summary>
        public IEnumerable<IdValueDto<long>> Persons { get; set; }
    }
}
