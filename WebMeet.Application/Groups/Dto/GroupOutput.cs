﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Groups.Dto
{
    /// <summary>
    /// Wynik operacji aktualizacji grupy
    /// </summary>
    public class UpdateGroupOutput : IOutputDto
    {
        /// <summary>
        /// Nazwa grupy
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar { get; set; }
    }

    /// <summary>
    /// Wynik operacji utworzenia grupy
    /// </summary>
    public class CreateGroupOutput : UpdateGroupOutput { }

    /// <summary>
    /// Wynik operacji wyszukiwania grup
    /// </summary>
    public class SearchGroupsOutput : PagedResultOutput<GroupDto> { }
}
