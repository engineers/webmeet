﻿using System.Threading.Tasks;
using Abp.Auditing;
using Abp.Authorization;
using Abp.AutoMapper;
using WebMeet.Sessions.Dto;

namespace WebMeet.Sessions
{
    [AbpAuthorize]
    public class SessionAppService : WebMeetAppServiceBase, ISessionAppService
    {
        /// <summary>
        /// Pobranie informacji o aktualnie zalogowanym użytkowniku
        /// </summary>
        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput
            {
                User = (await GetCurrentUserAsync()).MapTo<UserLoginInfoDto>()
            };

            if (AbpSession.TenantId.HasValue)
            {
                output.Tenant = (await GetCurrentTenantAsync()).MapTo<TenantLoginInfoDto>();
            }

            return output;
        }
    }
}