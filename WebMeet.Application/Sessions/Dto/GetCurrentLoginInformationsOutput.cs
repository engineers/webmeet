﻿using Abp.Application.Services.Dto;

namespace WebMeet.Sessions.Dto
{
    /// <summary>
    /// Informacje o zalogowanym użytkowniku
    /// </summary>
    public class GetCurrentLoginInformationsOutput : IOutputDto
    {
        public UserLoginInfoDto User { get; set; }

        public TenantLoginInfoDto Tenant { get; set; }
    }
}