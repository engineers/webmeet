﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using WebMeet.Users;

namespace WebMeet.Sessions.Dto
{
    /// <summary>
    /// Dane o zalogowanym użytkowniku
    /// </summary>
    [AutoMapFrom(typeof(User))]
    public class UserLoginInfoDto : EntityDto<long>
    {
        /// <summary>
        /// Imie
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Nazwisko
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Login
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Adres email
        /// </summary>
        public string EmailAddress { get; set; }
    }
}
