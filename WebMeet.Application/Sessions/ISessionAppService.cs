﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Sessions.Dto;

namespace WebMeet.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        /// <summary>
        /// Pobranie informacji o aktualnie zalogowanym użytkowniku
        /// </summary>
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
