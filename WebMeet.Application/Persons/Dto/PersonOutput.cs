﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Persons.Dto
{
    /// <summary>
    /// Wynik operacji wyszukiwania osób
    /// </summary>
    public class SearchPersonsOutput : PagedResultOutput<SearchPersonDto>
    {
    }

    /// <summary>
    /// Wynik operacji aktualizacji osoby
    /// </summary>
    public class UpdatePersonOutput : PersonDto, IOutputDto { }
}
