﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Shared;

namespace WebMeet.Persons.Dto
{
    /// <summary>
    /// Dane dla operacji wyszukiwania
    /// </summary>
    public class SearchPersonsInput : SearchInputBase
    {
        /// <summary>
        /// Nazwisko
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Login
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Czy jest znajomym
        /// </summary>
        public bool? IsFriend { get; set; }
    }

    /// <summary>
    /// Dane dal operacji aktualizacji dancyh osoby
    /// </summary>
    public class UpdatePersonInput : IInputDto
    {
        /// <summary>
        /// Max dłuogść imienia
        /// </summary>
        public const int MaxFirstNameLength = 100;
        /// <summary>
        /// Max długość nazwiska
        /// </summary>
        public const int MaxLastNameLength = 100;
        /// <summary>
        /// Max długość adresu
        /// </summary>
        public const int MaxAddressLength = 150;
        /// <summary>
        /// Max długość adresu email
        /// </summary>
        public const int MaxEmailAddressLength = 50;

        /// <summary>
        /// Imię
        /// </summary>
        [Required]
        [StringLength(MaxFirstNameLength)]
        public string FirstName { get; set; }
        /// <summary>
        /// Nazwisko
        /// </summary>
        [Required]
        [StringLength(MaxLastNameLength)]
        public string LastName { get; set; }
        /// <summary>
        /// Adres
        /// </summary>
        [StringLength(MaxAddressLength)]
        public string Address { get; set; }
        /// <summary>
        /// Adres email
        /// </summary>
        [Required]
        [StringLength(MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}
