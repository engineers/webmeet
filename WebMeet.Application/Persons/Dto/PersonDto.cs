﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WebMeet.Friends.Invitations;

namespace WebMeet.Persons.Dto
{
    /// <summary>
    /// Klasa reprezentująca DTO dla typu <see cref="Person"/>
    /// </summary>
    public class PersonDto : EntityDto<long>
    {
        /// <summary>
        /// Imię
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Nazwisko
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Adres
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Login
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Adres email
        /// </summary>
        public string EmailAddress { get; set; }     
    }

    /// <summary>
    /// Klasa reprezentująca osobę z operacji wyszukiwania
    /// </summary>
    public class SearchPersonDto : PersonDto
    {
        /// <summary>
        /// Czy osoba jest zaproszona do znajomości
        /// </summary>
        public bool IsInvited { get; set; }

        /// <summary>
        /// Czy osoba jest znajomym
        /// </summary>
        public bool IsFriend { get; set; }
    }
}
