﻿using System;
using System.Linq;
using System.Net.Configuration;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using AutoMapper;
using WebMeet.Core.Services.Persons;
using WebMeet.Core.Services.Users;
using WebMeet.Extensions;
using WebMeet.Friends.Acquaintances;
using WebMeet.Friends.Invitations;
using WebMeet.Helpers.Extensions;
using WebMeet.Persons.Dto;
using WebMeet.Shared;
using WebMeet.Users;

namespace WebMeet.Persons.Implementation
{
    /// <summary>
    /// Serwis odpowiedzialny za operacje na osobach
    /// </summary>
    [AbpAuthorize]
    public class PersonAppService :ApplicationBaseService, IPersonAppService
    {
        protected readonly IUserManager UserManager;
        protected readonly IPersonManager PersonManager;
        protected readonly IRepository<Invitation, long> InvitationRepository;
        protected readonly IRepository<Acquaintance, long> AcquaintanceRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="personRepository">Repozytorium osób</param>
        /// <param name="invitationRepository">Repozytorium zaproszeń</param>
        /// <param name="userManager">Menedżer osób</param>
        /// <param name="personManager">Serwis domenowy osób</param>
        /// <param name="acquaintanceRepository">Repozytorium znajomości</param>
        public PersonAppService(IRepository<Person, long> personRepository,
            IRepository<Invitation, long> invitationRepository,
            IUserManager userManager, 
            IPersonManager personManager,
            IRepository<Acquaintance, long> acquaintanceRepository) : base(personRepository)
        {
            InvitationRepository = invitationRepository;
            UserManager = userManager;
            PersonManager = personManager;
            AcquaintanceRepository = acquaintanceRepository;
        }

        /// <summary>
        /// Pobranie osób o określonych parametrach
        /// </summary>
        /// <param name="query">Model</param>
        public Dto.SearchPersonsOutput Get(Dto.SearchPersonsInput query)
        {           
            var queryable = PersonRepository.GetAll();
            queryable = queryable
                .FilterIfNotNullOrEmpty(query.LastName, person => person.LastName.StartsWith(query.LastName))
                .FilterIfNotNullOrEmpty(query.UserName, person => person.User.UserName.StartsWith(query.UserName))
                .FilterIfNotNull(query.IsFriend, person => query.IsFriend.Value ?
                    person.Acquaintances.Any(acq => acq.FirstPersonId == PersonId || acq.SecondPersonId == PersonId) :
                    person.Acquaintances.All(acq => acq.FirstPersonId != PersonId && acq.SecondPersonId != PersonId))
                .Where(person => person.Id > 2 && person.Id != PersonId);
            var count = queryable.Count();

            var items = queryable.FilterPage(query)
            .ToList()
            .Select(person => ConvertToDto(person, PersonId));

            return new SearchPersonsOutput
            {
                Items = items.ToList(),
                TotalCount = count
            };

        }

        /// <summary>
        /// Pobiera informacje o nas
        /// </summary>
        public async Task<PersonDto> GetMe()
        {
            var person = await PersonManager.GetAsync(PersonId);
            return person.MapTo<PersonDto>();
        }

        /// <summary>
        /// Aktualizuje informacje o nas
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Zaktualizowany model</returns>
        public async Task<UpdatePersonOutput> UpdateMe(UpdatePersonInput model)
        {
            var user = await UserManager.GetAsync(PersonId);
            var person = await PersonManager.GetAsync(PersonId);

            //1. update user properties
            await UpdateUser(user, model.EmailAddress);

            //2. update person
            await UpdatePerson(person, model.FirstName, model.LastName, model.Address);

            return new UpdatePersonOutput
            {
                Id = PersonId,
                UserName = user.UserName,
                EmailAddress = model.EmailAddress,
                Address = model.Address,
                FirstName = model.FirstName,
                LastName = model.LastName
            };
        }

        private async Task UpdateUser(User originalEntity, string email)
        {
            originalEntity.EmailAddress = email;
            await UserManager.Update(PersonId, originalEntity);
        }

        private async Task UpdatePerson(Person originalEntity, string firstName, string lastName, string address)
        {
            originalEntity.FirstName = firstName;
            originalEntity.LastName = lastName;
            originalEntity.Address = address;
            await PersonManager.Update(PersonId, originalEntity);
        }

        private SearchPersonDto ConvertToDto(Person person, long personId)
        {
            var dto = new SearchPersonDto
            {
                Address = person.Address,
                EmailAddress = person.User.EmailAddress,
                UserName = person.User.UserName,
                FirstName = person.FirstName,
                Id=person.Id,
                LastName = person.LastName
            };
            var invitation =
                InvitationRepository.FirstOrDefault(
                    i => ((i.ReceiverId == personId && i.SenderId == person.Id) || (i.ReceiverId == person.Id && i.SenderId == personId)) && i.State == InvitationState.Sent);

            var acquaintance =
                AcquaintanceRepository.FirstOrDefault(
                    a =>
                        (a.FirstPersonId == personId && a.SecondPersonId == person.Id) ||
                        (a.FirstPersonId == person.Id && a.SecondPersonId == personId));

            dto.IsInvited = invitation != null;
            dto.IsFriend = acquaintance != null;
            return dto;
        }
    }
}
