﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using WebMeet.Persons.Dto;

namespace WebMeet.Persons
{
    /// <summary>
    /// Interfejs definiujący operacje na osobach
    /// </summary>
    public interface IPersonAppService : IApplicationService
    {
        /// <summary>
        /// Pobranie osób o określonych parametrach
        /// </summary>
        /// <param name="query">Model</param>
        SearchPersonsOutput Get(SearchPersonsInput query);
        /// <summary>
        /// Pobiera informacje o nas
        /// </summary>
        Task<PersonDto> GetMe();
        /// <summary>
        /// Aktualizuje informacje o nas
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Zaktualizowany model</returns>
        Task<UpdatePersonOutput> UpdateMe(UpdatePersonInput model);
    }
}
