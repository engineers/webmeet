﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using WebMeet.Shared;

namespace WebMeet.Extensions
{
    public static class QueryExtensions
    {
        /// <summary>
        /// Metoda odpowiedzialna za paginację wyników
        /// </summary>
        /// <param name="collection">Kolekcja obiektów</param>
        /// <param name="pagingInfo">Dane do paginacji</param>
        /// <returns>Kolekcję zawierającą dane z przedziału paginacji</returns>
        public static IQueryable<T> FilterPage<T>(this IQueryable<T> collection, IPagingInfoModel pagingInfo)
            where T : FullAuditedEntity<long>
        {
            if (collection == null) throw new ArgumentNullException("collection");
            if (pagingInfo == null) throw new ArgumentNullException("pagingInfo");
            return pagingInfo.HasValidPagingInfo() ?
                collection.OrderByDescending(e => e.CreationTime).Skip(pagingInfo.SkipCount).Take(pagingInfo.MaxResultCount) :
                collection;
        }
    }
}
