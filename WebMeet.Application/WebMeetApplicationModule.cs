﻿using System.Linq;
using System.Reflection;
using Abp.Application.Features;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Modules;
using AutoMapper;
using Castle.MicroKernel.Registration;
using RestSharp;
using WebMeet.Account.Dto;
using WebMeet.Calendar;
using WebMeet.Calendar.Dto;
using WebMeet.Conversations;
using WebMeet.Conversations.Dto;
using WebMeet.Core.Services;
using WebMeet.Friends.Acquaintance.Dto;
using WebMeet.Friends.Acquaintances;
using WebMeet.Friends.Blacklist.Dto;
using WebMeet.Friends.Blacklists;
using WebMeet.Friends.Invitation.Dto;
using WebMeet.Friends.Invitations;
using WebMeet.Groups;
using WebMeet.Groups.Dto;
using WebMeet.Mail;
using WebMeet.Messages;
using WebMeet.Messages.Dto;
using WebMeet.Persons;
using WebMeet.Persons.Dto;
using WebMeet.Shared;
using WebMeet.Users;

namespace WebMeet
{
    /// <summary>
    /// Klasa reprezentująca moduł serwisów aplikacji
    /// </summary>
    [DependsOn(typeof(WebMeetCoreServicesModule), typeof(AbpAutoMapperModule))]
    public class WebMeetApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Register();
            ConfigureMapper();
        }

        protected void Register()
        {
            IocManager.Register<IMailSerivce, GunMailService>(DependencyLifeStyle.Transient);
            IocManager.IocContainer.Register(
                Component.For<GunMailServiceOptions>()
                    .UsingFactoryMethod(GunMailServiceOptions.FromAppConfig));
            IocManager.Register<IRestClient, RestClient>(DependencyLifeStyle.Transient);
        }

        protected void ConfigureMapper()
        {
            ConfigureMapperPersonArea();
            ConfigureMapperInvitationArea();
            ConfigureMapperBlacklistArea();
            ConfigureMapperCalendarArea();
            ConfigureMapperGroupArea();
            ConfigureMapperMessageArea();
            ConfigureConversationArea();
        }

        private void ConfigureMapperPersonArea()
        {
            Mapper.CreateMap<RegistrationInput, User>();
            //    .ForMember(user => user.Persons.FirstOrDefault(), opt => opt.MapFrom(src => src));
            Mapper.CreateMap<RegistrationInput, Person>()
                .ForMember(person => person.FirstName, opt => opt.MapFrom(src => src.Name))
                .ForMember(person => person.LastName, opt => opt.MapFrom(src => src.Surname))
                .ForMember(person => person.User, opt => opt.MapFrom(src => src));

            Mapper.CreateMap<Person, PersonDto>()
                .ForMember(dto => dto.EmailAddress, opt => opt.MapFrom(src => src.User.EmailAddress))
                .ForMember(dto => dto.UserName, opt => opt.MapFrom(src => src.User.UserName));
        }

        private void ConfigureMapperInvitationArea()
        {
            Mapper.CreateMap<Invitation, InvitationDto>()
                .ForMember(dto => dto.SenderName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Sender.FirstName ,src.Sender.LastName)))
                .ForMember(dto => dto.ReceiverName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Receiver.FirstName, src.Receiver.LastName)));
            Mapper.CreateMap<Invitation, UpdateInvitationOutput>();
            Mapper.CreateMap<Invitation, CreateInvitationOutput>();
        }

        private void ConfigureMapperBlacklistArea()
        {
            Mapper.CreateMap<BlacklistItem, BlacklistItemDto>()
                .ForMember(dto => dto.OwnerName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Owner.FirstName, src.Owner.LastName)))
                .ForMember(dto => dto.PersonName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Person.FirstName, src.Person.LastName)));

            Mapper.CreateMap<BlacklistItem, CreateBlacklistItemOutput>()
                .ForMember(dto => dto.OwnerName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Owner.FirstName, src.Owner.LastName)))
                .ForMember(dto => dto.PersonName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Person.FirstName, src.Person.LastName)));
        }

        private void ConfigureMapperCalendarArea()
        {
            Mapper.CreateMap<Event, EventDto>();
            Mapper.CreateMap<Event, UpdateEventOutput>();
            Mapper.CreateMap<Event, CreateEventOutput>();
        }

        private void ConfigureMapperGroupArea()
        {
            Mapper.CreateMap<Group, GroupDto>()
                .ForMember(dto => dto.Persons, opt => opt.MapFrom(src => src.GroupUsers.Select(gu => new IdValueDto<long>
                {
                    Id = gu.PersonId,
                    Value = string.Format("{0} {1}", gu.Person.FirstName, gu.Person.LastName)
                })));

            Mapper.CreateMap<Group, UpdateGroupOutput>();
            Mapper.CreateMap<Group, CreateGroupOutput>();
        }

        private void ConfigureMapperMessageArea()
        {
            Mapper.CreateMap<Message, MessageDto>();
            Mapper.CreateMap<Message, CreateMessageOutput>();
        }

        private void ConfigureConversationArea()
        {
            Mapper.CreateMap<Conversation, ConversationDto>()
                .ForMember(dto => dto.CallerName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Caller.FirstName, src.Caller.LastName)));
            Mapper.CreateMap<Conversation, CreateConversationOutput>()
                .ForMember(dto => dto.CallerName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Caller.FirstName, src.Caller.LastName)));
            Mapper.CreateMap<Conversation, UpdateConversationOutput>()
                .ForMember(dto => dto.CallerName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Caller.FirstName, src.Caller.LastName)));
        }
    }
}
