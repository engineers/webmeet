﻿using System;
using System.Threading.Tasks;
using WebMeet.Core.Services.Mail;

namespace WebMeet.Mail
{
    public interface IMailSerivce
    {
        /// <summary>
        ///     Sends message according to it's properties.
        /// </summary>
        /// <param name="message">Message to be sent.</param>
        /// <returns>Result of the operation.</returns>
        void SendMail(MailMessage message);

        /// <summary>
        ///     Sends message according to it's properties.
        /// </summary>
        /// <param name="message">Message to be sent.</param>
        /// <returns>Result of the operation.</returns>
        Task SendMailAsync(MailMessage message);

        /// <summary>
        ///     Sends message according to it's properties, using late-building. The provider function is not invoked if
        ///     preparation steps fail.
        /// </summary>
        /// <param name="messageProvider">Message providing function.</param>
        /// <returns>Result of the operation.</returns>
        void SendMail(Func<MailMessage> messageProvider);

        /// <summary>
        ///     Sends message according to it's properties, using late-building. The provider function is not invoked if
        ///     preparation steps fail.
        /// </summary>
        /// <param name="messageProvider">Message providing function.</param>
        /// <returns>Result of the operation.</returns>
        Task SendMailAsync(Func<MailMessage> messageProvider);
    }
}
