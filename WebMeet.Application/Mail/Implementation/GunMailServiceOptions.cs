﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMeet.Mail
{
    public class GunMailServiceOptions
    {
        private const string MailBaseUrlConfigKey = "MailBaseUrl";
        private const string MailFromConfigKey = "MailFrom";
        private const string MailApiKeyConfigKey = "MailApiKey";
        private const string MailDomainConfigKey = "MailDomain";

        /// <summary>
        ///     Base Url of mailing API.
        /// </summary>
        public string MailBaseUrl { get; set; }

        /// <summary>
        ///     Default 'From' mail header.
        /// </summary>
        public string MailFrom { get; set; }

        /// <summary>
        ///     GunMail API key.
        /// </summary>
        public string MailApiKey { get; set; }

        /// <summary>
        ///     Mail server domain.
        /// </summary>
        public string MailDomain { get; set; }

        /// <summary>
        ///     Creates new options from App settings using <see cref="ConfigurationManager" />.
        /// </summary>
        /// <returns></returns>
        public static GunMailServiceOptions FromAppConfig()
        {
            var appSettings = ConfigurationManager.AppSettings;
            var options = new GunMailServiceOptions
            {
                MailApiKey = appSettings[MailApiKeyConfigKey],
                MailBaseUrl = appSettings[MailBaseUrlConfigKey],
                MailDomain = appSettings[MailDomainConfigKey],
                MailFrom = appSettings[MailFromConfigKey]
            };
            return options;
        }
    }
}
