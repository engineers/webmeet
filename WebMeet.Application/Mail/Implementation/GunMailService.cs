﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Nito.AsyncEx;
using RestSharp;
using RestSharp.Authenticators;
using WebMeet.Core.Services.Mail;


namespace WebMeet.Mail
{
    public class GunMailService : IMailSerivce
    {
        public GunMailService(IRestClient client, GunMailServiceOptions options)
        {
            if (options == null) throw new ArgumentNullException("options");
            if (client == null) throw new ArgumentNullException("client");
            Client = client;
            Options = options;
        }

        public IRestClient Client { get; private set; }
        public GunMailServiceOptions Options { get; private set; }

        public void SendMail(MailMessage message)
        {
            if (message == null) throw new ArgumentNullException("message");
            SendMail(() => message);
        }

        public Task SendMailAsync(MailMessage message)
        {
            if (message == null) throw new ArgumentNullException("message");
            return SendMailAsync(() => message);
        }

        public void SendMail(Func<MailMessage> messageProvider)
        {
            if (messageProvider == null) throw new ArgumentNullException("messageProvider");
            SendCore(messageProvider);
        }

        public Task SendMailAsync(Func<MailMessage> messageProvider)
        {
            if (messageProvider == null) throw new ArgumentNullException("messageProvider");
            return SendCoreAsync(messageProvider);
        }

        protected virtual Task SendCoreAsync(Func<MailMessage> messageProvider)
        {
            var client = CreateClient();
            var reqest = CreateRequest();
            var message = messageProvider.Invoke();
            if (message == null) throw new ArgumentException("Null message provided.", "messageProvider");
            AddMessageDetailsToRequest(reqest, message);

            var tcs=new TaskCompletionSource();
            client.ExecuteAsync(reqest, response =>
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    tcs.SetException(new WebException(string.Format("Failed to send message. Response: {0}", response.Content)));
                }
                tcs.SetResult();
            });
            return tcs.Task;
        }

        protected virtual void SendCore(Func<MailMessage> messageProvider)
        {
            var client = CreateClient();
            var reqest = CreateRequest();
            var message = messageProvider.Invoke();
            if (message == null) throw new ArgumentException("Null message provided.", "messageProvider");
            AddMessageDetailsToRequest(reqest, message);

            var response = client.Execute(reqest);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new WebException(string.Format("Failed to send message. Response: {0}", response.Content));
            }
        }

        protected virtual void AddMessageDetailsToRequest(RestRequest request, MailMessage message)
        {
            if (request == null) throw new ArgumentNullException("request");
            if (message == null) throw new ArgumentNullException("message");
            if (message.Recipients == null || !message.Recipients.Any())
            {
                throw new ArgumentException("Message has no recipients.", "message");
            }
            foreach (var recipient in message.Recipients)
            {
                request.AddParameter("to", recipient);
            }
            request.AddParameter("subject", message.Subject);
            if (!string.IsNullOrWhiteSpace(message.Body))
            {
                request.AddParameter("text", message.Body);
            }
            if (!string.IsNullOrWhiteSpace(message.HtmlBody))
            {
                request.AddParameter("html", message.HtmlBody);
            }
        }

        protected virtual IRestClient CreateClient()
        {
            var apiKey = Options.MailApiKey;
            var baseUrl = Options.MailBaseUrl;
            if (string.IsNullOrWhiteSpace(apiKey)) throw new ArgumentException("apiKey");
            if (string.IsNullOrWhiteSpace(baseUrl)) throw new ArgumentException("baseUrl");
            var baseUri = new Uri(baseUrl);
            Client.Authenticator = new HttpBasicAuthenticator("api", apiKey);
            Client.BaseUrl = baseUri;
            return Client;
        }

        protected virtual RestRequest CreateRequest()
        {
            var domain = Options.MailDomain;
            var from = Options.MailFrom;
            if (string.IsNullOrWhiteSpace(domain)) throw new ArgumentException("domain");
            var request = new RestRequest();
            request.AddParameter("from", from);
            request.AddUrlSegment("domain", domain);
            request.Resource = "{domain}/messages";
            request.Method = Method.POST;
            return request;
        }
    }
}
