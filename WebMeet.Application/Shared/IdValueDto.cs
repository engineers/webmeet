﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMeet.Shared
{
    /// <summary>
    /// Reprezentacja obiektu w postaci Id,Value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class IdValueDto<T>
    {
        /// <summary>
        /// Id obiektu
        /// </summary>
        public T Id { get; set; }
        /// <summary>
        /// Wartość obiektu
        /// </summary>
        public string Value { get; set; }
    }
}
