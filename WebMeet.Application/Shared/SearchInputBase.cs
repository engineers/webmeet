﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Shared
{
    /// <summary>
    /// Klasa bazowa dla operacji wyszukiwania
    /// </summary>
    public class SearchInputBase : PagingInfoModel, IInputDto
    {
    }
}
