﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace WebMeet.Shared
{
    /// <summary>
    /// Interfejs do paginacji
    /// </summary>
    public interface IPagingInfoModel : IPagedResultRequest
    {
        bool HasValidPagingInfo();
    }

    /// <summary>
    /// Klasa zawierająca dane do paginacji
    /// </summary>
    public class PagingInfoModel : IPagingInfoModel
    {
        /// <summary>
        /// Ile elementów ominąć
        /// </summary>
        public int SkipCount { get; set; }

        /// <summary>
        /// Ile elementów pobrać
        /// </summary>
        public int MaxResultCount { get; set; }

        /// <summary>
        /// Określa czy parametry paginacji są prawidłowe
        /// </summary>
        /// <returns></returns>
        public bool HasValidPagingInfo()
        {
            if (SkipCount == 0 && MaxResultCount == 0)
                return false;
            return true;
        }
    }
}
