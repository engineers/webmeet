﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using WebMeet.Core.Services;
using WebMeet.Persons;

namespace WebMeet.Shared
{
    /// <summary>
    /// Klasa bazowa dla serwisów aplikacji
    /// </summary>
    public class ApplicationBaseService : ApplicationService
    {
        private long? _personId = null;
        protected readonly IRepository<Person, long> PersonRepository;
        /// <summary>
        /// Konstrukotr
        /// </summary>
        /// <param name="personRepository">Repozytorium osób</param>
        public ApplicationBaseService(IRepository<Person, long> personRepository)
        {
            PersonRepository = personRepository;
            LocalizationSourceName = WebMeetConsts.LocalizationSourceName;
        }

        /// <summary>
        /// Id zalogowanej osoby
        /// </summary>
        protected long PersonId
        {
            get
            {
                if (!_personId.HasValue)
                    _personId = GetLoggedPerson().Id;
                return _personId.Value;
            }
        }

        /// <summary>
        /// Pobranie zalogowanej osoby
        /// </summary>
        protected Person GetLoggedPerson()
        {
            var person = GetLoggedNullablePerson();
            if (person == null)
                throw new UserFriendlyException(L("ErrorDetected"), L("UserNotAssociatedWithAnyPerson"));
            return person;
        }

        private Person GetLoggedNullablePerson()
        {
            var userId = AbpSession.GetUserId();
            return PersonRepository.FirstOrDefault(p => p.UserId == userId);
        }
    }
}
