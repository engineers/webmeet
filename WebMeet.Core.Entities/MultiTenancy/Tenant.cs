﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.MultiTenancy;
using WebMeet.Users;

namespace WebMeet.MultiTenancy
{
    [Table("Tenants")]
    public class Tenant : AbpTenant<Tenant, User>
    {

    }
}