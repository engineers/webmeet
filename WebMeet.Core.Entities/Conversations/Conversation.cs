﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using WebMeet.Persons;

namespace WebMeet.Conversations
{
    /// <summary>
    /// Klasa reprezentująca rozmowę
    /// </summary>
    public class Conversation : FullAuditedEntity<long>
    {
        /// <summary>
        /// Id osoby która rozpoczęła rozmowę
        /// </summary>
        public virtual long CallerId { get; set; }
        /// <summary>
        /// Id osoby/grupy do której rozmowa była skierowana
        /// </summary>
        public virtual long PersonBaseId { get; set; }
        /// <summary>
        /// Czas rozpoczęcia rozmowy
        /// </summary>
        [Required]
        public virtual DateTime StartDate { get; set; }
        /// <summary>
        /// Czas zakończenia rozmowy
        /// </summary>
        public virtual DateTime? EndDate { get; set; }

        /// <summary>
        /// Osoba/grupa do której rozmowa była skierowana
        /// </summary>
        [ForeignKey("PersonBaseId")]
        public virtual PersonBase PersonBase { get; set; }
        /// <summary>
        /// Osoba która rozpoczęła rozmowę
        /// </summary>
        [ForeignKey("CallerId")]
        public virtual Person Caller { get; set; }
    }
}