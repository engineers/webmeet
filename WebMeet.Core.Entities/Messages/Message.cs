﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using WebMeet.Persons;

namespace WebMeet.Messages
{
    /// <summary>
    /// Klasa reprezentująca wiadomość
    /// </summary>
    public class Message : FullAuditedEntity<long>
    {
        /// <summary>
        /// Max długość zawartości wiadomości
        /// </summary>
        public const int MaxContentLength = 300;

        /// <summary>
        /// Id nadawcy wiadomości
        /// </summary>
        public virtual long SenderId { get; set; }
        /// <summary>
        /// Id osoby/grupy do której wiadomość została wysłana
        /// </summary>
        public virtual long ReceiverId { get; set; }
        /// <summary>
        /// Treść wiadomości
        /// </summary>
        [Required]
        [StringLength(MaxContentLength)]
        public virtual string Content { get; set; }

        /// <summary>
        /// Nadawca wiadomości
        /// </summary>
        [ForeignKey("SenderId")]
        public virtual Person Sender { get; set; }
        /// <summary>
        /// Odbiorca wiadomości (osoba/grupa)
        /// </summary>
        [ForeignKey("ReceiverId")]
        public virtual PersonBase Receiver { get; set; }
    }
}
