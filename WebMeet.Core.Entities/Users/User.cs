﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Authorization.Users;
using Abp.Extensions;
using WebMeet.MultiTenancy;
using WebMeet.Persons;

namespace WebMeet.Users
{
    /// <summary>
    /// Klasa reprezentująca użytkownika
    /// </summary>
    [Table("User")]
    public class User : AbpUser<Tenant, User>
    {      
        /// <summary>
        /// Osoby powiązane z użytkownikiem
        /// </summary>
        public virtual ICollection<Person> Persons { get; set; }

        /// <summary>
        /// Metoda generująca losowe hasło
        /// </summary>
        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }
    }
}