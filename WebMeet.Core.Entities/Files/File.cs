﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using WebMeet.Files.Shared;
using WebMeet.Persons;

namespace WebMeet.Files
{
    /// <summary>
    /// Klasa reprezentująca plik
    /// </summary>
    public class File : FullAuditedEntity<long>
    {
        /// <summary>
        /// Max długość nazwy pliku
        /// </summary>
        public const int MaxNameLength = 100;

        /// <summary>
        /// Id właściciela pliku (osoba lub grupa)
        /// </summary>
        public virtual long PersonBaseId { get; set; }
        /// <summary>
        /// Nazwa pliku
        /// </summary>
        [Required]
        [StringLength(MaxNameLength)]
        public virtual string Name { get; set; }

        /// <summary>
        /// Właściciel pliku (osoba lub grupa)
        /// </summary>
        [ForeignKey("PersonBaseId")]
        public virtual PersonBase PersonBase { get; set; }
        /// <summary>
        /// Lista udostępnień tego pliku innym osobom
        /// </summary>
        public virtual ICollection<SharedFile> SharedFiles { get; set; } 
    }
}