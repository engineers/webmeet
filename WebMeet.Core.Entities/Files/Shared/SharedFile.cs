﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using WebMeet.Persons;

namespace WebMeet.Files.Shared
{
    /// <summary>
    /// Klasa reprezentująca udostępniony plik
    /// </summary>
    public class SharedFile : FullAuditedEntity<long>
    {
        /// <summary>
        /// Id udostepnionego pliku
        /// </summary>
        public virtual long FileId { get; set; }
        /// <summary>
        /// Id osoby której plik został udostępniony
        /// </summary>
        public virtual long PersonId { get; set; }

        /// <summary>
        /// Osoba której plik został udostępniony
        /// </summary>
        [ForeignKey("PersonId")]
        public virtual Person Person { get; set; }

        /// <summary>
        /// Udostępniony plik
        /// </summary>
        [ForeignKey("FileId")]
        public virtual File File { get; set; }
    }
}
