﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using WebMeet.Persons;

namespace WebMeet.Calendar
{
    /// <summary>
    /// Klasa reprezentująca wydarzenie w czasie
    /// </summary>
    public class Event : FullAuditedEntity<long>
    {
        /// <summary>
        /// Max długość opisu
        /// </summary>
        public const int MaxDescriptionLength = 500;
        /// <summary>
        /// Max długość tytułu
        /// </summary>
        public const int MaxTitleLength = 100;

        /// <summary>
        /// Id osoby lub grupy do ktorej wydarzenie należy
        /// </summary>
        public virtual long PersonBaseId { get; set; }
        /// <summary>
        /// Data wydarzenia
        /// </summary>
        [Required]
        public virtual DateTime Date { get; set; }
        /// <summary>
        /// Tytuł wydarzenia
        /// </summary>
        [Required]
        [StringLength(MaxTitleLength)]
        public virtual string Title { get; set; }
        /// <summary>
        /// Opis wydarzenia
        /// </summary>
        [Required]
        [StringLength(MaxDescriptionLength)]
        public virtual string Description { get; set; }

        /// <summary>
        /// Osoba lub grupa do ktorej wydarzenie należy
        /// </summary>
        [ForeignKey("PersonBaseId")]
        public virtual PersonBase PersonBase { get; set; }
    }
}
