﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using WebMeet.Persons;

namespace WebMeet.Friends.Blacklists
{
    /// <summary>
    /// Klasa reprezentująca czarną listę
    /// </summary>
    public class BlacklistItem : FullAuditedEntity<long>
    {
        /// <summary>
        /// Id właściciela czarnej listy
        /// </summary>
        public virtual long OwnerId { get; set; }
        /// <summary>
        /// Id osoby na czarnej liście
        /// </summary>
        public virtual long PersonId { get; set; }

        /// <summary>
        /// Właściciel czarnej listy
        /// </summary>
        [ForeignKey("OwnerId")]
        public virtual Person Owner { get; set; }
        /// <summary>
        /// Osoba na czarnej liście
        /// </summary>
        [ForeignKey("PersonId")]
        public virtual Person Person { get; set; }
    }
}
