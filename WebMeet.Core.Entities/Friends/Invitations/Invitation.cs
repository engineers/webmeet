﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using WebMeet.Persons;

namespace WebMeet.Friends.Invitations
{
    /// <summary>
    /// Klasa reprezentująca zaproszenie do znajomości
    /// </summary>
    public class Invitation : FullAuditedEntity<long>
    {
        /// <summary>
        /// Id osoby zapraszającej do znajomości
        /// </summary>
        public virtual long SenderId { get; set; }
        /// <summary>
        /// Id osoby zapraszanej do znajomości
        /// </summary>
        public virtual long ReceiverId { get; set; }
        /// <summary>
        /// Status zaproszenia
        /// </summary>
        public virtual InvitationState State { get; set; }

        /// <summary>
        /// Osoba zapraszająca do znajomości
        /// </summary>
        [ForeignKey("SenderId")]
        public virtual Person Sender { get; set; }
        /// <summary>
        /// Osoba zapraszana do znajomości
        /// </summary>
        [ForeignKey("ReceiverId")]
        public virtual Person Receiver { get; set; }

        #region Factory methods

        /// <summary>
        /// Metoda tworząca zaproszenie pomiędzy osobami z statusem 'Sent'
        /// </summary>
        /// <param name="senderId"></param>
        /// <param name="receiverId"></param>
        /// <returns></returns>
        public static Invitation CreateSentInvitation(long senderId, long receiverId)
        {
            return new Invitation
            {
                SenderId = senderId,
                ReceiverId = receiverId,
                State = InvitationState.Sent
            };
        }
        #endregion
    }
}
