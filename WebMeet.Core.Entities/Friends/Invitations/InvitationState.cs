﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebMeet.Friends.Invitations
{
    /// <summary>
    /// Mozliwe statusy zaproszenia
    /// </summary>
    public enum InvitationState
    {
        /// <summary>
        /// Wysłane
        /// </summary>
        Sent,

        /// <summary>
        /// Zaakceptowane
        /// </summary>
        Accepted,

        /// <summary>
        /// Odrzucone
        /// </summary>
        Rejected,

        /// <summary>
        /// Anulowane
        /// </summary>
        Canceled
    }
}
