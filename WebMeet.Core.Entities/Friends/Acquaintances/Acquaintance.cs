﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using WebMeet.Persons;

namespace WebMeet.Friends.Acquaintances
{
    /// <summary>
    /// Klasa reprezentująca znajomość
    /// </summary>
    public class Acquaintance : FullAuditedEntity<long>
    {
        /// <summary>
        /// Id pierwszej osoby
        /// </summary>
        public virtual long FirstPersonId { get; set; }
        /// <summary>
        /// Id drugiej osoby
        /// </summary>
        public virtual long SecondPersonId { get; set; }

        /// <summary>
        /// Pierwsza osoba
        /// </summary>
        [ForeignKey("FirstPersonId")]
        public virtual Person FirstPerson { get; set; }
        /// <summary>
        /// Druga osoba
        /// </summary>
        [ForeignKey("SecondPersonId")]
        public virtual Person SecondPerson { get; set; }
    }
}
