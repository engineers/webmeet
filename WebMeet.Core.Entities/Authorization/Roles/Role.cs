﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Authorization.Roles;
using WebMeet.MultiTenancy;
using WebMeet.Users;

namespace WebMeet.Authorization.Roles
{
    /// <summary>
    /// Klasa reprezentująca role
    /// </summary>
    [Table("Roles")]
    public class Role : AbpRole<Tenant, User>
    {

    }
}