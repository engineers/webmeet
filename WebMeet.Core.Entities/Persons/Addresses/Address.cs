﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace WebMeet.Persons.Addresses
{
    /// <summary>
    /// Klasa reprezentująca adres
    /// </summary>
    public class Address : FullAuditedEntity<long>
    {
        /// <summary>
        /// Max dlugość kraju
        /// </summary>
        public const int MaxCountryLength = 30;
        /// <summary>
        /// Max dlugość miasta
        /// </summary>
        public const int MaxCityLength = 50;
        /// <summary>
        /// Max dlugość ulicy
        /// </summary>
        public const int MaxStreetLength = 50;
        /// <summary>
        /// Max dlugość numeru domu
        /// </summary>
        public const int MaxNumberLength = 10;
        /// <summary>
        /// Max dlugość kodu pocztowego
        /// </summary>
        public const int MaxZipCodeLength = 10;

        /// <summary>
        /// Kraj
        /// </summary>
        [Required]
        [StringLength(MaxCountryLength)]
        public virtual string Country { get; set; }
        /// <summary>
        /// Miasto
        /// </summary>
        [Required]
        [StringLength(MaxCityLength)]
        public virtual string City { get; set; }
        /// <summary>
        /// Ulica
        /// </summary>
        [Required]
        [StringLength(MaxStreetLength)]
        public virtual string Street { get; set; }
        /// <summary>
        /// Numer
        /// </summary>
        [Required]
        [StringLength(MaxNumberLength)]
        public virtual string Number { get; set; }
        /// <summary>
        /// Kod pocztowy
        /// </summary>
        [Required]
        [StringLength(MaxZipCodeLength)]
        public virtual string ZipCode { get; set; }
        /// <summary>
        /// Osoby mające ten adres
        /// </summary>
        public virtual ICollection<Person> Persons { get; set; } 
    }
}