﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using WebMeet.Calendar;
using WebMeet.Conversations;
using WebMeet.Files;
using WebMeet.Messages;

namespace WebMeet.Persons
{
    /// <summary>
    /// Bazwoa klasa dla osobygrupy/gościa
    /// </summary>
    public abstract class PersonBase : FullAuditedEntity<long>
    {
        /// <summary>
        /// Avatar
        /// </summary>
        public virtual string Avatar { get; set; }
        /// <summary>
        /// Lista rozmów osoby/grupy
        /// </summary>
        public virtual ICollection<Conversation> Conversations { get; set; }
        /// <summary>
        /// Otrzymane wiadomości
        /// </summary>
        public virtual ICollection<Message> ReceivedMessages { get; set; }
        /// <summary>
        /// Wydarzenia osoby/grupy
        /// </summary>
        public virtual ICollection<Event> Events { get; set; }
        /// <summary>
        /// Pliki osoby/grupy
        /// </summary>
        public virtual ICollection<File> Files { get; set; } 
    }
}