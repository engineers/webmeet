﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using WebMeet.Conversations;
using WebMeet.Files.Shared;
using WebMeet.Friends;
using WebMeet.Friends.Acquaintances;
using WebMeet.Friends.Blacklists;
using WebMeet.Friends.Invitations;
using WebMeet.Groups;
using WebMeet.Messages;
using WebMeet.Persons.Addresses;
using WebMeet.Users;

namespace WebMeet.Persons
{
    /// <summary>
    /// Klasa reprezentująca osobę
    /// </summary>
    [Table("Persons")]
    public class Person : PersonBase
    {
        /// <summary>
        /// Max długość imienia
        /// </summary>
        public const int MaxFirstNameLength = 100;
        /// <summary>
        /// Max długość nazwiska
        /// </summary>
        public const int MaxLastNameLength = 100;
        /// <summary>
        /// Max długość adresu
        /// </summary>
        public const int MaxAddressLength = 150;

        /// <summary>
        /// Imię osoby
        /// </summary>
        [Required]
        [StringLength(MaxFirstNameLength)]
        public virtual string FirstName { get; set; }
        /// <summary>
        /// Nazwisko osoby
        /// </summary>
        [Required]
        [StringLength(MaxLastNameLength)]
        public virtual string LastName { get; set; }
        /// <summary>
        /// Adres osoby
        /// </summary>
        [StringLength(MaxAddressLength)]
        public virtual string Address { get; set; }

        [Required]
        public virtual long UserId { get; set; }

        /// <summary>
        /// Użytkownik przypisany do osoby
        /// </summary>
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        /// <summary>
        /// Lista grup do których należy
        /// </summary>
        public virtual ICollection<GroupUser> UserGroups { get; set; }
        /// <summary>
        /// Znajomości
        /// </summary>
        public virtual ICollection<Acquaintance> Acquaintances { get; set; }
        /// <summary>
        /// Wysłane zaproszenia
        /// </summary>
        public virtual ICollection<Invitation> SentInvitations { get; set; }
        /// <summary>
        /// Odebrane zaproszenia do znajomości
        /// </summary>
        public virtual ICollection<Invitation> ReceivedInvitations { get; set; }
        /// <summary>
        /// Wysłane wiadomości
        /// </summary>
        public virtual ICollection<Message> SentMessages { get; set; }
        /// <summary>
        /// Udostępnione pliku
        /// </summary>
        public virtual ICollection<SharedFile> SharedFiles { get; set; }
        /// <summary>
        /// Czarna lista
        /// </summary>
        public virtual ICollection<BlacklistItem> BlacklistItems { get; set; }
        /// <summary>
        /// Czarne listy na których znajduję się osoba
        /// </summary>
        public virtual ICollection<BlacklistItem> Blacklists { get; set; }
        /// <summary>
        /// Przeprowadzone rozmowy
        /// </summary>
        public virtual ICollection<Conversation> CallerConversations { get; set; } 
    }
}
