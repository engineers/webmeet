﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using WebMeet.Conversations;

namespace WebMeet.Persons.Guests
{
    /// <summary>
    /// Klasa reprezentująca gościa
    /// </summary>
    [Table("Guests")]
    public class Guest : PersonBase
    {
        /// <summary>
        /// Token dostepu do systemu
        /// </summary>
        public Guid Token { get; set; }
        /// <summary>
        /// Data dołączenia
        /// </summary>
        public DateTime? ConnectedDate { get; set; }
        /// <summary>
        /// Id rozmowy do ktorej ma dosep
        /// </summary>
        public long ConversationId { get; set; }

        /// <summary>
        /// Udostepniona rozmowa
        /// </summary>
        [ForeignKey("ConversationId")]
        public virtual Conversation Conversation { get; set; }
    }
}