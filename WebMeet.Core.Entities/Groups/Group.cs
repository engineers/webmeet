﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebMeet.Persons;

namespace WebMeet.Groups
{
    /// <summary>
    /// Klasa reprezentująca grupę
    /// </summary>
    [Table("Groups")]
    public class Group : PersonBase
    {
        /// <summary>
        /// Max dlugość nazwy
        /// </summary>
        public const int MaxNameLength = 100;

        /// <summary>
        /// Nazwa grupy
        /// </summary>
        [Required]
        [StringLength(MaxNameLength)]
        public virtual string Name { get; set; }
        /// <summary>
        /// Lista osób w grupie
        /// </summary>
        public virtual ICollection<GroupUser> GroupUsers { get; set; } 
    }
}