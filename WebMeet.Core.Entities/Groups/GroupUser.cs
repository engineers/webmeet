﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using WebMeet.Persons;

namespace WebMeet.Groups
{
    /// <summary>
    /// Klasa reprezentująca przypisanie osoby do grupy
    /// </summary>
    public class GroupUser : FullAuditedEntity<long>
    {
        /// <summary>
        /// Id grupy
        /// </summary>
        public virtual long GroupId { get; set; }
        /// <summary>
        /// Id osoby
        /// </summary>
        public virtual long PersonId { get; set; }

        /// <summary>
        /// Grupa
        /// </summary>
        [ForeignKey("GroupId")]
        public virtual Group Group { get; set; }
        /// <summary>
        /// Osoba
        /// </summary>
        [ForeignKey("PersonId")]
        public virtual Person Person { get; set; }
    }
}
