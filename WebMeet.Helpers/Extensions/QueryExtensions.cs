﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WebMeet.Helpers.Extensions
{
    /// <summary>
    /// Klasa zawierająca metody rozszerzające dla kolekcji IQueryable
    /// </summary>
    public static class QueryExtensions
    {
        /// <summary>
        /// Metoda filtrująca kolekcję jeśli parametr nie jest pusty
        /// </summary>
        /// <param name="collection">Kolekcja</param>
        /// <param name="check">Parametr do sprawdzenie</param>
        /// <param name="predicate">Predykat sprawdzany dla kazdego obiektu w kolekcji</param>
        /// <returns>Zwraca kolekcję obiektów dla których predykat jest prawdziwy</returns>
        public static IQueryable<T> FilterIfNotNull<T, TNullable>(this IQueryable<T> collection, TNullable? check,
            Expression<Func<T, bool>> predicate) where TNullable : struct
        {
            if (collection == null) throw new ArgumentNullException("collection");
            return check == null
                ? collection
                : collection.Where(predicate);
        }

        /// <summary>
        /// Metoda filtrująca kolekcję jeśli parametr nie jest pusty
        /// </summary>
        /// <param name="collection">Kolekcja</param>
        /// <param name="check">Parametr do sprawdzenie</param>
        /// <param name="predicate">Predykat sprawdzany dla kazdego obiektu w kolekcji</param>
        /// <returns>Zwraca kolekcję obiektów dla których predykat jest prawdziwy</returns>
        public static IQueryable<T> FilterIfNotNullOrEmpty<T>(this IQueryable<T> collection, string check,
            Expression<Func<T, bool>> predicate)
        {
            if (collection == null) throw new ArgumentNullException("collection");
            return string.IsNullOrWhiteSpace(check)
                ? collection
                : collection.Where(predicate);
        }

        /// <summary>
        /// Metoda filtrująca kolekcję jeśli parametr jest ustawiony na true
        /// </summary>
        /// <param name="collection">Kolekcja</param>
        /// <param name="check">Parametr do sprawdzenie</param>
        /// <param name="predicate">Predykat sprawdzany dla kazdego obiektu w kolekcji</param>
        /// <returns>Zwraca kolekcję obiektów dla których predykat jest prawdziwy</returns>
        public static IQueryable<T> FilterIfTrue<T>(this IQueryable<T> collection, bool check,
            Expression<Func<T, bool>> predicate)
        {
            if (collection == null) throw new ArgumentNullException("collection");
            return check
                ? collection.Where(predicate)
                : collection;
        }
    }
}
