﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMeet.Helpers.Extensions
{
    /// <summary>
    /// Klasa zawierająca metody rozszerzające dla napisów
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Metoda łączy kolekcję napisów w jeden napis, składowe oddzielone są przecinkiem
        /// </summary>
        public static string Join(this IEnumerable<string> strings)
        {
            return string.Join(", ", strings);
        }
    }
}
