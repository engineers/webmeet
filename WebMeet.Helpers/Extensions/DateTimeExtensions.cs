﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;

namespace WebMeet.Helpers.Extensions
{
    /// <summary>
    /// Klasa zawierająca metody rozszerzające dla DateTime
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Metoda zwracająca date reprezentującą początek miesiąca przekazanej daty
        /// </summary>
        public static DateTime GetMonthStart(this DateTime dateTime)
        {
            //Condition.Requires(dateTime).IsNotNull();
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        /// <summary>
        /// Metoda zwracająca date reprezentującą koniec miesiąca przekazanej daty
        /// </summary>
        public static DateTime GetMonthEnd(this DateTime dateTime)
        {
            //sCondition.Requires(dateTime).IsNotNull();
            var daysInMonth = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
            return new DateTime(dateTime.Year, dateTime.Month, daysInMonth, 23, 59, 59);
        }
    }
}
