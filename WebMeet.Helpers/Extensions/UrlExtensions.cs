﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace WebMeet.Helpers.Extensions
{
    /// <summary>
    /// Klasa zawierająca metody rozszerzające dla Url
    /// </summary>
    public static class UrlExtensions
    {
        /// <summary>
        /// Metoda dodaje parametry z argumentu do adresu jako query string
        /// </summary>
        public static Url SetQueryParams(this Url url, object model)
        {
            if(model ==null)
                throw new ArgumentNullException("model");
            var builder = new StringBuilder(url.Value);
            var modelType = model.GetType();
            var properties = modelType.GetProperties();
            if (properties.Any())
                builder.Append("?");

            foreach (var property in properties)
            {
                builder.Append(property.Name);
                builder.Append("=");
                builder.Append(property.GetValue(model));
                builder.Append("&");
            }
            builder.Remove(builder.Length - 1, 1);
            return new Url(builder.ToString());
        }
    }
}
