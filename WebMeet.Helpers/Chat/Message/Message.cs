﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WebMeet.Helpers.Chat.Message
{
    public class Message
    {

        [JsonConverter(typeof(StringEnumConverter))]
        public MessageType MessageType { get; set; }

        public List<UserStatusChanged> UsersStatusChanged { get; set; } 
        public List<UserMediaAvailbility> UsersMediaAvailbility { get; set; } 

        public UserMessage UserMessage { get; set; }

        public Talk Talk { get; set; }
        public TalkHandshake TalkHandshake { get; set; }

        public long SenderId { get; set; }
        public List<long> ReceiversIdentities { get; set; }
    }

    public enum MessageType
    {
        UsersStatusChanged,
        UsersMediaAvailbility,

        UserMessage, Talk, TalkHandshake
    }

    public class UserStatusChanged
    {
        public long Id { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public UserStatus UserStatus { get; set; }

        public UserStatusChanged(long id, UserStatus userStatus)
        {
            Id = id;
            UserStatus = userStatus;
        }
    }

    public enum UserStatus
    {
        Connected, Disconnected
    }

    public class UserMediaAvailbility
    {
        public long Id { get; set; }
        public bool? WebcamAvaibility { get; set; }
        public bool? MicrophoneAvaibility { get; set; }

        public UserMediaAvailbility(long id, bool? webcamAvaibility, bool? microphoneAvaibility)
        {
            Id = id;
            WebcamAvaibility = webcamAvaibility;
            MicrophoneAvaibility = microphoneAvaibility;
        }
    }
 
    public class TalkHandshake
    {
        [JsonConverter(typeof (StringEnumConverter))] 
        public HType Type;

        public string Offer { get; set; }
        public string Answer { get; set; }
        public string IceCandidate { get; set; }
    }

    public enum HType
    {
        Offer, Answer, IceCandidate
    }

    public class UserMessage
    {
        public string Content { get; set; }
        public DateTime SendDate { get; set; }
        public long ReceiverId { get; set; }
    }


    public class UserBrowserMediaAvailbility
    {
        public bool WebcamAvaibility { get; set; }
        public bool MicrophoneAvaibility { get; set; }
    }

    public class Talk
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public TType Type { get; set; }
        public long Id { get; set; }
        public long ReceiverId { get; set; }
        public List<Interlocutor> Status { get; set; }
        public List<Interlocutor> TalkInfoInterlocutors { get; set; }
        public string Name { get; set; }

    }

    public enum TType
    {
        RequestTalk, AcceptTalk, RejectTalk, Status, Stopped, TalkInfoInterlocutors, Error
    }
}
