﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WebMeet.Helpers.Chat
{
    public class Conversation
    {
        public long Id { get; set; }
        public long Caller { get; set; }
        public List<Interlocutor> Interlocutors { get; set; }
        public long Destination { get; set; }
        public bool ended { get; set; }
    }

    public class Interlocutor
    {
        public long Id { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public InterlocutorStatus Status { get; set; }
        public string Name { get; set; }

        public Interlocutor(long id)
        {
            Id = id;
            Status = InterlocutorStatus.New;
        }
    }

    public enum InterlocutorStatus
    {
        New, Accepted, Rejected, Stopped
    }
}
