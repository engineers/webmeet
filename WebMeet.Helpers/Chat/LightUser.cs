﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMeet.Helpers.Chat
{
    public class LightUser
    {
        public long Id { get; set; }
        public bool? WebcamAvaibility { get; set; }
        public bool? MicrophoneAvaibility { get; set; }
        public string Name { get; set; }

        public LightUser(long id)
        {
            Id = id;
            WebcamAvaibility = null;
            MicrophoneAvaibility = null;
        }

    }

}
